@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        <div style = "background-color : white">
            <img src="{{ config('app.logo') }}" style="height: 100px;" />
        </div>
    @endslot

    # <b>Hello {{$user->first_name}} {{$user->last_name}}</b>

<p>Welcome to ManageTeamz – Fleet Management System. You will have access to the following and Its benefits.</p>

## Web access:

* As an administrator, you shall manage drivers, customers and fleets.<br>
* Assign Drivers to fleets and allocate delivery and pickup tasks. <br>
* Live monitor your fleet / driver’s current location. <br>
* View Trip Schedule, GPS History , Stoppage and Speed reports.<br>

## iPhone and Android Driver App:

* Allow drivers to view their tasks.<br>
* View specific task location and distance / travelling time.<br>
* Start navigating to customer location. <br>
* Map view of their deliverables.<br>
* Change status and update their profile.<br>

## Login Credentials :

<p>Go to <a href="http://managefleetz.com/dashboard/">Login </a><br>
Username : <b>{{$user->email}}</b><br>
Password : <b>{{$user->user_pwd}}</b><br>
</p>

<p>Once you logged in, go to my settings and change your password.</p>

<p>Fleet iPhone App can be downloaded here:</p>
<a href="https://itunes.apple.com/in/app/manageteamz-fleet/id1241927725?mt=8">https://itunes.apple.com/in/app/manageteamz-fleet/id1241927725?mt=8</a>

<p>Fleet Android App can be downloaded here:</p>
<a href="https://play.google.com/store/apps/details?id=com.w2s.android.sfafleet&hl=en">https://play.google.com/store/apps/details?id=com.w2s.android.sfafleet&hl=en</a>

<p>Write to us if you have any queries  ( bd@manageteamz.com )  or send us a tweet @ManageTeamz</p>

Thanks!<br>
ManageTeamz Team<br>
Call : + 91 73387 73388<br>
	   +1-630 299 7737<br>
Email : bd@manageteamz.com<br>
Twitter : <a href="https://twitter.com/ManageTeamz">@ManageTeamz</a><br>

    {{-- Subcopy --}}
    @slot('subcopy')
        @component('mail::subcopy')
            <!-- subcopy here -->
        @endcomponent
    @endslot


    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
     
            <table align="center" width="570" cellpadding="0" cellspacing="0">
                <tr>
                    <td">
                        <p">
                            &copy; {{ date('Y') }}
                            <a" href="{{ url('/') }}" target="_blank">{{ config('app.name') }}</a>.
                            All rights reserved.
                        </p>
                    </td>
                </tr>
            </table>

        @endcomponent
    @endslot
@endcomponent