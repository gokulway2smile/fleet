<tr>
    <td class="header">
        <a href="{{ $url }}">
            {{ $slot }}
            <img src="{{ config('app.logo') }}" style="height: 100px;" />
        </a>
    </td>
</tr>
