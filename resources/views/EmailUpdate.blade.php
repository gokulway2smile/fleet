@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        <div style = "background-color : white">
            <img src="{{ config('app.logo') }}" style="height: 100px;" />
        </div>
    @endslot

    # <b>Hello {{$user->first_name}} {{$user->last_name}}</b>

<p>Welcome to ManageTeamz – Fleet Management System. You Have  been Recently changed Email Address.</p>


Email Address : {{$user->email}}<br>

</p>

<p>Fleet iPhone App can be downloaded here:</p>
<a href="https://itunes.apple.com/in/app/manageteamz-fleet/id1241927725?mt=8">https://itunes.apple.com/in/app/manageteamz-fleet/id1241927725?mt=8</a>

<p>Fleet Android App can be downloaded here:</p>
<a href="https://play.google.com/store/apps/details?id=com.w2s.android.sfafleet&hl=en">https://play.google.com/store/apps/details?id=com.w2s.android.sfafleet&hl=en</a>

<p>Write to us if you have any queries  ( bd@manageteamz.com )  or send us a tweet @ManageTeamz</p>

Thanks!<br>
ManageTeamz Team<br>
Call : + 91 73387 73388<br>
	   +1-630 299 7737<br>
Email : bd@manageteamz.com<br>
Twitter : <a href="https://twitter.com/ManageTeamz">@ManageTeamz</a><br>

    {{-- Subcopy --}}
    @slot('subcopy')
        @component('mail::subcopy')
            <!-- subcopy here -->
        @endcomponent
    @endslot


    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
     
            <table align="center" width="570" cellpadding="0" cellspacing="0">
                <tr>
                    <td">
                        <p">
                            &copy; {{ date('Y') }}
                            <a" href="{{ url('/') }}" target="_blank">{{ config('app.name') }}</a>.
                            All rights reserved.
                        </p>
                    </td>
                </tr>
            </table>

        @endcomponent
    @endslot
@endcomponent