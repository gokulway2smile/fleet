(function() {
    'use strict';
    angular.module('app').controller('SpeedReportCtrl', ['$rootScope', '$state', '$stateParams', '$mdToast', '$timeout', '$scope', '$interval', '$q', '$http', '$mdDialog', 'api', SpeedReportCtrl])

    function SpeedReportCtrl($rootScope, $state, $stateParams, $mdToast, $timeout, $scope, $interval, $q, $http, $mdDialog, api) {
        $scope.mapshow = false;
        $scope.data = {
            "visits": 0,
            "cases": 0,
            "orders": 0,
        };

        function DialogController($scope, $mdDialog, dataToPass) {
            $scope.data = dataToPass;
            $scope.map = true;
            //       $scope.customers = [];
            // $scope.temp = api.customers();
            // $scope.temp().then().then(function(data) {
            //     $scope.customers = data.data;
            // });
            $timeout(function() {
                $scope.$apply();
            }, 0);
            $scope.hide = function() {
                $mdDialog.hide();
            };
            $scope.cancel = function() {
                $mdDialog.cancel();
            };
        }
        $scope.openMap = function(lat, lng, ev) {
            var data = {
                'lat': lat,
                'lng': lng
            };
            $mdDialog.show({
                controller: DialogController,
                locals: {
                    dataToPass: data
                },
                templateUrl: 'assets/app/speed-report/modal_view.html?' + Math.random(),
                targetEvent: ev,
                clickOutsideToClose: false
            }).then(function(answer) {}, function() {});
        };
        $scope.datePicker = {
            startDate: new Date(),
            endDate: new Date()
        };
        $scope.temp = api.customers();
        $scope.temp().then().then(function(data) {
            $scope.customers = data.data;
        });
        $scope.vehicle = [];
        $scope.temp = api.competitor();
        $scope.temp().then().then(function(data) {
            $scope.vehicle = data.data;
        });
        $scope.users = [];
        $scope.temp = api.users();
        $scope.temp().then().then(function(data) {
            $scope.users = data.data;
        });
        $scope.emp_id = '';
        // $scope.start_date = new Date();
        // $scope.end_date = new Date();
        $scope.emppath = [];
        $scope.data = [];
        $scope.mapcenter = api.mapcenter;
        $scope.DatePickerOptions = api.DatePickerOptions;
        $scope.users = [];
        $scope.mapinfo = [];
        $scope.temp = api.users();
        $scope.temp().then().then(function(data) {
            $scope.users = data.data;
        });
        $scope.mark = false;
        $scope.tempdemo = [];
        for (var i = 0; i < 1000; i++) {
            $scope.tempdemo.push(i);
        }
        // $http.get('assets/app/speed-report/data.json')
        // .success(function(data) {  });


        $scope.filter = function() {
            if ($scope.data_form.$valid) {
                $http.post('vehicle_speed_api/' + $scope.emp_id, {
                    data: {
                        start_date: api.CovertDateTime($scope.datePicker.startDate),
                        end_date: api.CovertDateTime($scope.datePicker.endDate),
                        emp_id: $scope.emp_id,
                        // speed_type: $scope.speed_type,
                        // speed: $scope.speed
                    }
                }).success(function(response) {


                    $scope.data = response.data;

                    $timeout(function() {


                        var data = response.data.visit_list.map(function(value, index) {
                            return [value.timestamp,value.speed];
                        });

                       $scope.testoption = {
                title: {
                    text: 'Speed Report'
                },
                tooltip: {
                    trigger: 'axis'
                },
                xAxis: {
                    data: data.map(function (item) {
                        return item[0];
                    })
                },
                yAxis: {
                    splitLine: {
                        show: false
                    }
                },
                toolbox: {
                    left: 'center',
                    feature: {
                        dataZoom: {
                            yAxisIndex: 'none'
                        },
                        restore: {},
                        saveAsImage: {}
                    }
                },
                dataZoom: [{
                    type: 'inside'
                }],
                visualMap: {
                    top: 10,
                    right: 10,
                    pieces: [{
                        gt: 0,
                        lte: 20,
                        color: '#096'
                    },

                    {
                        gt: 20,
                        lte: 40,
                        color: '#096'
                    },

                    {
                        gt: 40,
                        lte: 60,
                        color: '#ffde33'
                    }, {
                        gt: 60,
                        lte: 80,
                        color: '#ff9933'
                    }, {
                        gt: 100,
                        lte: 120,
                        color: '#cc0033'
                    }, {
                        gt: 120,
                        lte: 140,
                        color: '#660099'
                    }, {
                        gt: 140,
                        color: '#7e0023'
                    }],
                    outOfRange: {
                        color: '#999'
                    }
                },
                series: {
                    name: 'Speed',
                    type: 'line',
                    data: data.map(function (item) {
                        return item[1];
                    }),
                    markLine: {
                        silent: true,
                        data: [{
                            yAxis: 20
                        }, {
                            yAxis: 40
                        }, {
                            yAxis: 60
                        },

                        {
                            yAxis: 80
                        },
                        {
                            yAxis: 100
                        },

                        {
                            yAxis: 120
                        },

                                  {
                            yAxis: 140
                        },

                        {
                            yAxis: 140
                        }]
                    }
                }
            };
                        $scope.$apply();
                    }, 0);
                    // start = _.values(_.omitBy(_.map($scope.data.geo, function(v,index) {
                    //     if (v['activity'] == 'Start') {
                    //         return index;
                    //     }
                    // }), _.isNil));
                    // end = _.values(_.omitBy(_.map($scope.data.geo, function(v,index) {
                    //     if (v['activity'] == 'Stop') {
                    //         return index;
                    //     }
                    // }), _.isNil));
                    // $scope.geoPath = [];
                    // $scope.geoTempPath = [];
                    // for (var i = 0; i < start.length; i++) {
                    //     if (end[i]) {
                    //         $scope.geoPath.push($scope.data.geo.splice(start[i], end[i]));
                    //         if ($scope.data.geo[start[i] - 1] && $scope.data.geo[end[i] + 1]) {
                    //             $scope.geoTempPath.push($scope.data.geo.splice(end[i] + 1, start[i] - 1));
                    //         }
                    //     }
                    // }
                    $timeout(function() {
                        $scope.$apply();
                    }, 0);
                });
            }
        }
        $scope.$on('$viewContentLoaded', function() {
            $scope.mapshow = true;
        })
        //charts
        var textColor = '#989898' // label, legend etc.
            ,
            splitLineColor = 'rgba(0,0,0,.05)',
            splitAreaColor = ['rgba(250,250,250,0.035)', 'rgba(200,200,200,0.1)']
        $scope.chart = {};
        $scope.chart.options = {
            tooltip: {
                trigger: 'axis'
            },
            legend: {
                // data:['Email','Affiliate','Video Ads','Direct','Search'],
                textStyle: {
                    color: textColor
                }
            },
            toolbox: {
                show: true,
                // feature : {
                //     saveAsImage : {show: true, title: "save as image"}
                // }
            },
            calculable: true,
            xAxis: [{
                type: 'category',
                boundaryGap: false,
                data: [],
                axisLabel: {
                    textStyle: {
                        color: textColor
                    }
                },
                splitLine: {
                    lineStyle: {
                        color: splitLineColor
                    }
                }
            }],
            yAxis: [{
                type: 'value',
                axisLabel: {
                    textStyle: {
                        color: textColor
                    }
                },
                splitLine: {
                    lineStyle: {
                        color: splitLineColor
                    }
                },
                splitArea: {
                    show: true,
                    areaStyle: {
                        color: splitAreaColor
                    }
                }
            }],
            series: [{
                name: 'Speed',
                type: 'line',
                stack: 'Sum',
                itemStyle: {
                    normal: {
                        areaStyle: {
                            type: 'default'
                        }
                    }
                },
                data: []
            }]
        };
    }
})();
