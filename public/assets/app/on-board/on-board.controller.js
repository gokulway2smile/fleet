    (function() {
    'use strict';
    angular.module('app').controller('OnBoardCtrl', ['$rootScope', '$state', '$stateParams', '$mdToast', '$timeout', '$scope', '$mdEditDialog', '$q', '$http', '$mdDialog', 'api', 'WizardHandler', OnBoardCtrl])

    function OnBoardCtrl($rootScope, $state, $stateParams, $mdToast, $timeout, $scope, $mdEditDialog, $q, $http, $mdDialog, api, WizardHandler) {
        
        

        $scope.myObj.TimeZoneSelect =true;
        
        $scope.gps_active = api.gps_active;
        $scope.apipath="on-board";
        $scope.stepActive = true;
        $scope.canExit = false;
        $scope.userData = {};
        $scope.data = {};
        $scope.userData.CreateDriver = false;
        $scope.fleetData = {};
        $scope.fleetData.CreateFleet = false;
        $scope.customerData = {};
        $scope.customerData.CreateCustomer = false;
        $scope.roles = [];
        $scope.customerData.loc_lat = 0;
        $scope.customerData.loc_lng = 0;
        $scope.customerData.desc = 150;
        $scope.customers = [];
        $scope.datePicker = {
            startDate: new Date(),
            endDate: new Date()
        };
        $scope.taskData = {};
        $scope.taskData.height = 300;
        $scope.taskData.CreateDriver = false;
        $scope.taskData.stoppage_limit = "00:00";
        $scope.taskData.cust_jobs = [];
        $scope.scheduletypes = api.scheduletypes;
        $scope.method = ['Pickup', 'Drop'];

        $scope.data.start_dt = moment();


        $scope.addNewChoice = function(b = '') {

            $scope.taskData.height = $scope.taskData.height + 100;
            $scope.taskData.cust_jobs.push({
                cust_id: b,
                type: 0,
                notes: '',
                method: '',
            });
        };
        $scope.removeChoice = function(data) {
            if (data > -1) {
              $scope.taskData.height = $scope.taskData.height - 100;
                $scope.taskData.cust_jobs.splice(data, 1);
            }
        };
        $scope.currentStepNumber = function() {
            return WizardHandler.wizard().currentStepNumber()
        }
        $scope.ReachTask = function() {

            if ($scope.userData.user_id) {

                     $scope.taskData.emp = $scope.userData.user_id;
            }
            if ($scope.customerData.id) {


                  $scope.taskData.height = $scope.taskData.height + 100;
            $scope.taskData.cust_jobs.push({
                cust_id: $scope.customerData.id,
                type:  $scope.customerData.type,
                notes: '',
                method: 'Pickup',
            });

            }

            if ($scope.fleetData.id) {

                     $scope.taskData.vehicle_no = $scope.fleetData.id;
            }


        };
        $scope.previousStep = function() {
            if ($scope.userData.CreateDriver == 'Create') {
                $scope.userData.CreateDriver = false;
                return;
            }
            if ($scope.fleetData.CreateFleet) {
                $scope.fleetData.CreateFleet = false;
                return;
            }
            if ($scope.customerData.CreateCustomer) {
                $scope.customerData.CreateCustomer = false;
                return;
            }
            if ($scope.taskData.CreateDriver) {
                $scope.taskData.CreateDriver = false;
                return;
            }
            $scope.userData.CreateDriver = false;
            $scope.fleetData.CreateFleet = false;
            $scope.customerData.CreateCustomer = false;
            $scope.taskData.CreateDriver = false;
            try {
                WizardHandler.wizard().previous();
            } catch (err) {}
        }

        $scope.goTodashboard = function()
        {
               $state.go('admin.company_dashboard');
        }

        $scope.createTaskResponseMsg = "";
        $scope.createTask = function(isValid) {

            if (isValid) {

                if ($scope.taskData.cust_jobs.length == 0) {
                // $mdDialog.show($mdDialog.alert().title("No Tasks in This Schedule").ok('OK'));
                $scope.createTaskResponseMsg = "No Tasks in This Schedule";
                // alert("No Tasks in This Schedule");
                return;
            }
            if (api.checkdups($scope.taskData.cust_jobs, 'cust_id', 'int')) {
                // $mdDialog.show($mdDialog.alert().title("Duplicate Customers not allowed").ok('OK'));
                // alert("Duplicate Customers not allowed");
                $scope.createTaskResponseMsg = "Duplicate Customers not allowed";
                return;
            }

                    $scope.taskData.start_dt =  api.CovertDateTime($scope.datePicker.startDate);
                    $scope.taskData.end_dt =  api.CovertDateTime($scope.datePicker.endDate);



            $http.post('task/?getTask=yes', {
                data: $scope.taskData
            }).then(function(response) {
                var res = angular.fromJson(response);
                if (res.data.status == 'ok') {
                    $scope.taskData.Created
                    // alert("New trip allocated");
                    // $mdDialog.show($mdDialog.alert().title('New trip allocated').ok('OK'));
                    $scope.ShowHideTask();
                    $scope.createTaskResponseMsg = "";
                    $scope.taskData = {};
                    $scope.taskData.CreateDriver = false;
                    $scope.taskData.stoppage_limit = "00:00";
                    $scope.taskData.cust_jobs = [];
                    WizardHandler.wizard().next();
                } else {
                    // alert(res.data.data);
                    // $mdDialog.show($mdDialog.alert().title(res.data.data).ok('OK'));
                    $scope.createTaskResponseMsg = "";
                }
            }, function(response) {});

            }
            
        }


        $scope.StoppageTime = [];
        $scope.StoppageTime = api.StoppageTime();
        $scope.tempcus = api.customers();
        $scope.tempcus().then().then(function(data) {
            if (data.data['0']) {
                $scope.clat = data.data['0'].loc_lat;
                $scope.clng = data.data['0'].loc_lng;
            }
            $scope.customers = data.data;
        });
        $scope.companytype = api.companytype;
        $scope.mapzoom = 20;
        $scope.getpos = function(data, zoom = 12) {
            if (data.lat) {
                $scope.customerData.loc_lat = data.lat();
                $scope.customerData.loc_lng = data.lng();
                $scope.mapzoom = zoom;
                $timeout(function() {
                    $scope.$apply();
                }, 0);
            } else {
                $scope.customerData.loc_lat = data.location.lat;
                $scope.customerData.loc_lng = data.location.lng;
                $scope.mapzoom = zoom;
                $
                timeout(function() {
                    $scope.$apply();
                }, 0);
            }
        };
        $scope.geocodeupdateloc = function() {
            var address = '';
            if ($scope.customerData.address && $scope.customerData.address != undefined) {
                address = address + $scope.customerData.address;
            }
            if (address) {
                var geocoder = new google.maps.Geocoder();
                geocoder.geocode({
                    "address": address
                }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK && results.length > 0) {
                        var location = results[0].geometry.location;
                        $scope.getpos(location);
                    } else {}
                });
            }
        }
        $scope.temproles = api.roles();
        $scope.temproles().then().then(function(data) {
            $scope.roles = data.data;
        });
        $scope.users = [];
        $scope.temp = api.users();
        $scope.temp().then().then(function(data) {
            $scope.users = data.data;
        });
        $scope.competitorinfo = [];
        $scope.tempcompetitor = api.competitor();
        $scope.tempcompetitor().then().then(function(data) {
            $scope.competitorinfo = data.data;
        });
        $scope.userData.role_id = 1;
        $scope.userData.zipcode = 1;
        $scope.userData.is_active = 1;
        $scope.chooseUser = function() {
            if ($scope.userData.user_id) {
                console.log('done');
                WizardHandler.wizard().next();
            }
        }
        $scope.ee = function() {
            WizardHandler.wizard().next();
        }

        $scope.createFleetResponseMsg = "";
        $scope.createFleet = function(isValid) {
        
            if (isValid) {
                $http.post('competitor/', {
                data: $scope.fleetData
            }).then(function(response) {
                var res = angular.fromJson(response);
                if (res.data.status == 'ok') {
                    // $mdDialog.show($mdDialog.alert().title('New Fleet Added').ok('OK'));
                    $scope.createFleetResponseMsg = "";
                    $scope.ShowHideFleet();
                    $scope.tempcompetitor = api.competitor();
                    $scope.tempcompetitor().then().then(function(data) {
                        $scope.competitorinfo = data.data;
                    });
                    $scope.fleetData.CreateFleet = false;

                    $scope.fleetData.name = "";
                    $scope.fleetData.address = "";
                    $scope.fleetData.desc = "";
                    $scope.fleetData.remark = "";

                    $scope.fleetData.id = res.data.data.id;

                    WizardHandler.wizard().next();
                } else {
                    // $mdDialog.show($mdDialog.alert().title(res.data.data).ok('OK'));
                    // alert(res.data.data);
                    $scope.createFleetResponseMsg = res.data.data;
                }
            }, function(response) {});
            }
        }

        $scope.createLocationResponseMsg = "";
        $scope.createLocation = function(isValid) {

            if (isValid){
                if ($scope.customerData.loc_lat == 0 || $scope.customerData.loc_lng == 0) {
                // $mdDialog.show($mdDialog.alert().title('Location Invalid').ok('OK'));
                $scope.createLocationResponseMsg = "Location Invalid";
                return;
            }
            $scope.customerData.uploads = [];
            $http.post('customer/?getCustomer=yes', {
                data: $scope.customerData
            }).then(function(response) {
                var res = angular.fromJson(response);
                if (res.data.status == 'ok') {
                    // $mdDialog.show($mdDialog.alert().title('New Location Added').ok('OK'));
                    $scope.createLocationResponseMsg = "";
                    $scope.ShowHideLocation();
                    $scope.tempcus = api.customers();
                    $scope.tempcus().then().then(function(data) {
                        if (data.data['0']) {
                            $scope.clat = data.data['0'].loc_lat;
                            $scope.clng = data.data['0'].loc_lng;
                        }
                        $scope.customers = data.data;
                    });

                    $scope.customerData.name = "";
                    $scope.customerData.contact_no = "";
                    $scope.customerData.desc = "";
                    $scope.customerData.type = "";
                    $scope.customerData.address = "";

                    $scope.customerData.CreateCustomer = false;
                    $scope.customerData.id = res.data.data.id;
                    $scope.customerData.type = res.data.data.type;
                      $scope.ReachTask();
                    WizardHandler.wizard().next();
                } else {
                    $scope.createLocationResponseMsg = res.data.data;
                    // $mdDialog.show($mdDialog.alert().title(res.data.data).ok('OK'));
                    // alert(res.data.data);
                }
            }, function(response) {});
            }
        }

        $scope.createUserResponseMsg = "";

        $scope.createUser = function(isValid) {

            if (isValid){
                $http.post('user/?getUser=yes', {
                data: $scope.userData
            }).then(function(response) {
                var res = angular.fromJson(response);
                if (res.data.status == 'ok') {
                    if (res.data.data.user_id) {
                        // $mdDialog.show($mdDialog.alert().title('Driver Created').ok('OK'));
                        $scope.createUserResponseMsg = "";
                        $scope.ShowHideDriver();
                        $scope.userData.CreateDriver = false;
                        $scope.userData.user_id = res.data.data.user_id;
                        
                        $scope.userData.first_name = "";
                        $scope.userData.last_name = "";
                        $scope.userData.phone = "";
                        $scope.userData.email = "";
                        $scope.userData.street = "";
                        $scope.userData.zipcode = "";

                        $scope.temp = api.users();
                        $scope.temp().then().then(function(data) {
                            $scope.users = data.data;
                        });
                        WizardHandler.wizard().next();
                    }
                } else {
                    $scope.createUserResponseMsg = res.data.data;
                    // $mdDialog.show($mdDialog.alert().title(res.data.data).ok('OK'));
                    // alert(res.data.data);
                }
            }, function(response) {});
            }
        }


        $scope.finished = function() {
            alert("Wizard finished :)");
        };
        $scope.logStep = function() {
            console.log("Step continued");
        };
        $scope.goBack = function() {
            WizardHandler.wizard().goTo(0);
        };
        $scope.exitWithAPromise = function() {
            var d = $q.defer();
            $timeout(function() {
                d.resolve(true);
            }, 200);
            return d.promise;
        };
        $scope.exitToggle = function() {
            $scope.canExit = !$scope.canExit;
        };
        $scope.stepToggle = function() {
            $scope.stepActive = !$scope.stepActive;
        }





//       $scope.init  = function (ev) {
  $scope.opendiv = function(ev){      
        
        var a=$scope;
        $mdDialog.show({
            
            escapeToClose: false,
            controller: myDialogcntrller,
            locals: {parent: a},
            templateUrl: 'assets/app/on-board/fisrtpopup.tmpl.html',
            targetEvent: ev
            
        })
        };

        function  myDialogcntrller($scope, $mdDialog, $rootScope,parent) {           
        
                $scope.myObj = parent;
               $scope.hide = function () {
                    $mdDialog.hide(function () {                        
                   });
                    res = false;
                };
                $scope.cancel = function () {
                    $mdDialog.cancel(function () {

                   });
                    /*Call ur Function*/  
                };

            }
          
         $scope.IsDriverHidden = false;
        $scope.ShowHideDriver = function () {
                //If DIV is hidden it will be visible and vice versa.
                $scope.IsDriverHidden = true;
                $scope.IsFleetHidden = false;
            }

         $scope.IsFleetHidden = true;
         $scope.ShowHideFleet = function () {
                //If DIV is hidden it will be visible and vice versa.

                $scope.customerData.CreateCustomer ="Create";
                $scope.IsFleetHidden = true;
                $scope.IsLocationHidden = false;
            }

         $scope.IsLocationHidden = true;
         $scope.ShowHideLocation = function () {

                //If DIV is hidden it will be visible and vice versa.
                $scope.IsLocationHidden = true;
                $scope.IsTaskHidden = false;
            }

         $scope.IsTaskHidden = true;
         $scope.ShowHideTask = function () {
                //If DIV is hidden it will be visible and vice versa.
                // $scope.IsTaskHidden = true;
                // $scope.IsDriverHidden = false;

                // $scope.myDialogcntrller();
                $mdDialog.cancel();
                $scope.goTodashboard();
            }

        $scope.backTask = function () {
                //If DIV is hidden it will be visible and vice versa.
                $scope.customerData.CreateCustomer ="Create";
                $scope.IsTaskHidden = true
                $scope.IsLocationHidden = false;
            }

        $scope.backLocation = function () {
                //If DIV is hidden it will be visible and vice versa.
                $scope.IsLocationHidden = true;
                $scope.IsFleetHidden = false;
            }

        $scope.backFleet = function () {
                //If DIV is hidden it will be visible and vice versa.
                $scope.IsFleetHidden = true;
                $scope.IsDriverHidden = false;
            }


        $scope.exitValidation = function() {
            return $scope.canExit;
        };


        $scope.dateOptions = {
                timePicker: true,
                timePickerIncrement: 5,
                autoUpdateInput: false,
                singleDatePicker: true,
                locale: {
                    format: 'Do MMM YY, h:mm A'
                },
                eventHandlers: {
                    'cancel.daterangepicker': function(ev, picker) {
                        console.log(picker)
                        console.log(ev)
                    }
                }
            };
    }


angular.module('app').controller('TimeZoneCtl', ['$rootScope', '$state', '$stateParams', '$mdToast', '$timeout', '$scope', '$mdEditDialog', '$q', '$http', '$mdDialog', 'api', 'WizardHandler', TimeZoneCtl])


function TimeZoneCtl($rootScope, $state, $stateParams, $mdToast, $timeout, $scope, $mdEditDialog, $q, $http, $mdDialog, api, WizardHandler) 
{


$scope.opendiv = function(ev)
{

 $http.get('profile')
                .then(
                    function(response) {

                        var res = angular.fromJson(response);
                        
                        if (res.data.data.timezone != null) {
                             $mdDialog.hide();
                            
                            console.log(res.data.data.timezone);
                            $state.go("admin.company_dashboard");

//                            $scope.hide_time =true;
                            } else {
                                    $scope.opendiv1();
                                    
                            
                        }
                    },
                    function(response) {

                    }
                );
            }

    

        $scope.timeZone= $http.get('gettimezone/').then(function(response) {
            $scope.timeZone = angular.fromJson(response).data;
            
                       return $scope.timeZone;
            });

      //  $scope.timezone=$rootScope.timeData.timezone;
        
    //  console.log($scope.timeZone);
$scope.opendiv1 = function(ev)
{

      $http.get('gettimezone/').then(function (response){
        $scope.timeZone=angular.fromJson(response).data;
        var timezone = $scope.timeZone;
                      $mdDialog.show({
            controller: TimeZoneCtl,
             locals: {
                dataToPass: timezone
            },
            templateUrl: 'assets/app/on-board/timezone.html?' + Math.random(),
            targetEvent: ev,
            clickOutsideToClose: false,
            escapeToClose: false
            
        });
        


});
}
  
  
  

       $scope.funtion1  = function (ev) {
        
        
        var a=$scope;
        $mdDialog.show({
            
            escapeToClose: false,
            controller: OnBoardCtrl,
            locals: {parent: a},
            templateUrl: 'assets/app/on-board/fisrtpopup.tmpl.html',
            targetEvent: ev
            
        })
        };

        $scope.TimeZoneResponseMsg = "";

        $scope.TimeZone = function(isValid) {

             if (isValid){
                     var userData=this.timezone;
                     var address=this.address;
                     var mail=this.mail;
                     var sms=this.sms;
                   
                 $http.post('user/timezone', {
                 data: userData,address,mail,sms
             }).then(function(response) {
                 var res = angular.fromJson(response);
                  
                 if (res.data.status == 'ok') {
                    $mdDialog.show($mdDialog.alert().title('TimeZone Updated').ok('OK'));
                    //    $rootScope.OnBoardCtrl();
                            $state.reload();
                            //$state.go('admin.company_dashboard');

                      
                    
                     if (res.data.data.user_id) {
                          $mdDialog.show($mdDialog.alert().title('TimeZone Updated').ok('OK'));
                         $scope.TimeZoneResponseMsg = "";                        
                     }
                 } else {
                     $scope.TimeZoneResponseMsg = res.data.data;
                     $mdDialog.show($mdDialog.alert().title(res.data.data).ok('OK'));
                    
                 }
             }, function(response) {});
            }
         }



           
    $scope.searchTextChange=function(text) {
      $log.info('Text changed to ' + text);
    }

    $scope.selectedItemChange=function(item) {
        console.log(item)
      $log.info('Item changed to ' + JSON.stringify(item));
    }

    
   
                // $rootScope.updatebasicinfo = false;
                // $scope.timezone=$rootScope.timeData.timezone;
                // if($rootScope.timeData.mailnote == 1){
                //     $scope.mail=1;
                //     $scope.mailsts=true;

                // }
                // if($rootScope.timeData.smsnote == 1){
                //     $scope.sms=1;
                //     $scope.smssts=true;
                // }
                //  if($rootScope.timeData.is_multipick == 1){
                //     $scope.is_multipick=1;
                // }
                // $scope.address=$rootScope.timeData.street;
                $scope.cancel = function() {
                    // if($rootScope.timeData.timeZone && $rootScope.timeData.street){
                        $mdDialog.cancel();
                    // }
            };
                $scope.pickup_ladd = 0;
                $scope.pickup_long = 0;

            $scope.geocodeupdatelocpickup = function() {
                var address = $scope.address;
                
                if (address) {
                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode({
                        "address": address
                    }, function(results, status) {
                        if (status == google.maps.GeocoderStatus.OK && results.length > 0) {
                            var location = results[0].geometry.location;
                            console.log(results);
                            $scope.getpickuppos(location);
                        } else {}
                    });
                }
            }
             $scope.getpickuppos = function(data, zoom,$status) {
                var zoom = zoom || 18;
                console.log(data);


$scope.map = {
      center: {
        latitude: 21.0000,
        longitude: 78.0000
      },
      zoom: 4,
      events: {
        click: function(mapModel, eventName, originalEventArgs,ok) {
          var e = originalEventArgs[0]; 

            var geocoder = new google.maps.Geocoder();
            var latlng = new google.maps.LatLng(e.latLng.lat(), e.latLng.lng());

            geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[1]) {

                            console.log(results[1].formatted_address); // details address
                        } else {
                            console.log('Location not found');
                        }
                    } else {
                        console.log('Geocoder failed due to: ' + status);
                    }
                });


        }
      }
    };



                $scope.pickup_ladd = data.lat();
                $scope.pickup_long = data.lng();
                $scope.mapzoom = zoom;
                $timeout(function() {
                    $scope.$apply();
                }, 0);
                        $scope.showpickupmap = true;

            };
                            $scope.geocodeupdatelocpickup();

            $scope.showpickupmap1=function()
            {
                $scope.showpickupmap = false;
            }
            $scope.showpickupmap2=function()
            {
                $scope.showpickupmap = true;
            }

            $scope.addtolist=function(){
                if(!$scope.add){
                    $scope.records = $scope.data.records;
                   // console.log($scope.data.records);
                var pictime=moment($scope.data.picktime).format("YYYY-MM-DD, h:mm A")
                var data = {
                    pic_loc: $scope.data.pick_address,
                    pic_date: pictime,
                    pic_item: $scope.data.notes,
                    pic_ladd : $scope.data.pickup_ladd,
                    pic_long : $scope.data.pickup_long
                };
                 $scope.records.push(data);
                 $scope.data.pick_address = '';
                 $scope.data.notes = '';
                 $scope.data.records = $scope.records;
                }else{
                  var pictime=moment($scope.data.picktime).format("YYYY-MM-DD, h:mm A")
                  var data = {
                    pic_loc: $scope.data.pick_address,
                    pic_date: pictime,
                    pic_item: $scope.data.notes,
                    pic_long : $scope.data.pickup_ladd,
                    pic_ladd : $scope.data.pickup_long
                };
                 $scope.records.push(data);
                 $scope.data.pick_address = '';
                 $scope.data.notes = '';
                $scope.data.records = $scope.records;
                }
                $scope.data.pickup_ladd
            }
            $scope.editpickup = function(index){
                
                var datalist = $scope.data.records[index];
                $scope.data.pick_address = datalist.pic_loc;
                $scope.data.notes = datalist.pic_item; 
                $scope.data.pickup_ladd = datalist.pic_ladd;
                $scope.data.pickup_long = datalist.pic_long; 
                 $scope.data.records.splice(index,1);

            }
            $scope.deletepickup = function(index){
                $scope.data.records.splice(index,1);

            }


$scope.geocodeupdateloc1 = function() {
                var address = $scope.data.cust_address;
                if (address) {
                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode({
                        "address": address
                    }, function(results, status) {
                        if (status == google.maps.GeocoderStatus.OK && results.length > 0) {
                            var location = results[0].geometry.location;
                            $scope.getpos(location);
                        } else {}
                    });
                }
            }
             
            $scope.geocodeupdatelocsender = function() {
                var address = $scope.data.sent_address;
                if (address) {
                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode({
                        "address": address
                    }, function(results, status) {
                        if (status == google.maps.GeocoderStatus.OK && results.length > 0) {
                            var location = results[0].geometry.location;
                            $scope.getsenderpos(location);
                        } else {}
                    });
                }
            }
            $scope.getsenderpos = function(data, zoom,status) {
                var zoom = zoom || 18;
                 if(status){
                  var geocoder = new google.maps.Geocoder();
            var latlng = new google.maps.LatLng(data.lat(), data.lng());

            geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[1]) {
                            $scope.data.sent_address= results[1].formatted_address;
                            $scope.data.sent_ladd = data.lat();
                            $scope.data.sent_long = data.lng();
                            $scope.mapzoom = zoom;
                            $scope.$apply();
                            console.log(results[1].formatted_address); // details address
                        } else {
                            console.log('Location not found');
                        }
                    } else {
                        console.log('Geocoder failed due to: ' + status);
                    }
                });
        }
                $scope.data.sent_ladd = data.lat();
                $scope.data.sent_long = data.lng();
                $scope.mapzoom = zoom;
                $timeout(function() {
                    $scope.$apply();
                }, 0);
            };
            
             
 
             $scope.setadd = function()
             {
                $scope.getpickuppos();
             };
            $scope.getpos = function(data, zoom,status) {
                var zoom = zoom || 18;
                 if(status){
                 var geocoder = new google.maps.Geocoder();
            var latlng = new google.maps.LatLng(data.lat(), data.lng());

            geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[1]) {
                            $scope.data.cust_address= results[1].formatted_address;
                            $scope.data.loc_lat = data.lat();
                            $scope.data.loc_lng = data.lng();
                            $scope.mapzoom = zoom;
                            $scope.$apply();
                            console.log(results[1].formatted_address); // details address
                        } else {
                            console.log('Location not found');
                        }
                    } else {
                        console.log('Geocoder failed due to: ' + status);
                    }
                });
        }
                $scope.data.loc_lat = data.lat();
                $scope.data.loc_lng = data.lng();
                $scope.mapzoom = zoom;
                $timeout(function() {
                    $scope.$apply();
                }, 0);
            };
            $scope.submit_form = function() {
                if($scope.multipickup){
                    if($scope.add){
                    var arrayrecords = $scope.records;
                    }else{
                    var arrayrecords = $scope.data.records;
                    }
                    console.log(arrayrecords);
                    var i=0;
                    var pic_loc = [];
                    var pic_date = [];
                    var pic_item = [];
                    var pic_ladd = [];
                    var pic_long = [];
                    for(;i<arrayrecords.length;i++){
                        if(i == (arrayrecords.length)-1){
                            pic_loc [i] =arrayrecords[i].pic_loc
                        }else{
                            pic_loc [i] =arrayrecords[i].pic_loc+"||" 
                        }
                        pic_item [i]=arrayrecords[i].pic_item;
                        pic_ladd [i]=arrayrecords[i].pic_ladd;
                        pic_long [i]=arrayrecords[i].pic_long;
                    }                    
                  $scope.data.pick_address = pic_loc.toString(),
                  $scope.data.notes = pic_item.toString();
                  $scope.data.pickup_ladd = pic_long.toString();
                  $scope.data.pickup_long = pic_ladd.toString();
                  
                }else{

                }
                if ($scope.data_form1.$invalid) {
                    // if(!$scope.multipickup){

                    //     if(arrayrecords.length == 0){
                    //         $scope.alert = 'Please Add The Pickup Point To List ';
                    //         return;
                    //     }
                    // }else{
                    //                             alert(arrayrecords.length);

                    //         return;
                    // }
                }
                if ($scope.data.loc_lat == 0 || $scope.data.loc_lng == 0) {
                    return;
                }
                 else{
                    console.log('Address Lat Lang is Valid');
                }

                // if ($scope.datePicker._i.length == 13) {
                //                $scope.alert = 'Date time Invalid';
                // }


                console.log($scope.data.mob);
                $scope.data.schedule_date_time = api.CovertDateTime($scope.datePicker);
                $scope.data.pick_date_time = api.CovertDateTime($scope.data.picktime);
                console.log($scope.data.sent_ladd);
                console.log($scope.data.comments+$scope.data.mob);
                if ($scope.add == true) {
                    $http.post($scope.apipath, {
                        data: $scope.data,
                        comments:$scope.data.comments,
                        mob:$scope.data.mob,
                        cust_email:$scope.data.cust_email,
                        sender_name:$scope.data.sender_name,
                        sender_number:$scope.data.sender_number,
                        sent_ladd:$scope.data.sent_ladd ,
                        sent_long:$scope.data.sent_long ,
                    }).then(function(response) {
                        var res = angular.fromJson(response);
                        console.log(res);
                        if (res.data.status == 'ok') {
                            $mdDialog.hide('reload');
                            $mdDialog.show($mdDialog.alert().title(res.data.data).ok('OK'));
                            // setTimeout($scope.cancel(), 2000)
                        } else {
                          $scope.alert = res.data.data;
                        }
                    }, function(response) {
                        // failure call back
                    });
                } else {
                    $http.put($scope.apipath + $scope.data.id, {
                        data: $scope.data,
                         comments:$scope.data.comments,
                        mob:$scope.data.mob,
                        cust_email:$scope.data.cust_email,
                        sender_name:$scope.data.sender_name,
                        sender_number:$scope.data.sender_number
                    }).then(function(response) {
                        var res = angular.fromJson(response);
                        if (res.data.status == 'ok') {
                            //   $scope.alert = res.data.data;
                            $mdDialog.hide('reload');
                            $mdDialog.show($mdDialog.alert().title(res.data.data).ok('OK'));
                            //setTimeout($scope.cancel(), 2000)
                        } else {
                            $scope.alert = res.data.data;
                        }
                    }, function(response) {
                        // failure call back
                    });
                }
                console.log($scope.data);
            };
            $scope.hide = function() {
                $mdDialog.hide();
            };
            $scope.cancel = function() {
                $mdDialog.cancel();
            };
        
        $scope.datadesel = function(){
           
        }

        $scope.geocodeupdateloc = function() {
            if ($scope.address) {
                var geocoder = new google.maps.Geocoder();
                geocoder.geocode({
                    "address": $scope.address
                }, function(results, status) {

                    if (status == google.maps.GeocoderStatus.OK && results.length > 0) {
                        var location = results[0].geometry.location;
                        $scope.mapcenter = location;
                        $scope.zoom = 20;

                        $timeout(function() { $scope.$apply(); }, 10);
                       // $scope.getpos(location);
                    } else {}
                });
            }
        }
$scope.querySearch =function(query) {
       var deferred = $q.defer();      
           
                var states = $scope.timeZone.filter(function(state) {
                    console.log(state);
                    return (state.desc.toUpperCase().indexOf(query.toUpperCase()) !== -1 || state.desc.toUpperCase().indexOf(query.toUpperCase()) !== -1);
                });

                deferred.resolve(states);
                console.log(deferred.promise);
            return deferred.promise;
    }

        $scope.submit_data = function(){
            console.log(this.ctrl.searchText);

            if(!$scope.mail){
                $scope.mail = 0;
            }
            if(!$scope.sms){
                $scope.sms = 0;
            }
            if(!$scope.is_multipick){
                $scope.is_multipick = 0;
            }
            if ($scope.pickup_ladd == 0 || $scope.pickup_long == 0) {
                    return;
                }
            $http.post('updatebasicinfo/',{data:{"timezone":this.ctrl.searchText,
                "mailnote":$scope.mail,
                "smsnote":$scope.sms,
                "street":$scope.address,
                "is_multipick":$scope.is_multipick
             }
            }).then(function(response){
                          $mdDialog.show($mdDialog.alert().title("This TimeZone is Used For Your Applications").ok('OK'));
                                      var res = angular.fromJson(response);
                                      $scope.data = res.data.data;

                $rootScope.timeData=$scope.data;
                // debugger;
                console.log($rootScope.timeData);
                window.location.reload();

            });

        }

}


})();
