(function() {
    'use strict';
    angular.module('app').controller('TripSummaryCtrl', ['$rootScope', '$state', '$stateParams', '$mdToast', '$timeout', '$scope', '$interval', '$q', '$http', '$mdDialog', 'api', TripSummaryCtrl])

    function TripSummaryCtrl($rootScope, $state, $stateParams, $mdToast, $timeout, $scope, $interval, $q, $http, $mdDialog, api) {
        $scope.mapshow = false;
        $scope.data = {
            "visits": 0,
            "cases": 0,
            "orders": 0,
        };

        function DialogController($scope, $mdDialog, dataToPass) {
            $scope.data = dataToPass;
            $scope.customers = [];
            $scope.temp = api.customers();
            $scope.temp().then().then(function(data) {
                $scope.customers = data.data;
            });
            $scope.hide = function() {
                $mdDialog.hide();
            };
            $scope.cancel = function() {
                $mdDialog.cancel();
            };
        }
        $scope.showTabDialog = function(ev) {
            var data = $scope.data;
            $mdDialog.show({
                controller: DialogController,
                locals: {
                    dataToPass: data
                },
                templateUrl: 'assets/app/travel-map/modal_view.html',
                targetEvent: ev,
                clickOutsideToClose: false
            }).then(function(answer) {}, function() {});
        };
        $scope.datePicker = {
            startDate: new Date(),
            endDate: new Date()
        };
        $scope.temp = api.customers();
        $scope.temp().then().then(function(data) {
            $scope.customers = data.data;
        });
        $scope.temp = api.competitor();
        $scope.competitor = [];
        $scope.temp().then().then(function(data) {
            $scope.competitor = data.data;
        });
        $scope.users = [];
        $scope.temp = api.users();
        $scope.temp().then().then(function(data) {
            $scope.users = data.data;
        });
        $scope.emp_id = '';
        $scope.emppath = [];
        $scope.data = [];
        $scope.mapcenter = api.mapcenter;
        $scope.users = [];
        $scope.mapinfo = [];
        $scope.temp = api.users();
        $scope.temp().then().then(function(data) {
            $scope.users = data.data;
        });
        $scope.mark = false;
        $scope.tempdemo = [];
        for (var i = 0; i < 1000; i++) {
            $scope.tempdemo.push(i);
        }
        $scope.filter = function() {
            if ($scope.data_form.$valid) {
                $http.post('tripSummary/', {
                    data: {
                        start_date: api.CovertDateTime($scope.datePicker.startDate),
                        end_date: api.CovertDateTime($scope.datePicker.endDate),
                        emp_id: $scope.emp_id
                    }
                }).success(function(response) {


                    $scope.data = response.data;

                    $timeout(function() {
                        $scope.$apply();
                    }, 0);
                });
            }
        }
        $scope.$on('$viewContentLoaded', function() {
            $scope.mapshow = true;
        })
    }
})();
