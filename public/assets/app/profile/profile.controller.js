(function() {
    'use strict';



    angular.module('app')
        .controller('ProfileCtrl', ['$rootScope', '$state', '$stateParams', '$mdToast', '$timeout', '$scope', '$mdEditDialog', '$q', '$http', '$mdDialog', 'api', ProfileCtrl])

    function ProfileCtrl($rootScope, $state, $stateParams, $mdToast, $timeout, $scope, $mdEditDialog, $q, $http, $mdDialog, api) {


        $scope.title = 'Profile';
        $scope.apipath = 'profile/';
        $scope.data = {};
        $scope.role = api.getrole();

        $scope.pwd = {};
        $scope.pwd.old = '';
        $scope.pwd.new = '';
        $scope.pwd.confirm = '';

        $scope.mailsts ='';
        $scope.smssts ='';

        var old = [];



        $scope.reset = function() {

            if ($scope.lfApi) {
                $scope.lfApi.removeAll()
            }
            $scope.data_form.$setPristine();
            $scope.data_form.$setUntouched();

            $scope.pwd_reset_form.$setPristine();
            $scope.pwd_reset_form.$setUntouched();

            $scope.data = {};
            $scope.data.profile_image = [];
            $scope.files = [];
            $scope.isFilesNull = true;
            old = [];
            $scope.pwd = {};
            $scope.pwd.old = '';
            $scope.pwd.new = '';
            $scope.pwd.confirm = '';

        $scope.mailsts ='';
        $scope.smssts ='';

            $scope.update_info();
        };


        $scope.ProfileUpdate = function() {


            if ($scope.data_form.$invalid) {

                return;
            }


            // if ($scope.files.length == 1) {
            //     var upload = api.upload($scope.files, 'img');
            //     upload().then().then(function(res) {

            //         var res = angular.fromJson(res);
            //         if (res.status == 'ok') {

            //             old = $scope.data.profile_image;
            //             $scope.data.profile_image = res.data;
            //             $scope.initput();
            //         } else {
            //             $mdDialog.show(
            //                 $mdDialog.alert()
            //                 .title(res.data).ok('OK')
            //             );
            //         }

            //     });

            // } else {
                $scope.initput();
            // }


        }



        $scope.PasswordReset = function() {

            if ($scope.pwd_reset_form.$invalid) {

                return;
            }


            $http.put('change_password_emp', {
                    data: {
                        'old_password': $scope.pwd.old,
                        'new_password': $scope.pwd.new,
                        'confirm_password': $scope.pwd.confirm,
                    }
                })
                .then(
                    function(response) {

                        var res = angular.fromJson(response);


                        if (res.data.status == 'ok') {

                            $mdDialog.show(
                                $mdDialog.alert()
                                .title('Password reset successfully').ok('OK')
                            );

                            $scope.reset();




                        } else {



                            $mdDialog.show(
                                $mdDialog.alert()
                                .title(res.data.data).ok('OK')
                            );


                        }


                    },
                    function(response) {


                    }
                );




        }


        $scope.FileDelete = function(file, ev) {
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title('Are you sure to delete this  File ?')
                .targetEvent(ev)
                .ok('Yes, Delete')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function() {

                $scope.data.profile_image.splice(file, 1);

            }, function() {});

        }



   $scope.TimeZoneResponseMsg = "";

        $scope.TimeZone = function(isValid) {

             if (isValid){
                     var userData=this.ctrl.searchText;
                   
                 $http.post('user/timezone', {
                 data: userData
             }).then(function(response) {
                 var res = angular.fromJson(response);
                  
                 if (res.data.status == 'ok') {
                    $mdDialog.show($mdDialog.alert().title('TimeZone Updated').ok('OK'));
                     
                    
                     if (res.data.data.user_id) {
                          $mdDialog.show($mdDialog.alert().title('TimeZone Updated').ok('OK'));
                         $scope.TimeZoneResponseMsg = "";                        
                     }
                 } else {
                     $scope.TimeZoneResponseMsg = res.data.data;
                     $mdDialog.show($mdDialog.alert().title(res.data.data).ok('OK'));
                    
                 }
             }, function(response) {});
            }
         }

$scope.querySearch =function(query) {
       var deferred = $q.defer();      
           
                var states = $scope.timeZone.filter(function(state) {
                    
                    return (state.desc.toUpperCase().indexOf(query.toUpperCase()) !== -1 || state.desc.toUpperCase().indexOf(query.toUpperCase()) !== -1);
                });

                deferred.resolve(states);                
            return deferred.promise;
    }


           
    $scope.searchTextChange=function(text) {
      $log.info('Text changed to ' + text);
    }

    $scope.selectedItemChange=function(item) {
        console.log(item)
      $log.info('Item changed to ' + JSON.stringify(item));
    }



        $scope.initput = function() {
            

            $http.put('updateprofile', { data: $scope.data })
                .then(
                    function(response) {

                        var res = angular.fromJson(response)

                        if (res.data.status == 'ok') {

                            $mdDialog.show(
                                $mdDialog.alert()
                                .title($scope.title + '  Info Updated').ok('OK')
                            );

                            $scope.update_info();


                        } else {

                            $mdDialog.show(
                                $mdDialog.alert()
                                .title(res.data.data).ok('OK')
                            );


                            $scope.data.profile_image = old;


                        }
                    }

                );
        }



        $scope.update_info = function() {
                
                $http.get('gettimezone/').then(function (response){
                $scope.timeZone=angular.fromJson(response).data;
               console.log($scope.timeZone);                
            });

            $http.get($scope.apipath)
                .then(
                    function(response) {

                        var res = angular.fromJson(response);



                        if (res.data.status == 'ok') {

                            $scope.data = res.data.data;

                            $scope.data.profile_image = angular.fromJson($scope.data.profile_image);
                $rootScope.timeData=$scope.data;
                console.log($rootScope.timeData);
                
                //var asd = angular.element('#timezone').val($scope.data.timezonename);
                var mailnote= $rootScope.timeData.mailnote;
                
                if(mailnote=="true"){                   
                    $scope.mail=true;
                    $scope.data.mailsts="enabled";
                }
                else
                {
                    $scope.data.mailsts="disabled";
                    
                }
                var smsnote= $rootScope.timeData.smsnote;
                if(smsnote=="true"){
                    $scope.sms=true;
                    $scope.data.smssts="enabled";
                    console.log($scope.smssts);
                }
                else
                {
                    $scope.data.smssts="disabled";
                }
                if($rootScope.timeData.is_multipick == 1){
                    $scope.multipick=1;
                    $scope.multipicksts=true;

                }

                        } else {


                        }
                    },
                    function(response) {

                    }
                );

}

            $scope.geocodeupdatelocpickup = function() {

                $http.get($scope.apipath)
                .then(
                    function(response) {

                        var res = angular.fromJson(response);

                        var address = res.data.data.street;
               
                if (address) {
                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode({
                        "address": address
                    }, function(results, status) {
                        if (status == google.maps.GeocoderStatus.OK && results.length > 0) {
                            var location = results[0].geometry.location;
                          //  console.log(results);
                            $scope.getpickuppos(location);
                        } else {}
                    });
                }

            
        });
            }
            $scope.geocodeupdatelocpickup1 = function() {


                var address = $scope.data.street;
               
                if (address) {
                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode({
                        "address": address
                    }, function(results, status) {
                        if (status == google.maps.GeocoderStatus.OK && results.length > 0) {
                            var location = results[0].geometry.location;
                            console.log(results);
                            $scope.getpickuppos(location);
                        } else {}
                    });
                }

            }


             $scope.getpickuppos = function(data, zoom,$status) {
                var zoom = zoom || 18;
                console.log(data);


$scope.map = {
      center: {
        latitude: 21.0000,
        longitude: 78.0000
      },
      zoom: 4,
      events: {
        click: function(mapModel, eventName, originalEventArgs,ok) {
          var e = originalEventArgs[0]; 

            var geocoder = new google.maps.Geocoder();
            var latlng = new google.maps.LatLng(e.latLng.lat(), e.latLng.lng());

            geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[1]) {

                            console.log(results[1].formatted_address); // details address
                        } else {
                            console.log('Location not found');
                        }
                    } else {
                        console.log('Geocoder failed due to: ' + status);
                    }
                });


        }
      }
    };



                $scope.pickup_ladd = data.lat();
                $scope.pickup_long = data.lng();
                $scope.mapzoom = zoom;
                $timeout(function() {
                    $scope.$apply();
                }, 0);
                        $scope.showpickupmap = true;

            };
                            $scope.geocodeupdatelocpickup();

  $scope.showpickupmap1=function()
            {
                $scope.showpickupmap = false;
            }
            $scope.showpickupmap2=function()
            {
                $scope.showpickupmap = true;
            }

            $scope.addtolist=function(){
                if(!$scope.add){
                    $scope.records = $scope.data.records;
                   // console.log($scope.data.records);
                var pictime=moment($scope.data.picktime).format("YYYY-MM-DD, h:mm A")
                var data = {
                    pic_loc: $scope.data.pick_address,
                    pic_date: pictime,
                    pic_item: $scope.data.notes,
                    pic_ladd : $scope.data.pickup_ladd,
                    pic_long : $scope.data.pickup_long
                };
                 $scope.records.push(data);
                 $scope.data.pick_address = '';
                 $scope.data.notes = '';
                 $scope.data.records = $scope.records;
                }else{
                  var pictime=moment($scope.data.picktime).format("YYYY-MM-DD, h:mm A")
                  var data = {
                    pic_loc: $scope.data.pick_address,
                    pic_date: pictime,
                    pic_item: $scope.data.notes,
                    pic_long : $scope.data.pickup_ladd,
                    pic_ladd : $scope.data.pickup_long
                };
                 $scope.records.push(data);
                 $scope.data.pick_address = '';
                 $scope.data.notes = '';
                $scope.data.records = $scope.records;
                }
                $scope.data.pickup_ladd
            }
            $scope.editpickup = function(index){
                
                var datalist = $scope.data.records[index];
                $scope.data.pick_address = datalist.pic_loc;
                $scope.data.notes = datalist.pic_item; 
                $scope.data.pickup_ladd = datalist.pic_ladd;
                $scope.data.pickup_long = datalist.pic_long; 
                 $scope.data.records.splice(index,1);

            }
            $scope.deletepickup = function(index){
                $scope.data.records.splice(index,1);

            }


$scope.geocodeupdateloc1 = function() {
                var address = $scope.data.cust_address;
                if (address) {
                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode({
                        "address": address
                    }, function(results, status) {
                        if (status == google.maps.GeocoderStatus.OK && results.length > 0) {
                            var location = results[0].geometry.location;
                            $scope.getpos(location);
                        } else {}
                    });
                }
            }
             
            $scope.geocodeupdatelocsender = function() {
                var address = $scope.data.sent_address;
                if (address) {
                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode({
                        "address": address
                    }, function(results, status) {
                        if (status == google.maps.GeocoderStatus.OK && results.length > 0) {
                            var location = results[0].geometry.location;
                            $scope.getsenderpos(location);
                        } else {}
                    });
                }
            }
            $scope.getsenderpos = function(data, zoom,status) {
                var zoom = zoom || 18;
                 if(status){
                  var geocoder = new google.maps.Geocoder();
            var latlng = new google.maps.LatLng(data.lat(), data.lng());

            geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[1]) {
                            $scope.data.sent_address= results[1].formatted_address;
                            $scope.data.sent_ladd = data.lat();
                            $scope.data.sent_long = data.lng();
                            $scope.mapzoom = zoom;
                            $scope.$apply();
                            console.log(results[1].formatted_address); // details address
                        } else {
                            console.log('Location not found');
                        }
                    } else {
                        console.log('Geocoder failed due to: ' + status);
                    }
                });
        }
                $scope.data.sent_ladd = data.lat();
                $scope.data.sent_long = data.lng();
                $scope.mapzoom = zoom;
                $timeout(function() {
                    $scope.$apply();
                }, 0);
            };
            
             
 
             $scope.setadd = function()
             {
                $scope.getpickuppos();
             };
            $scope.getpos = function(data, zoom,status) {
                var zoom = zoom || 18;
                 if(status){
                 var geocoder = new google.maps.Geocoder();
            var latlng = new google.maps.LatLng(data.lat(), data.lng());

            geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[1]) {
                            $scope.data.cust_address= results[1].formatted_address;
                            $scope.data.pickup_ladd = data.lat();
                            $scope.data.pickup_long = data.lng();
                            $scope.mapzoom = zoom;
                            $scope.$apply();
                            console.log(results[1].formatted_address); // details address
                        } else {
                            console.log('Location not found');
                        }
                    } else {
                        console.log('Geocoder failed due to: ' + status);
                    }
                });
        }
                $scope.data.pickup_ladd = data.lat();
                $scope.data.pickup_long = data.lng();
                $scope.mapzoom = zoom;
                $timeout(function() {
                    $scope.$apply();
                }, 0);


}


        $scope.update_info();



    }
})();
