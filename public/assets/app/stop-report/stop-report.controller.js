(function() {
    'use strict';
    angular.module('app').controller('StopReportCtrl', ['$rootScope', '$state', '$stateParams', '$mdToast', '$timeout', '$scope', '$interval', '$q', '$http', '$mdDialog', 'api', StopReportCtrl])

    function StopReportCtrl($rootScope, $state, $stateParams, $mdToast, $timeout, $scope, $interval, $q, $http, $mdDialog, api) {
        $scope.mapshow = false;
        $scope.data = {
            "visits": 0,
            "cases": 0,
            "orders": 0,
        };

        function DialogController($scope, $mdDialog, dataToPass) {
            $scope.data = dataToPass;
            $scope.map = true;
            $timeout(function() {
                $scope.$apply();
            }, 0);
            $scope.hide = function() {
                $mdDialog.hide();
            };
            $scope.cancel = function() {
                $mdDialog.cancel();
            };
        }
        $scope.openMap = function(lat, lng, ev) {
            var data = {
                'lat': lat,
                'lng': lng
            };
            $mdDialog.show({
                controller: DialogController,
                locals: {
                    dataToPass: data
                },
                templateUrl: 'assets/app/stop-report/modal_view.html?' + Math.random(),
                targetEvent: ev,
                clickOutsideToClose: false
            }).then(function(answer) {}, function() {});
        };
        $scope.datePicker = {
            startDate: new Date(),
            endDate: new Date()
        };
        $scope.temp = api.customers();
        $scope.temp().then().then(function(data) {
            $scope.customers = data.data;
        });
        $scope.vehicle = [];
        $scope.temp = api.competitor();
        $scope.temp().then().then(function(data) {
            $scope.vehicle = data.data;
        });
        $scope.users = [];
        $scope.getRelativeTime = api.getRelativeTime;
        $scope.temp = api.users();
        $scope.temp().then().then(function(data) {
            $scope.users = data.data;
        });
        $scope.emp_id = '';
        // $scope.start_date = new Date();
        // $scope.end_date = new Date();
        $scope.emppath = [];
        $scope.data = [];
        $scope.mapcenter = api.mapcenter;
        $scope.users = [];
        $scope.mapinfo = [];
        $scope.temp = api.users();
        $scope.temp().then().then(function(data) {
            $scope.users = data.data;
        });


        $scope.DatePickerOptions = api.DatePickerOptions;


        $scope.addSecs = function(data,secs)
        {
            var date  =  moment(data).add(secs, 'seconds');
             return  date.format('Do MMM YY, h:mm A');
        }





        $scope.mark = false;
        $scope.tempdemo = [];
        for (var i = 0; i < 1000; i++) {
            $scope.tempdemo.push(i);
        }
        $scope.filter = function() {
            if ($scope.data_form.$valid) {
                $http.post('vehicle_stop_api/' + $scope.emp_id, {
                    data: {
                        start_date: api.CovertDateTime($scope.datePicker.startDate),
                        end_date: api.CovertDateTime($scope.datePicker.endDate),
                        emp_id: $scope.emp_id,
                    }
                }).success(function(response) {
                    $scope.data = response.data;
                    var start = [];
                    var end = [];
                    $timeout(function() {
                        $scope.$apply();
                    }, 0);
                });
            }
        }
        $scope.$on('$viewContentLoaded', function() {
            $scope.mapshow = true;
        })
    }
})();
