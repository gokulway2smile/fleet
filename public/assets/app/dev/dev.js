(function() {
    'use strict';
    angular.module('app').controller('DDashboardCtrl', ['$scope', '$state', '$mdEditDialog', '$q', '$timeout', '$http', '$mdDialog', 'api', DashboardCtrl])

    function DashboardCtrl($scope, $state, $mdEditDialog, $q, $timeout, $http, $mdDialog, api) {

        function DialogController($scope, $mdDialog, dataToPass, api, $window, $mdSelect) {
            $scope.dateOptions = {
                timePicker: true,
                timePickerIncrement: 5,
                autoUpdateInput: false,
                singleDatePicker: true,
                locale: {
                    format: 'Do MMM YY, h:mm A'
                },
                eventHandlers: {
                    'cancel.daterangepicker': function(ev, picker) {
                        console.log(picker)
                        console.log(ev)
                    }
                }
            };
//            $scope.task.driver=true;

            $scope.DriverSearch=true;
            $scope.showForm = true;
            $scope.mapp1=true;
            if (dataToPass.mode == 'TaskView') {
                $scope.showForm = false;
                $scope.data = dataToPass.data;
            } else {
                $scope.apipath = 'task/';
                $scope.apipath1 = 'profile/';
                $scope.temp = api.users();
                $scope.temp().then().then(function(data) {
                    $scope.users = data.data;
                });
                $scope.alert = '';
                $scope.TripMap = false;
                $scope.mapzoom = 18;
                $scope.customers = [];
                $scope.schedule = api.schedule;
                $scope.tempcus = api.customers();
                $scope.place_type = api.place_type;
                $scope.method = api.method;
                $scope.tempcus().then().then(function(data) {
                    $scope.customers = data.data;
                });
                // $scope.datePicker = {
                //     startDate: new Date(),
                //     endDate: new Date()
                // };
                $scope.data = {};



                $http.get('gettimezone/').then(function (response){
                 var timeZone=angular.fromJson(response).data;
                  
                  $http.get($scope.apipath1)
                .then(
                    function(response) {

                        var res = angular.fromJson(response);
                    
                        var user_timezone = res.data.data.timezonename;

                    timeZone.forEach(function(time)
                    {   

                       if(time.desc==user_timezone)
                       {
                       var utc_zone= time.utc_zone;  
                        var minus = "-";

                        if (utc_zone.indexOf(minus) >=0) {
                            
                             var minus_time  = utc_zone.substr(1);
                                    
                                    var final_time = -Math.abs(minus_time);                                    
                                   
                        }
                        else
                        {
                            var plus_time  = utc_zone.substr(1);
                            var final_time = Math.abs(plus_time);
                            
                        }

                        var time = moment.utc().utcOffset(final_time); 

                        console.log(time.toISOString()); 
                        
                        $scope.data.start_dt = time;
                          
                       }
                });
            });              

            });     

                $scope.data.start_dt1 = moment();               
                
                $scope.data.end_dt = "";
                $scope.add = true;
                $scope.data.cust_jobs = [];
                $scope.data.stoppage_limit = "00:00";
                $scope.scheduletypes = api.scheduletypes;
                $scope.StoppageTime = [];
                $scope.StoppageTime = api.StoppageTime();
                $scope.companytype = [];
                $scope.method = ['Pickup', 'Drop'];
                $scope.companytype = api.companytype;
                $scope.start_Date ='sdfdsffd';
                $scope.addNewChoice = function(b = '') {
                    $scope.data.cust_jobs.push({
                        cust_id: b,
                        type: 0,
                        notes: '',
                        method: 'Pickup',
                    });
                };
                $scope.removeChoice = function(data) {
                    // var lastItem = $scope.cust_jobs.length - 1;
                    console.log(data);
                    if (data > -1) {
                        $scope.data.cust_jobs.splice(data, 1);
                    }
                };
                $scope.competitorinfo = [];
                $scope.tempcompetitor = api.competitor();
                $scope.tempcompetitor().then().then(function(data) {
                    $scope.competitorinfo = data.data;
                });
                $scope.customersChange = function() {
                    var index = _.findIndex($scope.customers, function(item) {
                        return item.id == $scope.data.cust_id
                    })
                    var choosenCustomer = $scope.customers[index];
                    if (choosenCustomer) {
                        $scope.data.cust_phone = choosenCustomer.contact_no;
                        $scope.data.cust_email = choosenCustomer.email;
                        $scope.data.cust_name = '';
                        $scope.data.cust_address = choosenCustomer.address;
                        $scope.data.loc_lat = choosenCustomer.loc_lat;
                        $scope.data.loc_lng = choosenCustomer.loc_lng;
                    }
                }
                $scope.emptyCustomerInfo = function(id) {
                    $scope.data.cust_phone = '';
                    $scope.data.cust_email = '';
                    $scope.data.cust_name = '';
                    $scope.data.cust_address = '';
                    $scope.data.loc_lat = 0;
                    $scope.data.loc_lng = 0;
                    $scope.data.cust_id = '';
                };
                $scope.geocodeupdateloc1 = function() {
                    var address = $scope.data.cust_address;
                    if (address) {
                        var geocoder = new google.maps.Geocoder();
                        geocoder.geocode({
                            "address": address
                        }, function(results, status) {
                            if (status == google.maps.GeocoderStatus.OK && results.length > 0) {
                                var location = results[0].geometry.location;
                                $scope.getpos(location);
                            } else {}
                        });
                    }
                }
                $scope.mapzoom = 18;
                $scope.clat = 40.74;
                $scope.clng = -74.18;
                $scope.getpos = function(data, zoom = 18) {
                    $scope.data.loc_lat = data.lat();
                    $scope.data.loc_lng = data.lng();
                    $scope.mapzoom = zoom;
                    $timeout(function() {
                        $scope.$apply();
                    }, 0);
                };
                $scope.$watch('data.cust_jobs', function(newVal, oldVal) {
                    $scope.mapArray = [];
                    var temp = 1;
                    angular.forEach($scope.data.cust_jobs, function(key, value) {
                        var data = _.find($scope.customers, {
                            id: parseInt(key.cust_id)
                        })
                        if (data) {
                            if (temp == 1) {
                                $scope.clat = data.loc_lat;
                                $scope.clng = data.loc_lng;
                                temp = 0;
                            }
                            $scope.mapArray.push({
                                "id": data.id,
                                "loc_lat": data.loc_lat,
                                "loc_lng": data.loc_lng,
                            })
                        }
                    })
                }, true);
                if (dataToPass.mode == 'TripEdit') {
                    $http.get($scope.apipath + dataToPass.trip_id).then(function(response) {
                        var res = angular.fromJson(response);
                        if (res.data.status == 'ok') {
                            $scope.data = res.data.data;
                            $scope.data.uploads = angular.fromJson($scope.data.uploads);
                            $scope.start_dt = moment($scope.data.start_dt)._d;

                            $scope.end_dt = moment($scope.data.end_dt)._d;
                            var temp = $scope.data.cust_jobs;
                            $scope.data.cust_jobs = [];
                            angular.forEach(angular.fromJson(temp), function(value, key) {
                                var type = _.values(_.omitBy(_.map($scope.customers, function(v, index) {
                                    if (v.id == value.cust_id) {
                                        return v.type;
                                    }
                                }), _.isNil))[0];
                                if (type) {
                                    value.type = type;
                                }
                                $scope.data.cust_jobs.push({
                                    cust_id: parseInt(value.cust_id),
                                    id: parseInt(value.id),
                                    lat: value.lat,
                                    lng: value.lng,
                                    notes: value.notes,
                                    status: value.status,
                                    method: value.method,
                                    timestamps: value.timestamps,
                                    type: value.type,
                                    delivery_to: value.delivery_to,
                                    delivery_phone: value.delivery_phone,
                                    is_cust_delivery: value.is_cust_delivery,
                                    remarks: value.remarks,
                                    signature: value.signature
                                });
                            });
                            //$scope.data.cust_jobs.push({ cust_id: b, type: 0, notes: '' });
                            //   $scope.data.date = new Date($scope.data.date);
                        } else {}
                    }, function(response) {
                        // failure call back
                    });
                }
                else
                {
                       $scope.addNewChoice();
                }
                $scope.submit_form = function() {
                    if ($scope.data_form1.$invalid) {
                        return;
                    }
                    if ($scope.data.cust_jobs.length == 0) {
                        $scope.alert = "No Tasks in This Schedule";
                        return;
                    }
                    if (api.checkdups($scope.data.cust_jobs, 'cust_id', 'int')) {
                        $scope.alert = "Duplicate Customers not allowed";
                        return;
                    }
                    if (dataToPass.mode == 'TripEdit') {
                        $scope.data.start_dt = api.CovertDateTime($scope.data.start_dt);
                        $scope.data.end_dt = api.CovertDateTime($scope.data.end_dt);
                        $http.put($scope.apipath + $scope.data.id, {
                            data: $scope.data
                        }).then(function(response) {
                            var res = angular.fromJson(response)
                            if (res.data.status == 'ok') {
                                $mdDialog.hide('reload');
                                $mdDialog.show($mdDialog.alert().title(res.data.data).ok('OK'));
                            } else {
                                $mdDialog.show($mdDialog.alert().title(res.data.data).ok('OK'));
                            }
                        });
                    } else {


                        $http.post($scope.apipath, {
                            data: $scope.data
                        }).then(function(response) {
                            var res = angular.fromJson(response);
                            if (res.data.status == 'ok') {
                                $mdDialog.hide('reload');
                                $mdDialog.show($mdDialog.alert().title(res.data.data).ok('OK'));
                            } else {
                                $scope.alert = res.data.data;
                            }
                        }, function(response) {
                            // failure call back
                        });
                    }
                };
            }
            $scope.hide = function() {
                $mdDialog.hide();
            };
            $scope.cancel = function() {
                $mdDialog.cancel();
            };
        }
        $scope.showTabDialog = function(ev, add, id) {
            var add = add || true;
            if (add == true) {
                var data = false;

                $mdDialog.show({
                    controller: DialogController,
                    locals: {
                        dataToPass: data
                    },
                    templateUrl: 'assets/app/dev/modal_view.html?' + Math.random(),
                    targetEvent: ev,
                    clickOutsideToClose: false
                }).then(function(answer) {
                    if (answer == 'reload') {
                        $scope.updateData();
                    }
                }, function() {});
            } else if (add == 11) {
                var data = {
                    'mode': 'TaskView',
                    'data': id
                };
                $mdDialog.show({
                    controller: DialogController,
                    locals: {
                        dataToPass: data
                    },
                    templateUrl: 'assets/app/dev/modal_view.html?' + Math.random(),
                    targetEvent: ev,
                    clickOutsideToClose: false
                }).then(function(answer) {
                    if (answer == 'reload') {
                        $scope.updateData();
                    }
                }, function() {});
            } else {
                var data = {
                    'mode': 'TripEdit',
                    'trip_id': id,
                };
                $mdDialog.show({
                    controller: DialogController,
                    locals: {
                        dataToPass: data
                    },
                    templateUrl: 'assets/app/dev/modal_view.html?' + Math.random(),
                    targetEvent: ev,
                    clickOutsideToClose: false
                }).then(function(answer) {
                    if (answer == 'reload') {
                        $scope.updateData();
                    }
                }, function() {});
            }
        };
        $scope.customers = [];
        $scope.users = [];
        $scope.tasksData = {};
        $scope.tasksData.data = [];
        $scope.task = {};
        $scope.cust_id = '';
        $scope.task.driver = true;
        $scope.showTrafficLayer = false;
        $scope.startDate='2014-06-01T12:00:00Z';
        $scope.taskFilter = '';
        $scope.taskFilterSt = false;
        $scope.DriverFilter = '';
        $scope.DriverFilterSt = false;
        $scope.RouteOrigin = api.mapcenter;
        $scope.SideBar = true;
        $scope.trackingMap = true;
        $scope.date = new Date();
        $scope.apipath = 'task/';
        $scope.driverDefaultImg = 'assets/images/canva-man-delivering-boxes-on-scooter-icon-MAB60VR-Abk.png';
        $scope.updateData = function() {
            $scope.tempcus = api.customers();
            $scope.tempcus().then().then(function(data) {
                $scope.customers = data.data;
            });
            $scope.tempusers = api.users();
            $scope.tempusers().then().then(function(data) {
                $scope.users = data.data;
            });
            $scope.competitorinfo = [];
            $scope.tempcompetitor = api.competitor();
            $scope.tempcompetitor().then().then(function(data) {
                $scope.competitorinfo = data.data;
            });
            var tepDate = moment($scope.date).format('YYYY-MM-DD');
            $http.get($scope.apipath + '?date=' + tepDate + '&mobile_mode=yes').then(function(response) {
                var res = angular.fromJson(response);
                if (res.data.status == 'ok') {
                    console.log(res.data.data);
                    $scope.tasksData.data = res.data.data;


                } else {}
            }, function(response) {
                // failure call back
            });
            if ($scope.TaskView) {
                if ($scope.TaskView.id) {
                    $http.get($scope.apipath + $scope.TaskView.id + '?subtask=true').then(function(response) {
                        var res = angular.fromJson(response);
                        if (res.data.status == 'ok') {
                            $scope.ToggleTrackingView(res.data.data, 'update');
                        } else {
                            $scope.ToggleTrackingView();
                        }
                    }, function(response) {
                        // failure call back
                    });
                }
            }
        }
        $scope.parseJsonProfile = function(data) {
            if (data) {
                data = angular.fromJson(data);
                if (data.length) {
                    return data[0];
                }
            }
            return $scope.driverDefaultImg;
        }
        $scope.updateData();
        $scope.task_info = function(data) {
            console.log(data);
            var info = '<p>ID :#' + data.id + '</p> ' + '<p>Name :' + data.cust_name + '</p> ' + '<p>Items :' + data.notes + ' ' + '</p> ' + '<p>Status :' + ' ' + data.status + '</p> ';
            if (data.timestamps) {
                info = info + '<p>  Delivered - Date / Time  : ' + data.timestamps + ' </p>';
            }
            if (data.allocated_emp) {
                info = info + '<p>  Delivery Agent : ' + data.allocated_emp + ' </p>';
            }
            return data;
        }
        $scope.RouteDraw = function(data) {
            if (data.allocated_emp) {
                var index = _.findIndex($scope.onlineemp, function(item) {
                    return item.user_id == data.allocated_emp
                })
                if (index > -1) {
                    $scope.RouteDestination = [data.loc_lat, data.loc_lng];
                    $scope.RouteOrigin = [lat, lng];
                    $scope.$apply();
                }
            }
        }
        $scope.emp_info = function($data) {
            var info = '<p>Emp id :' + $data['user']['user_id'] + '</p> ' + '<p>Name :' + $data['user']['first_name'] + ' ' + $data['user']['last_name'] + '</p> ' + '<p> Active : ' + api.timeago($data['timestamp']) + ' </p>';
            return info;
        }
        $scope.GetIcons = function(status) {
            if (status == "Unallocated") {
                return '/assets/images/Unallocated.png';
            } else if (status == "Assigned") {
                return '/assets/images/Allocated.png';
            } else if (status == "In-Progress") {
                return '/assets/images/On-Progress.png';
            } else if (status == "Incomplete") {
                return '/assets/images/Delivered.png';
            }
            // else if (status == "Completed") {
            //     return '/assets/images/Home.png';
            // }
            else if (status == "Completed") {
                return '/assets/images/Delivered.png';
            } else if (status == "Declined") {
                return '/assets/images/Canceled.png';
            } else if (status == "Canceled") {
                return '/assets/images/Canceled.png';
            } else {
                return '/assets/images/Allocated.png';
            }
        }
        $scope.GetEmpIcons = function(emp) {
            // return 'https://openclipart.org/image/2400px/svg_to_png/203366/top-down-bike-racing-monsterbraingames.png'
            var name = 'Offline';
            if (emp) {
                if (emp.user_id) {
                    var status = $scope.getEmpStatusCheck(emp.user_id);
                    if (status == "Free") {
                        var name = 'Free';
                    } else if (status.indexOf("Busy") >= 0) {
                        var name = 'Busy';
                    } else {
                        var name = 'Offline';
                    }
                    if (emp.compass_direction) {
                        if (emp.compass_direction <= 180) {
                            return 'assets/images/' + name + '_left.png';
                        } else {
                            return 'assets/images/' + name + '_right.png';
                        }
                    } else {
                        return 'assets/images/' + name + '_right.png';
                    }
                }
            }
            return 'assets/images/' + name + '_right.png';
        }
        $scope.taskStatus = function() {
            if ($scope.taskFilter == '') {
                $scope.taskFilterSt = false;
            }
        }
        $scope.mapcenter = api.mapcenter;
        $scope.schedule_status = angular.copy(api.schedule);
        $scope.address = '';
        $scope.schedule_status.unshift('');
        $scope.zoom = 18;
        $scope.mapshow = true;
        $scope.pan = function(task, type,index) {
            var type = type || 0;            
            if (task && type == 'task') {
                $scope.mapcenter = [task.tasks[index].cust_info.loc_lat, task.tasks[index].cust_info.loc_lng];
                console.log($scope.mapcenter);
                $scope.zoom = 18;
                $scope.RouteDraw(task);
            }
        }
        $scope.DriverFilterStatus = function() {
            if ($scope.DriverFilterV == 'All') {
                $scope.DriverFilterSt = false;
                $scope.DriverFilter = '';
            }
            if ($scope.DriverFilterV == 'Busy') {
                $scope.DriverFilterSt = true;
                $scope.DriverFilter = 'Busy';
            }
            if ($scope.DriverFilterV == 'Free') {
                $scope.DriverFilterSt = true;
                $scope.DriverFilter = 'Free';
            }
        }
        $scope.DriverGPSFilterStatus = function() {
            // if($scope.DriverGPSFilterV == 'All')
            // {
            //      $scope.DriverGPSFilter='';
            // }
            // if($scope.DriverGPSFilterV == 'Online')
            // {
            //      $scope.DriverGPSFilter='Online';
            // }
            // if($scope.DriverGPSFilterV == 'Offline')
            // {
            //      $scope.DriverGPSFilter='Offline';
            // }
            // if($scope.DriverGPSFilterV == 'Inactive')
            // {
            //      $scope.DriverGPSFilter='Inactive';
            // }
        }
        $scope.GetSpeedStatus = function(user_id) {
            if ($scope.onlineemp) {
                var data = _.find($scope.onlineemp, function(item) {
                    if (item.user_id == user_id) {
                        return true;
                    } else {
                        return false;
                    }
                })
                if (data) {
                    if (data.speed) {
                        if (Math) {
                            data.speed = Math.round(data.speed);
                        } else {}
                        return data.speed + ' kph';
                    } else {
                        return 'No Data';
                    }
                } else {
                    return 'No Data';
                }
            } else {
                return 'No Data';
            }
        }
        $scope.GetBatteryStatus = function(user_id) {
            if ($scope.onlineemp) {
                var data = _.find($scope.onlineemp, function(item) {
                    if (item.user_id == user_id) {
                        return true;
                    } else {
                        return false;
                    }
                })
                if (data) {
                    if (data.battery_status) {
                        return data.battery_status + '%';
                    } else {
                        return 'No Data';
                    }
                } else {
                    return 'No Data';
                }
            } else {
                return 'No Data';
            }
        }
        $scope.MobileLast = function(data) {
            if (data == 'Active') {
                return '#256725';
            } else if (data == 'In Active') {
                return '#2e43ff';
            } else {
                return '#693f3f';
            }
        }
        $scope.getVehicleInfo = function(user_id) {
            if ($scope.onlineemp) {
                var data = _.find($scope.onlineemp, function(item) {
                    if (item.user_id == user_id) {
                        return true;
                    } else {
                        return false;
                    }
                })
                if (data) {
                    var vehicle = _.find($scope.competitorinfo, ['id', parseInt(data.vehicle_no)]);
                    if (!vehicle) {
                        return 'No Vehicle';
                    } else {
                        return vehicle.name;
                    }
                } else {
                    return 'No Vehicle';
                }
            } else {
                return 'No Vehicle';
            }
        }
        $scope.getVehicleColor = function(user_id) {
            if ($scope.onlineemp) {
                var data = _.find($scope.onlineemp, function(item) {
                    if (item.user_id == user_id) {
                        return true;
                    } else {
                        return false;
                    }
                })
                if (data) {
                    var vehicle = _.find($scope.competitorinfo, ['id', parseInt(data.vehicle_no)]);
                    if (!vehicle) {
                        return '#00326a';
                    } else {
                        return vehicle.desc;
                    }
                } else {
                    return '#00326a';
                }
            } else {
                return '#00326a';
            }
        }
        $scope.getLastActivity = function(e, color, timestamp) {
            var color = color || false;
            var timestamp = timestamp || false;
            if ($scope.onlineemp) {
                var data = _.find($scope.onlineemp, function(item) {
                    if (item.user_id == e) {
                        return true;
                    } else {
                        return false;
                    }
                })
                if (data) {
                    if (timestamp) {
                        return api.onlineTimeAgo(data.timestamp);
                    }
                    var check = new Date(data.timestamp);
                    var now = moment(new Date());
                    if (moment(check).format("YYYY-MM-DD") == moment().format("YYYY-MM-DD")) {
                        var diffmm = now.diff(check, 'minutes');
                        // .format("mm");
                        if (parseInt(diffmm) < 10 && parseInt(diffmm) > 0) {
                            if (color) {
                                return '#256725';
                            } else {
                                return "Online";
                            }
                        } else {
                            if (color) {
                                return '#2e43ff';
                            } else {
                                return "Inactive";
                            }
                        }
                    } else {
                        if (color) {
                            return '#693f3f';
                        } else {
                            return "No Data";
                        }
                    }
                }
            }
            if (color) {
                return '#693f3f';
            } else {
                return "No Data";
            }
        }
        // $scope.getEmpStatusCheck
        $scope.criteriaMatch = function(criteria) {
            return function(item) {
                var status = $scope.getEmpStatusCheck(item.user_id);
                if ($scope.DriverFilter == 'Busy' && status == 'Busy') {
                    return true;
                } else if ($scope.DriverFilter == 'Free' && status == 'Free') {
                    return true;
                } else if ($scope.DriverFilter == '') {
                    return true;
                } else {
                    return false;
                }
            };
        };
        $scope.criteriaMatchGPS = function(criteria) {
            return function(item) {
                // var status = $scope.getLastActivity(item.user_id);
                if ($scope.DriverGPSFilterV == item.activity) {
                    return true;
                } else if ($scope.DriverGPSFilterV == 'All') {
                    return true;
                } else {
                    return false;
                }
                // if ($scope.DriverGPSFilterV == 'Active' && status == 'Active') {
                //     return true;
                // } else if ($scope.DriverGPSFilterV == 'In Active' && status == 'Inactive') {
                //     return true;
                // } else if ($scope.DriverGPSFilterV == 'Offline' && status == 'No Data') {
                //     return true;
                // } else if ($scope.DriverGPSFilterV == 'All') {
                //     return true;
                // } else {
                //     return false;
                // }
            };
        };

        $scope.TaskStatusColor = function(status) {
           if (status == "Unallocated") {
               return "#66bb6a";
           } else if (status == "Assigned") {
               return "#043507";
           } else if (status == "In-Progress") {
               return "Green";
           } else if (status == "Incomplete") {
               return "#924324";
           } else if (status == "Completed") {
                return "#8c8181";
           }  else if (status == "Delivered") {
               return "#8c8181";
           } else if (status == "Canceled") {
               return "#c90505";
           } else {
               return "#4f4f9c";
           }
       }

        
        $scope.getEmpStatusCheck = function(e, JobOnly) {

            var JobOnly = JobOnly || false;
            if ($scope.tasksData.data) {
                var tasks = 0;
                var data = _.find($scope.tasksData.data, function(item) {
                    if ((item.allocated_emp_id == e) && (item.status == 'In-Progress' || item.status == 'Assigned')) {
                        return true;
                    } else {
                        return false;
                    }
                })
                if (data) {
                    if (JobOnly) {
                        angular.forEach($scope.tasksData.data, function(item, index) {
                            if ((item.allocated_emp_id == e) && (item.status == 'In-Progress' || item.status == 'Assigned')) {
                                tasks++;
                            }
                        });
                        return '(' + tasks + ' Task)';
                    }
                    return 'Busy';
                }
            }
            if (JobOnly) {
                return '';
            }
            return 'Free';
        }
        // $scope.driverBusy = false;
        // $scope.driverFree = false;
        // $scope.driverAll = true;
        // $scope.filterEmpStatus = function(val) {
        //     if (val == 1) {
        //         $scope.driverFree = false;
        //         $scope.driverBusy = false;
        //     } else if (val == 2) {
        //         $scope.driverAll = false;
        //         $scope.driverBusy = false;
        //     } else if (val == 3) {
        //         $scope.driverFree = false;
        //         $scope.driverAll = false;
        //     } else {}
        // };



        if (api.manager()) {
            $scope.user_id = '';
            $scope.profile = api.profile();
            $scope.profile().then().then(function(data) {
                $scope.user_id = data.data.user_id;
                var geocoder = new google.maps.Geocoder();
                geocoder.geocode({
                    "address": data.data.street
                }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK && results.length > 0) {
                        var location = results[0].geometry.location;
                        $scope.mapcenter = location;
                        $scope.zoom = 20;
                        $scope.$apply();
                        // $scope.getpos(location);
                    } else {}
                });
                window.Echo.private(globalapp.channel + 'user-' + $scope.user_id).listen('LocationUpdate', (e) => {
                    var index = _.findIndex($scope.onlineemp, function(item) {
                        return item.user_id == e.data.user_id
                    })
                    var index1 = _.findIndex($scope.users, function(item) {
                        return item.user_id == e.data.user.user_id
                    })
                    if (index1 > -1) {
                        $scope.users[index1] = e.data.user;
                    }
                    if (index > -1) {
                        $scope.onlineemp[index] = e.data;
                    } else {
                        $scope.onlineemp.push(e.data);
                    }
                    $scope.$apply();
                }).listen('TaskUpdateEvent', (e) => {
                    if (e.data) {
                        var index1 = _.findIndex($scope.tasksData.data, function(item) {
                            return item.id == e.data.id;
                        })
                        if (index1 > -1) {
                            $scope.tasksData.data[index1] = e.data;
                            $scope.$apply();
                        }
                    }
                });
            });
        } else {
            if (api.superAdmin()) {
                var domain = api.getSubDomain() + '-';
            } else {
                var domain = globalapp.channel;
            }
            window.Echo.private(domain + 'current_location').listen('LocationUpdate', (e) => {
                var index = _.findIndex($scope.onlineemp, function(item) {
                    return item.user_id == e.data.user_id
                })
                console.log(index);
                if (index > -1) {
                    $scope.onlineemp[index] = e.data;
                } else {
                    $scope.onlineemp.push(e.data);
                }
                $scope.$apply();
            });
        }

        function GetOnlineEmp() {
            if ($state.current.name == 'admin.dev' || $state.current.name == 'admin.company_dashboard') {
                $http.get('location_online_emp').success(function(response) {
                    $scope.onlineemp = [];

                    angular.forEach(response.data, function(element) {
                        $scope.onlineemp.push(element);
                    });
                });
                // // $timeout(GetOnlineEmp, 10000);
            } else {}
        }
        $scope.$on('$viewContentLoaded', function() {
            GetOnlineEmp();
        })
        $scope.geocodeupdateloc = function() {
            if ($scope.address) {
                var geocoder = new google.maps.Geocoder();
                geocoder.geocode({
                    "address": $scope.address
                }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK && results.length > 0) {
                        var location = results[0].geometry.location;
                        $scope.mapcenter = location;
                        $scope.zoom = 20;
                        $timeout(function() {
                            $scope.$apply();
                        }, 10);
                        // $scope.getpos(location);
                    } else {}
                });
            }
        }
        $scope.ToggleTrackingViewStatus = false;
        $scope.ToggleDefault = function() {
             $scope.ShowTaskViewIcons = true;
             $scope.mapp1=false;
            $scope.ToggleTrackingViewZoom = 20;
            $scope.ToggleTrackingViewCenter = api.mapcenter;
            
            $scope.ToggleTrackingType = '';
            $scope.ToggleTrackingViewCust = {};
            $scope.ToggleRouteDestination = 'London';
            $scope.ToggleRouteOrigin = 'London';
            $scope.ToggleRouteStatus = false;
            $scope.ToggleTrackingViewSingleEmp = false;
            $scope.ToggleTrackingShowRedraw = false;
            $scope.ToggleGPSData = false;
            $scope.TaskViewEmp_id = '';
            $scope.taskGeoData = [];
            $scope.TaskView = [];
            $scope.TripGeoData = [];
            $scope.TaskViewList = [];
            $scope.AllStatus=[];
            $scope.TaskSelect = '';
            $scope.TripDuration = '';
            $scope.TripKMS = '';
            $scope.taskStart = '';
            $scope.taskEnd = '';
            $scope.TripGeoSegmentsData = [];
            $scope.TripGeoSegments = [];
            $scope.TripGeoDataStart = [];
            $scope.TripGeoDataStop = [];
            $scope.TripStared = [];
            $scope.ToggleTrackingViewShowAllEmp = false;
            $scope.ToggleTrackingViewCust.lat = $scope.ToggleTrackingViewCenter[0];
            $scope.ToggleTrackingViewCust.lng = $scope.ToggleTrackingViewCenter[1];
            if (angular.element("#panelId")[0]) {
                angular.element("#panelId")[0].innerHTML = '';
            }
            $scope.ToggleTrackingViewCust = [];
            $scope.ToggleTrackingViewCustSt = false;
        }
        $scope.ToggleDefault();
        $scope.allocatedTask = function(emp) {
            if (!emp) {
                return;
            }
            var status = 'Allocated';
            var emp = emp;
            var taskId = $scope.TaskView.id;
            var data = {
                "status": status,
                "emp": emp
            };
            $http.post('allocateTask/' + taskId, {
                data: data
            }).then(function(response) {
                var res = angular.fromJson(response);
                if (res.data.status == 'ok') {
                    $scope.TaskView = res.data.data;
                    $mdDialog.hide('reload');
                    $mdDialog.show($mdDialog.alert().title('Task Allocated').ok('OK'));
                    $scope.updateData();
                } else {
                    $scope.alert = res.data.data;
                }
            }, function(response) {});
        }
        $scope.ToggleTrackingRedraw = function() {
            $scope.ToggleRouteStatus = false;
            if (angular.element("#panelId")[0]) {
                angular.element("#panelId")[0].innerHTML = '';
            }
            $timeout(function() {
                $scope.$apply();
                $scope.ToggleRouteStatus = true;
            }, 200);
        }
        $scope.ToggleTrackingViewDrawDirection = function(data) {
            console.log(data);
            if ($scope.TaskView.status == 'Unallocated' && $scope.ToggleRouteStatus == false) {
                if (!data.user_id) {
                    if ($scope.onlineemp) {
                        var data = _.find($scope.onlineemp, function(item) {
                            if (item.user_id == data) {
                                return true;
                            } else {
                                return false;
                            }
                        })
                        if (!data) {
                            return;
                        }
                    }
                }
                if (angular.element("#panelId")[0]) {
                    angular.element("#panelId")[0].innerHTML = '';
                }
                $scope.ToggleRouteOrigin = [data.lat, data.lng];
                $scope.TaskViewEmp_id = data.user_id;
                $scope.ToggleRouteDestination = [$scope.TaskView.loc_lat, $scope.TaskView.loc_lng];
                $scope.ToggleRouteStatus = true;
            } else {
                if (angular.element("#panelId")[0]) {
                    angular.element("#panelId")[0].innerHTML = '';
                }
                $scope.ToggleRouteStatus = false;
            }
        }

            

        $scope.ToggleTrackingView = function(data, type,index1) {

            var data = data || '';
            var type = type || '';
            
            $scope.ToggleDefault();
            $scope.mapp1=true;
            // $scope.updateData();
            $scope.ToggleTrackingType = type;

            $scope.ToggleTrackingViewCenter = $scope.mapcenter;
            if (type == 'update') {
                $scope.ToggleTrackingViewStatus = false;
                $scope.ToggleTrackingType = 'task';
            }
            if ($scope.ToggleTrackingType == 'emp') {
                $scope.ToggleTrackingViewCust.lat = $scope.mapcenter[0];
                $scope.ToggleTrackingViewCust.lng = $scope.mapcenter[1];
            }
            if ($scope.ToggleTrackingType == 'task') {
                $scope.TaskView = data;
                $scope.taskdiv=true;
                $scope.TaskViewEmp_id = data.user_id;                
                angular.forEach($scope.tasksData.data, function(item, index) { 
               
                    if ((item.allocated_emp_id == $scope.TaskViewEmp_id)) {
                        $scope.TaskViewList.push(item);
                    }
                });

            } else if ($scope.ToggleTrackingType == 'emp') {
                $scope.TaskViewEmp_id = data.user_id;
                $scope.TaskSelect = 'All';
                angular.forEach($scope.tasksData.data, function(item, index) {
                
                angular.forEach(item.tasks, function(itemq, index1) {         
                $scope.AllStatus.push(itemq);
                });
                    if ((item.allocated_emp_id == $scope.TaskViewEmp_id)) {
                        $scope.TaskViewList.push(item);
                    }
                });
                var tepDate = moment($scope.date).format('YYYY-MM-DD');

                $http.get('getMyLocations?date=' + tepDate + '&emp=' + $scope.TaskViewEmp_id).then(function(data) {
                   
                    $scope.TripKMS = data.data.data.distance;
                    $scope.TripDuration = data.data.data.time_taken;
                    $scope.TripGeoData = data.data.data.geoData;
                    $scope.TripGeoSegmentsData = [];
                    $scope.TripGeoSegments = [];
                    $scope.TripGeoDataStart = [];
                    $scope.TripGeoDataStop = [];
                    if ($scope.TripGeoData.length > 0) {
                        for (var i = 0; i <= $scope.TripGeoData.length - 1; i++) {
                            if ($scope.TripGeoData[i]['activity'] == 'Start') {
                                $scope.TripGeoDataStart.push([$scope.TripGeoData[i]['lat'], $scope.TripGeoData[i]['lng']]);
                            }
                            if ($scope.TripGeoData[i]['activity'] == 'Stop') {
                                $scope.TripGeoDataStop.push([$scope.TripGeoData[i]['lat'], $scope.TripGeoData[i]['lng']]);
                            }
                            if ($scope.TripGeoData[i]['activity'] == 'Start') {
                                $scope.TripGeoSegments.push($scope.TripGeoSegmentsData);
                                $scope.TripGeoSegmentsData = [];
                                $scope.TripGeoSegmentsData.push([$scope.TripGeoData[i]['lat'], $scope.TripGeoData[i]['lng']]);
                            } else {
                                $scope.TripGeoSegmentsData.push([$scope.TripGeoData[i]['lat'], $scope.TripGeoData[i]['lng']]);
                            }
                        }
                        if ($scope.TripGeoSegmentsData.length > -1) {
                            $scope.TripGeoSegments.push($scope.TripGeoSegmentsData);
                            $scope.TripGeoSegmentsData = [];
                        }
                    }
                    if ($scope.TripGeoData[0]) {
                        $scope.TripGeoData[0].lat ="13.058875";
                        $scope.TripGeoData[0].lng = "80.2491603";
                        $scope.mapcenter = [$scope.TripGeoData[0].lat, $scope.TripGeoData[0].lng];
                        $scope.TripStared = [$scope.TripGeoData[0].lat, $scope.TripGeoData[0].lng];
                    }
                    $timeout(function() {
                        $scope.$apply();
                    }, 100);
                })
            } else {}
            if ($scope.ToggleTrackingViewStatus) {
                $scope.SideBar = true;
                $scope.trackingMap = true;
                $scope.ToggleTrackingViewStatus = false;
            } else {
                $scope.SideBar = false;
                $scope.trackingMap = false;
                $scope.ToggleTrackingViewStatus = true;
                $scope.taskindex=index1;
                if ($scope.ToggleTrackingType == 'task') {
                    
                    
                    $scope.loadTaskData(data,index1)
                }
            }
        }
        $scope.ToggleTrackingView1 = function(data, type) {
           var data = data || '';
            var type = type || '';
            $scope.mapp1=true;
            $scope.ToggleDefault();
            // $scope.updateData();
            $scope.ToggleTrackingType = type;
            $scope.ToggleTrackingViewCenter = $scope.mapcenter;
            if (type == 'update') {
                $scope.ToggleTrackingViewStatus = false;
                $scope.ToggleTrackingType = 'task';
            }
            if ($scope.ToggleTrackingType == 'emp') {
                $scope.ToggleTrackingViewCust.lat = $scope.mapcenter[0];
                $scope.ToggleTrackingViewCust.lng = $scope.mapcenter[1];
            }
            if ($scope.ToggleTrackingType == 'task') {
                $scope.TaskView = data;
            } else if ($scope.ToggleTrackingType == 'emp') {
                $scope.TaskViewEmp_id = data.user_id;
                $scope.TaskSelect = 'All';
                angular.forEach($scope.tasksData.data, function(item, index) {
                    if ((item.allocated_emp_id == $scope.TaskViewEmp_id)) {
                        $scope.TaskViewList.push(item);
                    }
                });

                

                var tepDate = moment($scope.date).format('YYYY-MM-DD');

                $http.get('getMyLocations?date=' + tepDate + '&emp=' + $scope.TaskViewEmp_id).then(function(data) {


                    $scope.TripKMS = data.data.data.distance;
                    $scope.TripDuration = data.data.data.time_taken;
                    $scope.TripGeoData = data.data.data.geoData;
                    $scope.TripGeoSegmentsData = [];
                    $scope.TripGeoSegments = [];
                    $scope.TripGeoDataStart = [];
                    $scope.TripGeoDataStop = [];
                    if ($scope.TripGeoData.length > 0) {
                        for (var i = 0; i <= $scope.TripGeoData.length - 1; i++) {
                            if ($scope.TripGeoData[i]['activity'] == 'Start') {
                                $scope.TripGeoDataStart.push([$scope.TripGeoData[i]['lat'], $scope.TripGeoData[i]['lng']]);
                            }
                            if ($scope.TripGeoData[i]['activity'] == 'Stop') {
                                $scope.TripGeoDataStop.push([$scope.TripGeoData[i]['lat'], $scope.TripGeoData[i]['lng']]);
                            }
                            if ($scope.TripGeoData[i]['activity'] == 'Start') {
                                $scope.TripGeoSegments.push($scope.TripGeoSegmentsData);
                                $scope.TripGeoSegmentsData = [];
                                $scope.TripGeoSegmentsData.push([$scope.TripGeoData[i]['lat'], $scope.TripGeoData[i]['lng']]);
                            } else {
                                $scope.TripGeoSegmentsData.push([$scope.TripGeoData[i]['lat'], $scope.TripGeoData[i]['lng']]);
                            }
                        }
                        if ($scope.TripGeoSegmentsData.length > -1) {
                            $scope.TripGeoSegments.push($scope.TripGeoSegmentsData);
                            $scope.TripGeoSegmentsData = [];
                        }
                    }
                    if ($scope.TripGeoData[0]) {
                        $scope.mapcenter = [$scope.TripGeoData[0].lat, $scope.TripGeoData[0].lng];
                        $scope.TripStared = [$scope.TripGeoData[0].lat, $scope.TripGeoData[0].lng];
                    }
                    $timeout(function() {
                        $scope.$apply();
                    }, 100);
                })
            } else {}
            if ($scope.ToggleTrackingViewStatus) {
                $scope.SideBar = true;
                $scope.trackingMap = true;
                $scope.ToggleTrackingViewStatus = false;
            } else {
                $scope.SideBar = false;
                $scope.trackingMap = false;
                $scope.ToggleTrackingViewStatus = true;
                if ($scope.ToggleTrackingType == 'task') {
                    

                    $scope.loadTaskData(data)
                }
            }
        }
        $scope.empLoadTask = function(id) {
            var data = {};
            angular.forEach($scope.TaskViewList, function(item, index) {

                if ((item.id == id)) {
                    data = item;
                }
            });
            $scope.TaskView = data;
            $scope.loadTaskData(data);
        }
        $scope.loadTaskData = function(data,index1) {
           
            if (data.emp_info.first_name) {
                if ((data.emp_info.user_id) && (data.tasks[index1].status == 'In-Progress' || data.tasks[index1].status == 'Assigned')) {
                    // star Tracking view

                    $scope.taskindex=index1;
                    $scope.ToggleTrackingViewCenter = [data.tasks[index1].cust_info.loc_lat, data.tasks[index1].cust_info.loc_lng];
                    $scope.ToggleTrackingViewCust.lat = data.tasks[index1].cust_info.loc_lat;
                    $scope.ToggleTrackingViewCust.lng = data.tasks[index1].cust_info.loc_lng;
                    $scope.ToggleTrackingViewShowAllEmp = false;
                    $scope.ToggleTrackingViewSingleEmp = true;
                    $scope.ToggleTrackingViewCustSt = true;
                    $scope.TaskViewEmp_id = data.emp_info.user_id;
                    
                    var index = _.findIndex($scope.onlineemp, function(item) {
                        return item.user_id == $scope.TaskViewEmp_id;
                    });
                    if (index > -1) {
                        $scope.ToggleRouteOrigin = [$scope.onlineemp[index].lat, $scope.onlineemp[index].lng];
                    }
                    console.log(index1);
                    $scope.ToggleRouteDestination = [data.tasks[index1].cust_info.loc_lat, data.tasks[index1].cust_info.loc_lng];
                    console.log($scope.ToggleRouteDestination);
                    $scope.ToggleRouteStatus = true;
                    

                    if(data.tasks[index1].status == 'Assigned')
                    {
                             $scope.ToggleTrackingShowRedraw = true;
                             $scope.ShowTaskViewIcons = false;
                             $scope.ToggleRouteStatus = false;
                    }

                }
                if ((data.emp_info.user_id) && (data.tasks[index1].status == 'Completed' || data.tasks[index1].status == 'Incomplete')) {
                    // star Tracking view
                    $scope.taskindex=index1;
                    $scope.ToggleTrackingViewCenter = [data.tasks[index1].cust_info.loc_lat, data.tasks[index1].cust_info.loc_lng];
                    $scope.ToggleTrackingViewCust.lat = data.tasks[index1].cust_info.loc_lat;
                    $scope.ToggleTrackingViewCust.lng = data.tasks[index1].cust_info.loc_lng;
                    $scope.ToggleTrackingViewShowAllEmp = false;
                    $scope.ToggleTrackingViewSingleEmp = false;
                    $scope.ToggleTrackingViewCustSt = true;
                    $scope.TaskViewEmp_id = data.emp_info.user_id;
                    // ToggleGPSData
                    $http.get('getTaskSummary/' + $scope.TaskView.tasks[index1].id).then(function(response) {
                        // $http.get('assets/app/travel-map/data.json').then(function(response) {
                        var res = angular.fromJson(response);
                        
                        if (res.data.status == 'ok') {
                            $scope.taskGeoData = [];
                            angular.element("#panelId")[0].innerHTML = res.data.data.distance + ', Taken ' + res.data.data.time_taken
                            $scope.taskStart = moment(res.data.data.start).format("Do MMM, h:mm A");
                            $scope.taskEnd = moment(res.data.data.end).format("Do MMM, h:mm A");
                            angular.forEach(res.data.data.gpsData, function(value, key) {
                                $scope.taskGeoData.push([value.lat, value.lng]);
                            });
                            // $scope.taskGeoData = res.data.data.gpsData;
                            $scope.ToggleGPSData = true;
                        } else {}
                    });
                }
            }
            if (data.tasks[index1].status == 'Unallocated') {
                // set allocation mode
                $scope.taskindex=index1;
                $scope.ToggleTrackingViewCenter = [data.tasks[index1].cust_info.loc_lat, data.tasks[index1].cust_info.loc_lng];
                $scope.ToggleTrackingViewCust.lat = data.tasks[index1].cust_info.loc_lat;
                $scope.ToggleTrackingViewCust.lng = data.tasks[index1].cust_info.loc_lng;
                $scope.ToggleTrackingViewCustSt = true;
                $scope.ToggleTrackingViewShowAllEmp = true;
                $scope.ToggleTrackingShowRedraw = false;
            }
            if (data.tasks[index1].status == 'Canceled' || data.tasks[0].status == 'Declined') {
                // set allocation mode
                $scope.taskindex=index1;
                $scope.ToggleTrackingViewCenter = [data.tasks[index1].cust_info.loc_lat, data.tasks[index1].cust_info.loc_lng];
                $scope.ToggleTrackingViewCust.lat = data.tasks[index1].cust_info.loc_lat;
                $scope.ToggleTrackingViewCust.lng = data.tasks[index1].cust_info.loc_lng;
                $scope.ToggleTrackingViewCustSt = true;
                $scope.ToggleTrackingViewShowAllEmp = false;
                $scope.ToggleTrackingShowRedraw = false;
            }
            setTimeout(function() {
                $scope.$apply();
            }, 1000);
        }
        setTimeout(function() {
            $scope.RouteDestination = [12.9830, 80.2594];
            $scope.RouteOrigin = [12.90230, 80.2294];
            $scope.$apply();
        }, 1000);
        $scope.setPanel = function(renderer) {
            if (angular.element("#panelId")[0]) {
                angular.element("#panelId")[0].innerHTML = '';
            }
            renderer.setPanel(document.getElementById('panelId'));
        }
    }
})();
