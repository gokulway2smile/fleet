(function() {
    'use strict';
    angular.module('app').controller('AppSidenavRightCtrl', ['$scope', '$http', 'api','$timeout','$rootScope', AppSidenavRightCtrl])

    function AppSidenavRightCtrl($scope, $http, api, $timeout,$rootScope) {
        $scope.messages_mob = [];
        $scope.messages_web = [];
        $scope.notifications = [];
        $scope.messages_mobPage = 1;
        $scope.GetMyNotificationsPage = 1;
        $scope.notificationsPage = 1;
        $scope.newcount = 0;
        $scope.locations = [];
        $scope.tempcus = api.customers();
        $scope.tempcus().then().then(function(data) {
            $scope.locations = data.data;
        });
        $scope.temp = api.users();
        $scope.temp().then().then(function(data) {
            $scope.users = data.data;
        });
        $scope.fleets = [];
        $scope.tempcompetitor = api.competitor();
        $scope.tempcompetitor().then().then(function(data) {
            $scope.fleets = data.data;
        });
        $scope.parseInt = parseInt;

        $scope.loadaa = function()
        {
            console.log('Data');
        };

        $scope.GetNote = function () {
             $http.get('GetNotifications?page='+$scope.notificationsPage).success(function(response) {
            var alert = false;
            $scope.newcount =  $scope.newcount +  response.data.total;
            $timeout(function() { $rootScope.$digest()}, 10);
            $scope.notificationsPage =  response.data.current_page+1;
            angular.forEach(response.data.data, function(element) {
                if (!element.read_at) {
                    if (!alert) {
                        globalapp.alert();
                        alert = true;
                    }

                }

                if(element.data)
                {
                     element.data = angular.fromJson(element.data);
                }

                if (element.is_mobile == 1) {
                    $scope.messages_mob.push(element);
                } else {
                    $scope.messages_web.push(element);
                }
            });
        })
        }


        // $scope.AllCount =  function()
        // {
        //     return $scope.newcount;
        // }

        $scope.GetMyNote = function () {
        $http.get('GetMyNotifications?page='+$scope.GetMyNotificationsPage).success(function(response) {
            var alert = false;
            $scope.newcount =  $scope.newcount +  response.data.total;
            $timeout(function() { $rootScope.$digest()}, 10);
            $scope.GetMyNotificationsPage =  response.data.current_page+1;
            angular.forEach(response.data.data, function(element) {
                if (!element.read_at) {
                    if (!alert) {
                        globalapp.alert();
                        alert = true;
                    }
                }
                if(element.data)
                {
                     element.data = angular.fromJson(element.data);
                }

                $scope.notifications.push(element);
            });
        })
        }


        $scope.GetNote();
        $scope.GetMyNote();

        $scope.ReadNotifications = function(data, type) {
            var type = type || null;
            if (type == 'web') {
                var id = _.findIndex($scope.messages_web, ['id', parseInt(data)]);
                if (typeof id !== 'undefined') {
                                   $scope.newcount =  $scope.newcount - 1 ;
                    $scope.messages_web[id].read_at = new Date();
                    var id = $scope.messages_web[id].id;
                }
            } else {
                var id = _.findIndex($scope.messages_mob, ['id', parseInt(data)]);
                if (typeof id !== 'undefined') {
                    //                            $scope.newcount =  $scope.newcount - 1 ;
                    // $scope.messages_mob[id].read_at = new Date();
                    var id = $scope.messages_mob[id].id;
                }
            }
            if (typeof id !== 'undefined') {
                $http.get('ReadNotifications/' + id).success(function(response) {

                    $scope.newcount =  $scope.newcount - 1 ;
                    $timeout(function() { $rootScope.$digest()}, 10);
                });
            }
        }
        $scope.ReadMyNotifications = function(data) {
            var id = _.findIndex($scope.notifications, ['id', data]);
            if (typeof id !== 'undefined') {
                $scope.notifications[id].read_at = new Date();

                var id = $scope.notifications[id].id;
                $http.get('ReadMyNotifications/' + id).success(function(response) {
                    $scope.newcount =  $scope.newcount - 1 ;
                    $timeout(function() { $rootScope.$digest()}, 10);
                });
            }
        }

        $rootScope.$on('NotificationEvent', function(e, args) {
            if(args)
            {
            $scope.newcount =  $scope.newcount +  1;
            $timeout(function() { $rootScope.$digest()}, 10);
            globalapp.alert();

            if(args.data)
            {
                 args.data = angular.fromJson(args.data);
            }

            $scope.notifications.push(args.data)
            }

        });
        $rootScope.$on('GeoFenceFromMobile', function(e, args) {
            if(args)
            {
            $scope.newcount =  $scope.newcount +  1;
            globalapp.alert();
                    if(args.data)
            {
                 args.data = angular.fromJson(args.data);
            }
            $scope.messages_mob.push(args.data)
            }
        });
        $rootScope.$on('GeoFenceFromWeb', function(e, args) {
              if(args)
            {
            $scope.newcount =  $scope.newcount +  1;
            globalapp.alert();
                    if(args.data)
            {
                 args.data = angular.fromJson(args.data);
            }
            $scope.messages_web.push(args.data)
            }
        });
    }
})();
