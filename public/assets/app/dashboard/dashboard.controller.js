(function () {
    'use strict';

    angular.module('app')
        .controller('DashboardCtrl', ['$scope','$mdEditDialog', '$q', '$timeout', '$http','$mdDialog','api','$translate',DashboardCtrl])


    function DashboardCtrl($scope,$mdEditDialog,$q,$timeout,$http,$mdDialog,api,$translate) {
console.log($translate.instant('TITLE'));
console.log('sdsds');





 function DialogController($scope, $mdDialog, dataToPass) {
            $scope.data = dataToPass;
            $scope.map = true;
        //       $scope.customers = [];
        // $scope.temp = api.customers();
        // $scope.temp().then().then(function(data) {
        //     $scope.customers = data.data;
        // });

            $timeout(function() {
                    $scope.$apply();
                }, 0);


            $scope.hide = function() {
                $mdDialog.hide();
            };
            $scope.cancel = function() {
                $mdDialog.cancel();
            };
        }
        $scope.openMap = function(lat,lng,ev) {

            var data = {
                'lat' : lat,
                'lng' : lng
            };
            $mdDialog.show({
                controller: DialogController,
                locals: {
                    dataToPass: data
                },
                templateUrl: 'assets/app/stop-report/modal_view.html?'+Math.random(),
                targetEvent: ev,
                clickOutsideToClose: false
            }).then(function(answer) {}, function() {});
        };




     $scope.activefilter = false;
        var w;
        var original;
        $scope.smart = true;
        $scope.wwstatus = true;
        $scope.datatable = {};
        $scope.datatable.data = [];
           var old = [];
      $scope.limitOptions = [5, 10, 15, 20, 25, 50, 100];
        $scope.options = {
            limitSelect: true,
            pageSelect: true,
            boundaryLinks: true
        };
        $scope.query = {
            order: 'company_name',
            limit: 10,
            page: 1
        };


        $scope.autocolumn = [{
            name: "userName",
            display: "Driver"
        },
        {
            name: "VehicleNo",
            display: "Vehicle"
        }, {
            name: "timestamp",
            display: "Last Update"
        }, {
            name: "activity",
            display: "Status"
        },
        {
            name: "Speed",
            display: "Speed"
        },
        {
            name: "Traveled",
            display: "Distance"
        },
         {
            name: "battery_status",
            display: "Battery"
        }
,
         {
            name: "CurrentLocation",
            display: "Location"
        }


        ];


        $scope.multisearch = Array();
        $scope.multisearch[0] = {
            id: 0,
            column: "",
            ident: "",
        };
        $scope.addRow = function() {
            $scope.multisearch.push({
                id: $scope.multisearch.length,
                column: "",
                ident: ""
            });
        };
        $scope.deleteRow = function(int) {
            $scope.multisearch.splice(int, 1);
            for (var i = 0; i < $scope.multisearch.length; i++) {
                $scope.multisearch[i].id = i;
            }
            $scope.updateDataTable();
        };
        $scope.resettable = function() {
            $scope.wwstatus = true;
           GetOnlineEmp();

        };
        $scope.updateDataTable = function() {


            $scope.wwstatus = false;
            var filter = false; //set filter false
            for (var j = 0; j < $scope.multisearch.length; j++) {
                if ($scope.multisearch[j].ident && $scope.multisearch[j].column) {
                    filter = true; //if a filter exists
                }
            }
            if (filter) { //if a filter is set
                if (w) {
                    w.terminate();
                }
                w = new Worker("assets/app/core/filter.js");
                w.postMessage({
                    multisearch: $scope.multisearch,
                    datatable: angular.copy(original), //copy the original,
                    smart: $scope.smart
                });
                w.onmessage = function(event) {
                    $scope.datatable = event.data;
                    $scope.wwstatus = true;
                    $scope.$digest();
                };
            } else {
                $scope.wwstatus = true;
                $scope.datatable = angular.copy(original);
            }
        }

 		 $scope.mapshow = false;
        $scope.onlineemp = [];
        $scope.emppath = [];
        $scope.data = [];
        $scope.mapcenter = api.mapcenter;
        $scope.customers = [];
        $scope.tempcus = api.customers();
        $scope.tempcus().then().then(function(data) {
            $scope.customers = data.data;
        });
        $scope.competitorinfo = [];
        $scope.tempcompetitor = api.competitor();
        $scope.tempcompetitor().then().then(function(data) {
            $scope.competitorinfo = data.data;
        });
        $scope.$on('LocationUpdate', function(e, args) {
            var index = _.findIndex($scope.onlineemp, function(item) {
                return item.user_id == args.data.user_id
            })
            console.log(index);
            if (index > -1) {
                $scope.onlineemp[index] = $scope.attach_info(args.data);
            } else {

 			$scope.onlineemp.push($scope.attach_info(args.data));

            }


                  	$scope.datatable.data = angular.copy($scope.onlineemp);


 			original = $scope.datatable;



            $scope.$apply();
        });
        $scope.content = function($data) {
            var info = '<p>Emp id :' + $data['user']['user_id'] + '</p> ' + '<p>Name :' + $data['user']['last_name'] + ' ' + $data['user']['first_name'] + '</p> ' + '<p> Active : ' + api.timeago($data['timestamp']) + ' </p>';
            return info;
        }
        // function GetEmpData(id) {
        //     var id = id;
        //     $http.get('location_emp_api/' + id).success(function(response) {
        //         $scope.emppath = response.data;
        //     });
        // }
        function GetOnlineEmp() {

                $http.get('location_online_emp').success(function(response) {
                    $scope.onlineemp = [];
                    angular.forEach(response.data, function(element) {

                        $scope.onlineemp.push($scope.attach_info(element));



                    });

                          	$scope.datatable.data = angular.copy($scope.onlineemp);


 			original = $scope.datatable;



                });


        }


 		$scope.attach_info = function(data)
        {
        	data.userName = data.user.first_name+' '+data.user.last_name;




			var vehicle =_.find($scope.competitorinfo, ['id', parseInt(data.vehicle_no)]);
            data.VehicleNo = 'No Vehicle';
            if(vehicle)
            {
                  if(vehicle.name)
                 {
                    data.VehicleNo = vehicle.name;
                 }
            }







        	return data;
        };


        $scope.$on('$viewContentLoaded', function() {

            GetOnlineEmp();
        })
    }



    }



)();



