(function() {
    'use strict';
    angular.module('app').controller('CategoryCtrl', ['$rootScope', '$state','$filter', '$stateParams', '$mdToast', '$timeout', '$scope', '$mdEditDialog', '$q', '$http', '$mdDialog', 'api', CategoryCtrl])

    function CategoryCtrl($rootScope, $state, $filter,$stateParams, $mdToast, $timeout, $scope, $mdEditDialog, $q, $http, $mdDialog, api) {
        $scope.addRow = function() {
            $scope.multisearch.push({
                id: $scope.multisearch.length,
                column: "",
                ident: ""
            });
        };
        $scope.deleteRow = function(int) {
            $scope.multisearch.splice(int, 1);
            for (var i = 0; i < $scope.multisearch.length; i++) {
                $scope.multisearch[i].id = i;
            }
            $scope.updateDataTable();
        };
        $scope.resettable = function() {
            $scope.wwstatus = true;
            $scope.datatable = angular.copy(original);
        };
        $scope.updateDataTable = function() {
            $scope.wwstatus = false;
            var filter = false; //set filter false
            for (var j = 0; j < $scope.multisearch.length; j++) {
                if ($scope.multisearch[j].ident && $scope.multisearch[j].column) {
                    filter = true; //if a filter exists
                }
            }
            if (filter) { //if a filter is set
                if (w) {
                    w.terminate();
                }
                w = new Worker("assets/app/core/filter.js");
                w.postMessage({
                    multisearch: $scope.multisearch,
                    datatable: angular.copy(original), //copy the original,
                    smart: $scope.smart
                });
                w.onmessage = function(event) {
                    $scope.datatable = event.data;
                    $scope.wwstatus = true;
                    $scope.$digest();
                };
            } else {
                $scope.wwstatus = true;
                $scope.datatable = angular.copy(original);
            }
        }
        $scope.closeform = function(data) {
            data = data || 0;
            $scope.formmode('close', data);
        }
        $scope.addbtn = function() {
            $scope.formmode('add');
        }
        $scope.submit_form = function() {
            $scope.data_form.$setSubmitted();
            $scope.add_data();
        }
        $scope.view = function(id) {
            $state.go('admin.devview', {
                id: api.encode(id)
            });
        }
        $scope.formmode = function(type, data = 0) {
            var ev = '';
            if ($scope.data_form.$dirty && data == 0) {
                var confirm = $mdDialog.confirm().title('You will lose unsaved changes if you do this action ?').targetEvent(ev).ok('OK, Do it').cancel('Cancel');
                $mdDialog.show(confirm).then(function() {
                    $scope.reset();
                    if (type === 'update') {
                        $scope.showform = true;
                        $scope.add = false;
                    } else if (type === 'add') {
                        $scope.showform = true;
                        $scope.add = true;
                    } else {
                        $scope.showform = false;
                        $scope.add = true;
                    }
                    return;
                }, function() {
                    return;
                });
            } else {
                $scope.reset();
                if (type === 'update') {
                    $scope.showform = true;
                    $scope.add = false;
                } else if (type === 'add') {
                    $scope.showform = true;
                    $scope.add = true;
                } else {
                    $scope.showform = false;
                    $scope.add = true;
                }
            }
        };
        $scope.customers = [];
        $scope.tempcus = api.customers();
        $scope.tempcus().then().then(function(data) {
            $scope.customers = data.data;
        });
        $scope.title = 'Geo Fence';
        $scope.apipath = 'alert/';
        $scope.showform = false;
        $scope.add = true;
        $scope.data = {};
        $scope.activefilter = false;
        $scope.showsearch = false;
        var w;
        var original;
        $scope.smart = true;
        $scope.wwstatus = true;
        $scope.datatable = {};
        $scope.datatable.data = [];
        $scope.data.lat = 0;
        $scope.data.lng = 0;
        $scope.mapzoom = 12;
        $scope.users = [];
        $scope.temp = api.users();
        $scope.st = api.st;
        $scope.active = api.active;
        $scope.notify_type = api.notify_type;
        $scope.place_type = api.place_type;
        $scope.temp().then().then(function(data) {
            $scope.users = data.data;
        });
        $scope.mapzoom = 12;
        $scope.limitOptions = [5, 10, 15, 20, 25, 50, 100];
        $scope.options = {
            limitSelect: true,
            pageSelect: true,
            boundaryLinks: true
        };
        $scope.query = {
            order: 'name',
            limit: 10,
            page: 1
        };
        $scope.autocolumn = [{
            name: "titile",
            display: "Name"
        }, {
            name: "info",
            display: "Description"
        }];
        $scope.multisearch = Array();
        $scope.multisearch[0] = {
            id: 0,
            column: "",
            ident: "",
        };

        $scope.data.radius = 150;
        $scope.reset = function() {
            if ($scope.lfApi) {
                $scope.lfApi.removeAll()
            }
            $scope.data_form.$setPristine();
            $scope.data_form.$setUntouched();
            $scope.data = {};
            $scope.data.lat = 0;
            $scope.data.lng = 0;
            $scope.data.uploads = [];
            $scope.files = [];
            $scope.isFilesNull = true;
            $scope.mapzoom = 12;
             $scope.data.radius = 6000;
        };
        $scope.toggleLimitOptions = function() {
            $scope.limitOptions = $scope.limitOptions ? undefined : globalapp.pagenation;
        };
        $scope.update = function() {
            $http.put($scope.apipath + $scope.data.id, {
                data: $scope.data
            }).then(function(response) {
                var res = angular.fromJson(response)
                console.log(res.data);
                if (res.data.status == 'ok') {
                    $scope.closeform(1);
                    $mdDialog.show($mdDialog.alert().title($scope.title + '  Info Updated').ok('OK'));
                    $scope.loaddata();
                } else {
                    $mdDialog.show($mdDialog.alert().title(res.data.data).ok('OK'));
                }
            });
        }
        $scope.add_data = function() {
            if ($scope.data_form.$invalid) {
                return;
            }
            if ($scope.add == false) {
                $scope.update();
                return;
            }
            $http.post($scope.apipath, {
                data: $scope.data
            }).then(function(response) {
                var res = angular.fromJson(response);
                if (res.data.status == 'ok') {
                    $scope.closeform(1);
                    $mdDialog.show($mdDialog.alert().title('New ' + $scope.title + ' Added').ok('OK'));
                    $scope.files = [];
                    $scope.loaddata();
                } else {
                    $mdDialog.show($mdDialog.alert().title(res.data.data).ok('OK'));
                }
            }, function(response) {});
        }
        $scope.delete = function(dataid, ev) {
            var confirm = $mdDialog.confirm().title('Are you sure to delete this  ' + $scope.title + ' ?').targetEvent(ev).ok('Yes, Delete').cancel('Cancel');
            $mdDialog.show(confirm).then(function() {
                $http.delete($scope.apipath + dataid).then(function(response) {
                    if (response.data.status == 'ok') {

                        $mdToast.show($mdToast.simple().textContent($scope.title + ' Deleted').position('top center').hideDelay(3000));
                        $scope.loaddata();
                         $scope.closeform(1);
                    } else {
                        $mdToast.show($mdToast.simple().textContent(response.data.data).position('top center').hideDelay(3000));
                    }
                    $scope.loaddata();
                });
            }, function() {});
        }
        $scope.update_info = function(dataid) {
            $scope.formmode('update');
            $http.get($scope.apipath + dataid).then(function(response) {
                var res = angular.fromJson(response);
                if (res.data.status == 'ok') {
                    $scope.data = res.data.data;



        if(($scope.data.loc_id ==! null)  && ($scope.data.loc_type == 0))
        {
            $scope.geocodeupdateExist($scope.data.loc_id);
        }



                    // if($scope.data.cust)
                    // {
                    //  $scope.geocodeupdateExist()
                    // }

                } else {}
            }, function(response) {
                // failure call back
            });
        }
        $scope.loaddata = function() {
            $http.get($scope.apipath).then(function(response) {
                var res = angular.fromJson(response);
                if (res.data.status == 'ok') {
                    $scope.datatable.data = res.data.data;
                    original = res.data;
                } else {}
            }, function(response) {
                // failure call back
            });
        }


        $scope.geocodeupdateExist = function(id)
        {


          var temp =  $filter('filter')($scope.customers, {id: id})[0];




                    if(temp)
                    {
                        $scope.data.lat =temp.loc_lat;
                        $scope.data.lng = temp.loc_lng;
                    }
       $timeout(function() {
                    $scope.$apply();
                }, 0);

        }



            $scope.getpos = function(data, zoom = 12) {
            if (data.lat) {
                $scope.data.lat = data.lat();
                $scope.data.lng = data.lng();
                $scope.mapzoom = zoom;
                $timeout(function() {
                    $scope.$apply();
                }, 0);
            } else {
                $scope.data.lat = data.location.lat;
                $scope.data.lng = data.location.lng;
                $scope.mapzoom = zoom;
                $timeout(function() {
                    $scope.$apply();
                }, 0);
            }
        };



                $scope.geocodeupdateloc = function() {
           var address = '';
            if ($scope.data.address && $scope.data.address != undefined) {
                address = address + $scope.data.address;
            }
            if (address) {
                var geocoder = new google.maps.Geocoder();
                geocoder.geocode({
                    "address": address
                }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK && results.length > 0) {
                        var location = results[0].geometry.location;
                        $scope.getpos(location);
                    } else {}
                });
            }

        }

        $scope.loaddata();
        $scope.$on('$viewContentLoaded', function() {
            if ($stateParams.mode == 'edit' && $stateParams.data != null) {
                (function() {}, 10);
                $scope.update_info(api.decode($stateParams.data));
            } else if ($stateParams.mode == 'delete' && $stateParams.data != null) {
                $scope.delete(api.decode($stateParams.data));
            } else {}
        })
    }
})();
