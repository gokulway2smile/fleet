(function() {
    'use strict';
    angular.module('app').controller('MapCtrl', ['$rootScope', '$state', '$stateParams', '$mdToast', '$filter','$timeout', '$scope', '$interval', '$q', '$http', '$mdDialog', 'api', MapCtrl])

    function MapCtrl($rootScope, $state, $stateParams, $mdToast, $filter,$timeout, $scope, $interval, $q, $http, $mdDialog, api) {
        $scope.mapshow = false;
        $scope.onlineemp = [];
        $scope.emppath = [];
        $scope.data = [];
        $scope.mapcenter = api.mapcenter;
        $scope.zoom = 5;
        $scope.customers = [];
        $scope.tempcus = api.customers();
        $scope.tempcus().then().then(function(data) {
            $scope.customers = data.data;
        });
        $scope.competitorinfo = [];
        $scope.tempcompetitor = api.competitor();
        $scope.tempcompetitor().then().then(function(data) {
            $scope.competitorinfo = data.data;
        });



        $scope.pan = function()
        {
            $scope.filteredItems = $filter('filter')($scope.onlineemp, $scope.location);
            if($scope.filteredItems)
            {
                if($scope.filteredItems[0])
                {
                         $scope.mapcenter = [$scope.filteredItems[0].lat,$scope.filteredItems[0].lng];
                         $scope.zoom = 13;
                }

            }

             if(!$scope.location)
             {
                $scope.zoom = 5;
                $scope.mapcenter = api.mapcenter;
             }
        }


        $scope.$on('LocationUpdate', function(e, args) {
            var index = _.findIndex($scope.onlineemp, function(item) {
                return item.user_id == args.data.user_id
            })
            console.log(index);
            if (index > -1) {
                $scope.onlineemp[index] = $scope.attach_info(args.data);
            } else {
                $scope.onlineemp.push( $scope.attach_info(args.data));
            }
            $scope.$apply();
        });
        $scope.content = function($data) {
            var info = '<p>Emp id :' + $data['user']['user_id'] + '</p> <p>Vehicle : '+ $data['VehicleInfo']['name'] +' </p> ' + '<p>Name :' + $data['user']['last_name'] + ' ' + $data['user']['first_name'] + '</p> ' + '<p> Active : ' + api.timeago($data['timestamp']) + ' </p>';
            return info;
        }
        // function GetEmpData(id) {
        //     var id = id;
        //     $http.get('location_emp_api/' + id).success(function(response) {
        //         $scope.emppath = response.data;
        //     });
        // }
        function GetOnlineEmp() {
            if ($state.current.name == 'admin.map') {
                $http.get('location_online_emp').success(function(response) {
                    $scope.onlineemp = [];
                    angular.forEach(response.data, function(element) {
                        $scope.onlineemp.push( $scope.attach_info(element));
                    });
                });
                // // $timeout(GetOnlineEmp, 10000);
            } else {}
        }




        $scope.attach_info = function(data)
        {


            var vehicle =_.find($scope.competitorinfo, ['id', parseInt(data.vehicle_no)]);

            if(!vehicle)
            {
                data.VehicleInfo = {};
                data.VehicleInfo.name = 'No Vehicle';
                data.VehicleInfo.color = '0000ff';
                return data;
            }

            data.VehicleInfo = vehicle;
            data.VehicleInfo.color =  data.VehicleInfo.desc.replace('#','');
            return data;
        };




        $scope.$on('$viewContentLoaded', function() {
            $scope.mapshow = true;
            GetOnlineEmp();
        })
    }
})();
