<?php

namespace App\Notifications;

use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class SpeedLimit extends Notification
{
    use Queueable;
    private $trip;
    private $user;
    // private $speed;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($trip,$user,$speed)
    {
        $this->trip = $trip->toArray();
        $this->user = $user;
        $this->trip['speed'] = $speed;
        // $this->speed = $speed;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line('The introduction to the notification.')
            ->action('Login', Base::get_domin() . Base::urls['login'])
            ->line('Thank you for using our application!');
    }



    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {

        return [
            'data' => $this->trip,
        ];
    }



    // public function toBroadcast($notifiable)
    // {
    //     return new BroadcastMessage([
    //         'data' => $this->user->notifications->last(),
    //     ]);
    // }

    // public function broadcastOn()
    // {
    //     return [
    //         new PrivateChannel(\App\Http\Controllers\Base::get_sub_domain() . '-user-' . $this->user->belongs_manager),
    //     ];
    // }
}
