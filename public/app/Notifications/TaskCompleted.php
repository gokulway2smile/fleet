<?php

namespace App\Notifications;

use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\Channels\PushChannel;
class TaskCompleted extends Notification
{
    use Queueable;
    private $trip;
    private $user;
    public $push_title;
    public $push_body;
    public $push_payload;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(\App\Models\EmpCustSchedule $trip,\App\Models\User $user)
    {
        $this->trip = $trip;
        $this->user = $user;

    // $this->push_title = 'New Task Completed';
    // $this->push_body = 'New Task Completed';
    // $this->push_payload = [];
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line('The introduction to the notification.')
            ->action('Login', Base::get_domin() . Base::urls['login'])
            ->line('Thank you for using our application!');
    }


        public function toPush($notifiable)
    {
        // $notifiable = 'kalidass';
        // return $notifiable;
    }



    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {

        return [
            'data' => $this->trip,
        ];
    }



    // public function toBroadcast($notifiable)
    // {
    //     return new BroadcastMessage([
    //         'data' => $this->user->notifications->last(),
    //     ]);
    // }

    // public function broadcastOn()
    // {
    //     return [
    //         new PrivateChannel(\App\Http\Controllers\Base::get_sub_domain() . '-user-' . $this->user->belongs_manager),
    //     ];
    // }
}
