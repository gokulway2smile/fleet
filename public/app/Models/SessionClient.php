<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SessionClient extends Model
{
    protected $table = 'session_clients';

    public $timestamps = true;

    protected $fillable = [
        'client_ip',
        'client_data',
        'client_info'
    ];

    protected $guarded = [];
}
