<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TravelHistory extends Model
{
    protected $table = 'travel_history';

    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'user_id');
    }

    public function trip_id()
    {
        return $this->belongsTo('App\Models\EmpSchedule', 'trip_id', 'id');
    }

    public function vehicle_info()
    {
        return $this->belongsTo('App\Models\Competitor', 'vehicle_no', 'id');
    }

    /**
     * Display timestamps in user's timezone
     */
    protected function asDateTime($value)
    {

        $value = \App\Http\Controllers\Base::ConvertTimezone($value);

        return $value;

    }

    public function getTimestampAttribute($value)
    {

        $value = \App\Http\Controllers\Base::ConvertTimezone($value, true);

        return $value;

    }

    public function getTimestampDateAttribute()
    {
        return $this->timestamp->format('d.m.Y');
    }

    // protected $dates = ['timestamp'];
    protected $fillable = [
        'user_id',
        'accuracy', //cust_id
        'speed',
        'bearing', //geo_status
        'lat',
        'lng',
        'activity',
        'timestamp',
        'vehicle_no',
        'trip_id',
        'battery_status',
    ];

    protected $guarded = [];
}
