<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class EmpCustSchedule extends Model implements AuditableContract
{
    use Auditable;

    /**
     * Display timestamps in user's timezone
     */
    protected function asDateTime($value)
    {

        $value = \App\Http\Controllers\Base::ConvertTimezone($value);

        return $value;

    }

    public function all_status()
    {
        return $this->hasMany('App\Models\ScheduleTaskStatus', 'task_id', 'id');
    }

    protected $table = 'emp_cust_schedule';

    public $timestamps = true;

    public function cust()
    {

        return $this->belongsTo('App\Models\Customer', 'cust_id', 'id')->select(['id', 'name', 'loc_lat', 'loc_lng', 'type', 'desc', 'contact_no']);
    }

    public function task()
    {

        return $this->belongsTo('App\Models\EmpSchedule', 'emp_cust_id', 'id');
    }

    public function cust_info()
    {

        return $this->belongsTo('App\Models\Customer', 'cust_id', 'id')->select(['id', 'name', 'loc_lat', 'loc_lng', 'type', 'desc', 'contact_no', 'address']);
    }
    public function vehicle_no()
    {

        return $this->belongsTo('App\Models\EmpSchedule', 'emp_cust_id', 'id')->select(['id', 'vehicle_no']);
    }

    public function getStartDtAttribute($value)
    {

        $value = \App\Http\Controllers\Base::ConvertTimezone($value,true);
        return $value;

    }
    public function getStatusAttribute($value)
    {

        return ucwords($value);
    }


    public function getIsCompleteAttribute($value)
    {
        return ucwords($value);
    }

     public function getEndDtAttribute($value) {

        $value = \App\Http\Controllers\Base::ConvertTimezone($value,true);

           return $value;

    }

     public function getTimestampAttribute($value) {

        $value = \App\Http\Controllers\Base::ConvertTimezone($value,true);

           return $value;

    }

    protected $hidden = ['created_at', 'deleted_at','start_dt','end_dt'];

    protected $fillable = [
        'cust_id',
        'start_dt',
        'end_dt',
        'status',
        'type',
        'notes',
        'emp_id',
        'lat',
        'lng',
        'emp_cust_id',
        'remarks',
        'method',
        'timestamp',
        'signature',
        'delivery_phone',
        'delivery_to',
        'is_cust_delivery',
    ];

    protected $guarded = [];
}
