<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class GeoNotifications
 */
class GeoNotifications extends Model
{



    public function GeoAlerts()
    {
        return $this->belongsTo('App\Models\GeoAlerts', 'geo_id', 'id');
    }



    protected $table = 'geo_notifications';

    public $timestamps = true;

    protected $primaryKey = 'id';
    
    protected $fillable = [
        'is_mobile',
        'geo_id',
        'user_id',
        'message',
        'title',
        'vehicle_no',
        'read_at',
    ];

    protected $guarded = [];
}
