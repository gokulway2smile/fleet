<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use Illuminate\Notifications\Notifiable;

class Sample extends Model implements AuditableContract
{
    use Auditable,Notifiable;


         /**
     * Display timestamps in user's timezone
     */
    protected function asDateTime($value)
    {

        $value = \App\Http\Controllers\Base::ConvertTimezone($value);

        return $value;

    }



    


 // 'auth_user_id',
 //  'is_active',


    protected $table = 'sample';


    protected $primaryKey = 'id';

    public $timestamps = false;



    protected $fillable = [
        'first_name',
        'last_name',
        'mobile',
        'email',
        'address'
    ];

    protected $guarded = [];
}
