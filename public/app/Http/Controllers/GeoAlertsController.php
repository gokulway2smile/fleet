<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Base;
use App\Models\GeoAlerts;
use Illuminate\Http\Request;
use Validator;

class GeoAlertsController extends Controller
{
    public function index()
    {
       
        if ($this->admin || $this->backend) {
           $data = GeoAlerts::all();
        } elseif ($this->manager) {

            $belongsemp = Base::getEmpBelongsUser($this->emp_id);

            $data = GeoAlerts::whereIn('add_by', $belongsemp)->get()->toArray();

        } else {


             $data = GeoAlerts::where('add_by', $this->emp_id)->get()->toArray();

        }



        return Base::touser($data, true);
    }

    public function store(Request $request)
    {
        $data = $request->input('data');

        $rules = [
            'info' => 'required',
            'title' => 'required',
            'to_driver' => 'required',
            'loc_type' => 'required',
            'status' => 'required',
            'notifi_type' => 'required',
            'radius' => 'required',
        ];


        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return Base::touser($validator->errors()->all()[0]);
        }

        $prospect       = new GeoAlerts();

        $prospect->info = $data['info'];
        $prospect->title = $data['title'];
        $prospect->to_driver = $data['to_driver'];
        $prospect->status = $data['status'];
        $prospect->loc_type = $data['loc_type'];
        $prospect->notifi_type = $data['notifi_type'];
        $prospect->radius = $data['radius'];


        if(isset($data['address']) && $prospect->loc_type == 1) 
        {
             $prospect->address = $data['address'];
             $prospect->lat = $data['lat'];
             $prospect->lng = $data['lng'];
        }

         if(isset($data['loc_id']) && $prospect->loc_type == 0)
        {
             $prospect->loc_id = $data['loc_id'];
        }


        $prospect->add_by   = isset($data['add_by']) ? $data['add_by'] : $this->emp_id;


        $prospect->save();

        return Base::touser('Geo Alert Created', true);
    }

    public function show($id)
    {

        $data = GeoAlerts::find($id)->toArray();

        //   if(isset($data['loc_id']) && $data['loc_type'] == 0)
        // {
        //      $data['cust'] = \App\Models\Customer::find($data['loc_id']);
        // }

        
        return Base::touser($data, true);
    }

    public function update(Request $request, $id)
    {
        $data = $request->input('data');

        $rules = [
            'info' => 'required',
            'title' => 'required',
            'to_driver' => 'required',
            'loc_type' => 'required',
            'status' => 'required',
            'notifi_type' => 'required',
            'radius' => 'required',
        ];


        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return Base::touser($validator->errors()->all()[0]);
        }



        $prospect       =GeoAlerts::find($id);
        $prospect->info = $data['info'];
        $prospect->title = $data['title'];
        $prospect->to_driver = $data['to_driver'];
        $prospect->status = $data['status'];
        $prospect->loc_type = $data['loc_type'];
        $prospect->notifi_type = $data['notifi_type'];
        $prospect->radius = $data['radius'];


        if(isset($data['address']) && $prospect->loc_type == 1) 
        {
             $prospect->address = $data['address'];
             $prospect->lat = $data['lat'];
             $prospect->lng = $data['lng'];
        }

         if(isset($data['loc_id']) && $prospect->loc_type == 0)
        {
             $prospect->loc_id = $data['loc_id'];
        }


        $prospect->add_by   = isset($data['add_by']) ? $data['add_by'] : $this->emp_id;


        $prospect->save();

        return Base::touser('Geo Alert Created', true);

    }

    public function destroy($id)
    {
        try {
            $api = new GeoAlerts();

            $api = $api->where('id', '=', $id)->first();

            $api->delete();

            return Base::touser('Geo Alert Deleted', true);
        } catch (\Exception $e) {

            return Base::touser("Can't able to delete Geo Alert  its connected to Products !");
        
        }
    }

}
