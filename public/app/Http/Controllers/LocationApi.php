<?php
namespace App\Http\Controllers;

use App\Models\EmpSchedule as task;
use App\Models\TravelHistory as api;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use Toin0u\Geotools\Facade\Geotools;
use Validator;

class LocationApi extends Controller
{

    public function store(Request $request)
    {
        $rules = [
            'lat'       => 'required',
            'lng'       => 'required',
            'timestamp' => 'required',
            //  'compass_direction' => 'required',
        ];

        $data      = $request->input('data');
        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return Base::touser($validator->errors()->all()[0]);
        }

        $ch = false;

        if (isset($data['geo_status']) && !empty($data['geo_status'])) {

            $data['vehicle_no']    = isset($data['vehicle_no']) ? $data['vehicle_no'] : 'No Vehicle';
            $geoNotify             = new \App\Models\GeoNotifications;
            $geoNotify->user_id    = $this->emp_id;
            $geoNotify->is_mobile  = 1;
            $geoNotify->cust_id    = $data['cust_id'];
            $geoNotify->status     = $data['geo_status'];
            $geoNotify->vehicle_no = $data['vehicle_no'];
            $geoNotify->save();
            event(new \App\Events\GeoFenceFromMobile($geoNotify, $this->emp_id, $data['vehicle_no']));

            return Base::touser([], true);
        }

        if (($data['activity'] == 'Start') || ($data['activity'] == 'Stop')) {

            $last = api::where('user_id', $this->emp_id)
                ->get()->last();

            if ($last) {

                if ($last->activity == 'Monitor') {
                    $ch             = true;
                    $last->activity = "Stop";
                    $last->save();
                }

                if ($last->activity == 'Start') {

                    $api                    = new api;
                    $api->user_id           = $this->emp_id;
                    $api->accuracy          = $last->accuracy;
                    $api->speed             = $last->speed;
                    $api->bearing           = $last->bearing;
                    $api->lng               = $last->lng;
                    $api->lat               = $last->lat;
                    $api->battery_status    = $last->battery_status;
                    $api->compass_direction = $last->compass_direction;
                    $api->timestamp         = $last->timestamp;
                    $api->vehicle_no        = $last->vehicle_no;
                    $api->trip_id           = $last->trip_id;
                    $api->activity          = 'Stop';
                    $api->save();
                }
            }

        }

        $last = api::where('user_id', $this->emp_id)
            ->get()->last();

        $insert = false;
        if ($last) {
            if ($last->activity == 'Stop' && ($data['activity'] == 'Monitor' || $data['activity'] == 'Stop') && ($ch == false)) {
                $insert = true;
            }
        } else {
            if ($data['activity'] == 'Monitor' || $data['activity'] == 'Stop') {

                $insert = true;

            }

        }

        self::GPSDataClean();
        if ($insert == false) {
            $api                    = new api;
            $api->user_id           = $this->emp_id;
            $api->accuracy          = isset($data['accuracy']) ? $data['accuracy'] : null;
            $api->speed             = isset($data['speed']) ? $data['speed'] : null;
            $api->compass_direction = isset($data['compass_direction']) ? $data['compass_direction'] : null;
            $api->lng               = $data['lng'];
            $api->lat               = $data['lat'];
            $api->bearing           = isset($data['geo_status']) ? $data['geo_status'] : null;
            $api->battery_status    = isset($data['battery_status']) ? $data['battery_status'] : null;
            $api->timestamp         = Base::tomysqldatetime($data['timestamp']);
            $api->activity          = isset($data['activity']) ? $data['activity'] : null;
            $api->vehicle_no        = isset($data['vehicle_no']) ? $data['vehicle_no'] : null;
            $api->trip_id           = isset($data['trip_id']) ? $data['trip_id'] : null;
            $api->save();

            event(new \App\Events\LocationUpdate($api, $this->emp_id));

        }

        if ($insert == false) {
            self::checkGeoFence($api);
            event(new \App\Events\LocationUpdate($api, $this->emp_id));

            if ($api->trip_id) {
                $trip = \App\Models\EmpSchedule::find($api->trip_id);
                if (count($trip) == 1) {

                    if (($api->speed >= $trip->speed_limit) && !empty($trip->speed_limit)) {

                        $user = \App\Models\User::find($this->emp_id);

                        $data = \App\Models\Notifications::where('notifiable_type', 'App\Models\User')
                            ->where('type', 'App\Notifications\SpeedLimit')
                            ->where('notifiable_id', $this->emp_id)
                            ->where('data', 'like', '%"id":' . $trip->id . ',"%')
                            ->orderBy('created_at', 'desc')->first();

                        if (count($data) == 1) {
                            $datetime1 = new \DateTime();
                            $datetime2 = new \DateTime($data->created_at);
                            $interval  = $datetime1->diff($datetime2);
                            $elapsed   = (int) $interval->format('%i');
                            if ($elapsed > 30) //half hour
                            {
                                $notification = $user->notify(new \App\Notifications\SpeedLimit($trip, $user, $api->speed));
                                event(new \App\Events\NotificationEvent($user));
                            }
                            // $elapsed = $interval->format('%y years %m months %a days %h hours %i minutes %s seconds');
                            // echo $elapsed;
                        } else {
                            $notification = $user->notify(new \App\Notifications\SpeedLimit($trip, $user, $api->speed));
                            event(new \App\Events\NotificationEvent($user));
                        }

                    }
                }
                //here we go the stoppage limit logic
            }
        }

        return Base::touser([], true);
    }

    public function GPSDataClean()
    {

        // to clear the start and stop with out monitoring
        $checkold = api::where('user_id', $this->emp_id)
            ->orderBy('id', 'desc')
            ->take(2)->get();

        $st = false;
        if (count($checkold) == 2) {

            if ($checkold[0]['activity'] == 'Stop' && $checkold[1]['activity'] == 'Start') {
                $checkold = api::where('user_id', $this->emp_id)
                    ->orderBy('id', 'desc')
                    ->where('activity', '<>', 'Monitor')
                    ->where('activity', '<>', 'Ideal')
                    ->where('activity', '<>', 'Geofence')
                    ->take(2)->delete();
                $st = true;
            }
        }

        if ($st) {
            self::GPSDataClean();
        }

        return;

    }

    public function store1(Request $request)
    {
        $rules = [
            'lat'       => 'required',
            'lng'       => 'required',
            'timestamp' => 'required',

        ];

        $data      = $request->input('data');
        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return Base::touser($validator->errors()->all()[0]);
        }

        if (($data['activity'] == 'Start') || ($data['activity'] == 'Stop')) {
            $checkold = api::where('user_id', $this->emp_id)
                ->where('activity', '<>', 'Monitor')
                ->where('activity', '<>', 'Ideal')
                ->where('activity', '<>', 'Geofence')
                ->get()->last();
            if ($checkold) {

                if ($data['activity'] == $checkold->activity) {

                    $last = api::where('user_id', $this->emp_id)
                        ->get()->last();

                    if ($last->id == $checkold->id) {

                        $api                 = new api;
                        $api->user_id        = $this->emp_id;
                        $api->accuracy       = $last->accuracy;
                        $api->speed          = $last->speed;
                        $api->bearing        = $last->bearing;
                        $api->lng            = $last->lng;
                        $api->lat            = $last->lat;
                        $api->timestamp      = $last->timestamp;
                        $api->battery_status = $last->battery_status;
                        if ($data['activity'] == 'Start') {
                            $api->activity = "Stop";
                        } else {
                            $api->activity = "Start";
                        }

                        $api->save();
                    } else {

                        if ($data['activity'] == 'Start') {
                            $last->activity = "Stop";
                        } else {
                            $last->activity = "Start";
                        }

                        $last->save();
                    }

                }
            }
        }

        $api                 = new api;
        $api->user_id        = $this->emp_id;
        $api->accuracy       = isset($data['accuracy']) ? $data['accuracy'] : null;
        $api->speed          = isset($data['speed']) ? $data['speed'] : null;
        $api->bearing        = isset($data['geo_status']) ? $data['geo_status'] : null;
        $api->battery_status = isset($data['battery_status']) ? $data['battery_status'] : null;

        if (isset($data['geo_status']) && !empty($data['geo_status'])) {
            $geoNotify             = new \App\Models\GeoNotifications;
            $geoNotify->user_id    = $this->emp_id;
            $geoNotify->is_mobile  = 1;
            $geoNotify->cust_id    = $data['cust_id'];
            $geoNotify->status     = $data['geo_status'];
            $geoNotify->vehicle_no = $data['vehicle_no'];
            $geoNotify->save();
            event(new \App\Events\GeoFenceFromMobile($geoNotify, $this->emp_id, $data['vehicle_no']));
        }

        $api->accuracy   = isset($data['cust_id']) ? $data['cust_id'] : null;
        $api->vehicle_no = isset($data['vehicle_no']) ? $data['vehicle_no'] : null;
        $api->lng        = $data['lng'];
        $api->lat        = $data['lat'];
        $api->timestamp  = Base::tomysqldatetime($data['timestamp']);
        $api->activity   = isset($data['activity']) ? $data['activity'] : null;
        $api->trip_id    = isset($data['trip_id']) ? $data['trip_id'] : null;
        $api->save();

        self::checkGeoFence($api);

        event(new \App\Events\LocationUpdate($api, $this->emp_id));

        if ($api->trip_id) {
            $trip = \App\Models\EmpSchedule::find($api->trip_id);
            if (count($trip) == 1) {

                if (($api->speed >= $trip->speed_limit) && !empty($trip->speed_limit)) {

                    $user = \App\Models\User::find($this->emp_id);

                    $data = \App\Models\Notifications::where('notifiable_type', 'App\Models\User')
                        ->where('type', 'App\Notifications\SpeedLimit')
                        ->where('notifiable_id', $this->emp_id)
                        ->where('data', 'like', '%"id":' . $trip->id . ',"%')
                        ->orderBy('created_at', 'desc')->first();

                    if (count($data) == 1) {
                        $datetime1 = new \DateTime();
                        $datetime2 = new \DateTime($data->created_at);
                        $interval  = $datetime1->diff($datetime2);
                        $elapsed   = (int) $interval->format('%i');
                        if ($elapsed > 30) //half hour
                        {
                            $notification = $user->notify(new \App\Notifications\SpeedLimit($trip, $user, $api->speed));
                            event(new \App\Events\NotificationEvent($user));
                        }
// $elapsed = $interval->format('%y years %m months %a days %h hours %i minutes %s seconds');
                        // echo $elapsed;
                    } else {
                        $notification = $user->notify(new \App\Notifications\SpeedLimit($trip, $user, $api->speed));
                        event(new \App\Events\NotificationEvent($user));
                    }

                }
            }
            //here we go the stoppage limit logic
        }

        return Base::touser([], true);
    }

    public function getUserTodayGeoAlerts()
    {
        $users = [];

        $data = \App\Models\GeoNotifications::where('user_id', $this->emp_id)
            ->whereRaw('Date(updated_at) = CURDATE()')
            ->get(['geo_id'])->toArray();

        foreach ($data as $key => $value) {

            $users[] = $data[$key]['geo_id'];
        }

        return $users;
    }

    public function GetNotifications()
    {

        if ($this->manager) {

            $belongsemp = Base::getEmpBelongsUser($this->emp_id);

            $data = \App\Models\GeoNotifications::whereIn('user_id', $belongsemp)->with('GeoAlerts')
                  ->whereNull('read_at')
                ->paginate(10);
        } elseif ($this->backend || $this->admin) {

            $data = \App\Models\GeoNotifications::all();

        } else {

            $data = \App\Models\GeoNotifications::where('user_id', $this->emp_id)
               ->whereNull('read_at')
                ->get()->toArray();

        }

        return Base::touser($data, true);
    }

    public function ReadNotifications($id)
    {

        $data = \App\Models\GeoNotifications::where('id',$id)->first();

        $data->read_at = date('Y-m-d h:i:s');

        $data->save();

        return Base::touser($data, true);
    }

    public function checkGeoFence(\App\Models\TravelHistory $api)
    {

        $coordA = Geotools::coordinate([$api->lat, $api->lng]);
        $user = \App\Models\User::where("user_id",$this->emp_id)->first();

        $allgeo = \App\Models\GeoAlerts::whereNotIn('id', self::getUserTodayGeoAlerts())
            ->where('status', '=', 1)
            ->where('add_by', '=', $user->belongs_manager) ;

        $key_name = 'fleets_' . $this->emp_id . '_geofence_id-' . date('Y-m-d', strtotime('yesterday')) . '-';

        $keys = Redis::keys($key_name . '*');

        foreach ($keys as $key) {
            if (strpos($key, $key_name)) {
                Redis::del($key);
            }
        }

        $allmatched = $allgeo->each(function ($item, $key) use ($coordA, $api) {

            $key_name = 'fleets_' . $this->emp_id . '_geofence_id-' . date('Y-m-d') . '-' . $item->id;

            if ($item->loc_type == 1) // address
            {
                $coordB = Geotools::coordinate([$item->lat, $item->lng]);
            } else {
                $customers = \App\Models\Customer::find($item->loc_id);
                $coordB    = Geotools::coordinate([$customers->loc_lat, $item->loc_lng]);
            }

            if ($item->notifi_type == 0) // entry
            {
                $distance = Geotools::distance()->setFrom($coordA)->setTo($coordB);

                if ($item->radius >= $distance->flat()) {

                    $geoNotify = new \App\Models\GeoNotifications;

                    $geoNotify->geo_id     = $item->id;
                    $geoNotify->user_id    = $this->emp_id;
                    $geoNotify->message    = $item->info;
                    $geoNotify->title      = $item->title;
                    $geoNotify->is_mobile  = 0;
                    $geoNotify->vehicle_no = $api->vehicle_no;
                    $geoNotify->save();

                    event(new \App\Events\GeoFenceFromWeb($geoNotify, $this->emp_id, $api->vehicle_no));

                }

            } else {

                $distance = Geotools::distance()->setFrom($coordA)->setTo($coordB);

                $redis = Redis::exists($key_name); //check in redis

                if ($redis) {

                    if ($item->radius <= $distance->flat()) {

                        // exit check
                        $geoNotify             = new \App\Models\GeoNotifications;
                        $geoNotify->geo_id     = $item->id;
                        $geoNotify->user_id    = $this->emp_id;
                        $geoNotify->message    = $item->info;
                        $geoNotify->title      = $item->title;
                        $geoNotify->is_mobile  = 0;
                        $geoNotify->vehicle_no = $api->vehicle_no;
                        $geoNotify->save();
                        Redis::del($key_name);
                        event(new \App\Events\GeoFenceFromWeb($geoNotify, $this->emp_id, $api->vehicle_no));
                    }

                } else {

                    if ($item->radius >= $distance->flat()) {

                        Redis::set($key_name, date('Y-m-d H:s:i'));

                        // to keep the entry in redis for furture exit cal
                    }
                }

            }

        });

    }

    public function GeoFenceFromMobile()
    {

        $geoNotify            = new \App\Models\GeoNotifications;
        $geoNotify->geo_id    = $item->id;
        $geoNotify->user_id   = $this->emp_id;
        $geoNotify->message   = $item->info;
        $geoNotify->title     = $item->title;
        $geoNotify->is_mobile = 1;
        $item->save();

    }

    public function TravelClear()
    {
        \App\Models\TravelHistory::where('user_id', $this->emp_id)->delete();

        return 'User Travel Cleared :)';
    }

    public function get($id)
    {
        $array = api::orderBy('timestamp', 'desc')->with('user')->where('user_id', '=', $id)->first();

        $array['timestamp'] = Base::time_elapsed_string($array['timestamp']);

        return Base::touser($array, true);
    }

    public function locationbulk(Request $request)
    {

        $data = $request->input('data');

        foreach ($data as $key => $value) {

            // $api             = new api;
            // $api->user_id    = $this->emp_id;
            // $api->accuracy   = isset($data['accuracy']) ? $data['accuracy'] : null;
            // $api->speed      = isset($data['speed']) ? $data['speed'] : null;
            // $api->bearing    = isset($data['geo_status']) ? $data['geo_status'] : null;
            // $api->accuracy   = isset($data['cust_id']) ? $data['cust_id'] : null;
            // $api->vehicle_no = isset($data['vehicle_no']) ? $data['vehicle_no'] : null;
            // $api->lng        = $data['lng'];
            // $api->lat        = $data['lat'];
            // $api->timestamp  = Base::tomysqldatetime($data['timestamp']);
            // $api->activity   = isset($data['activity']) ? $data['activity'] : null;

            // $api->trip_id = isset($data['trip_id']) ? $data['trip_id'] : null;

            // $api->save();

            $api                 = new api;
            $api->user_id        = $this->emp_id;
            $api->accuracy       = isset($data[$key]['cust_id']) ? $data[$key]['cust_id'] : null;
            $api->speed          = isset($data[$key]['speed']) ? $data[$key]['speed'] : null;
            $api->vehicle_no     = isset($data[$key]['vehicle_no']) ? $data[$key]['vehicle_no'] : null;
            $api->bearing        = isset($data[$key]['geo_status']) ? $data[$key]['geo_status'] : null;
            $api->lng            = $data[$key]['lng'];
            $api->lat            = $data[$key]['lat'];
            $api->timestamp      = Base::tomysqldatetime($data[$key]['timestamp']);
            $api->activity       = isset($data[$key]['activity']) ? $data[$key]['activity'] : null;
            $api->battery_status = isset($data[$key]['battery_status']) ? $data[$key]['battery_status'] : null;
            $api->trip_id        = isset($data[$key]['trip_id']) ? $data[$key]['trip_id'] : null;
            $api->save();

        }

        return Base::touser([], true);
    }

    public function emp_getonline(Request $request)
    {
        $array = [];

        if ($this->manager) {

            $belongsemp = Base::getEmpBelongsUser($this->emp_id);
            $array      = api::with('user')
                ->whereIn('user_id', $belongsemp)
                ->orderBy('timestamp', 'desc')
                ->get()->unique('user_id');
        } else {
            $array = api::with('user')->orderBy('timestamp', 'desc')->get()->unique('user_id');
        }

        foreach ($array as $key => $value) {

            $first = api::with('user')
                ->where('trip_id', $value->trip_id)
                ->orderBy('timestamp', 'asc')->first();

            $coordA   = Geotools::coordinate([$first->lat, $first->lng]);
            $coordB   = Geotools::coordinate([$value->lat, $value->lng]);
            $distance = Geotools::distance()->setFrom($coordA)->setTo($coordB);

            $array[$key]['Distance'] = round($distance->in('km')->haversine(), 2);

        }

        // echo $array;
        // // $array = api::union('user_id')->orderBy('timestamp', 'desc')->get();

        // foreach ($array as $i => $item) {
        //     // $array[$i]['timestamp'] = Base::time_elapsed_string($array[$i]['timestamp']);

        //     $array[$i]['user']['profile_image'] = (Array) json_decode(stripslashes($array[$i]['user']['profile_image']));

        // $array[$i]['content'] =
        // '<img width=100 height=100 src="' . $array[$i]['user']['profile_image'][0] . '"/>';

        // if(!$array[$i]['user']['profile_image'])
        // {
        //     $array[$i]['user']['profile_image'][0] = '';
        // }

        // $array[$i]['content'] =
        // //'<img width=50 src="' . $array[$i]['user']['profile_image'][0] . '"/>'
        // '<p>Emp id :' . $array[$i]['user']['user_id']
        //     . '</p> '
        //     . '<p>Name :' . $array[$i]['user']['last_name']
        //     . ' '
        //     . $array[$i]['user']['first_name']
        //     . '</p> '
        //     . '<p> Active :'
        //     . $array[$i]['timestamp'] . '</p>'

        //     ;

// + 'Time : <h5>'
        //   // + emp.timestamp
        //   // + '</h5>

        // + 'Emp _ID : '
        // + emp.user.user_id
        // +'- '
        // + emp.user.first_name
        // +' '
        // + emp.user.last_name
        // + '<img ng-src="'
        // +emp.user.profile_image[0]
        // +'"/>'
        // + 'Time : <h5>'
        // + emp.timestamp
        // + '</h5></p>'
        // }

        return Base::touser($array, true);
    }

    public function vehicle_stop_filter(Request $request)
    {
        $data = $request->input('data');

        if ((null !== $data['start_date']) && (null !== $data['end_date'])) {

            $start = Base::tomysqldatetime($data['start_date']);
            $end   = Base::tomysqldatetime($data['end_date']);
            $user_id       = (int)$data['emp_id'];


            $array = api::select( \DB::raw("*,user_id as user_id,timestamp as timestamp,activity,
       TIMESTAMPDIFF(SECOND,
         timestamp,
         (SELECT IF(activity='Stop',timestamp,'null')  FROM travel_history WHERE id = t.id+1)
       ) as time_diff_mins" ) )
            ->from('travel_history as t')
            ->having('time_diff_mins','>',300)
            ->having('user_id','=',$user_id)
            ->having('timestamp','>=',$start)
            ->having('timestamp','<=',$end)
            // ->toSql();

            // print_r($array);

            // die();

            ->get()->toArray();



     //        $array = \DB::select("SELECT *,user_id as user_id,timestamp as timestamp,activity,
     //   TIMESTAMPDIFF(SECOND,
     //     timestamp,
     //     (SELECT IF(activity='Stop',timestamp,'null')  FROM travel_history WHERE id = t.id+1)
     //   ) as time_diff_mins
     // FROM travel_history t
     // having (time_diff_mins > 5 and user_id = '".$user_id."') and (timestamp >= '".$start."'
     // and  timestamp <=  '".$end."' )
     // ");

            $data               = [];
            $data['visit_list'] = $array;
            return Base::touser($data, true);
        } else {
            Base::touser([], false);
        }
    }

    public function vehicle_speed_filter(Request $request)
    {
        $data = $request->input('data');

        if ((null !== $data['start_date']) && (null !== $data['end_date'])) {

            $start_time = Base::tomysqldatetime($data['start_date']);
            $end_time   = Base::tomysqldatetime($data['end_date']);

            if ($this->manager) {
                $array = api::
                    orderBy('timestamp', 'asc')->
                    where('user_id', '=', (int) $data['emp_id'])->
                    where('timestamp', '<=', $end_time)->
                    // with('trip_id')->
                    where('speed', '>', 5)->
                    where('timestamp', '>=', $start_time)->
                    get(['timestamp','speed'])->toArray();

                $data               = [];
                $data['visit_list'] = $array;

                return Base::touser($data, true);
            }

        } else {
            Base::touser([], false);
        }
    }

    public function tripSummary(Request $request)
    {
        $data = $request->input('data');

        if ((null !== $data['start_date']) && (null !== $data['end_date']) && (null !== $data['emp_id'])) {

            $start_time = Base::tomysqldatetime($data['start_date']);
            $end_time   = Base::tomysqldatetime($data['end_date']);

            $data['emp_id'] = (int) $data['emp_id'];

            $array = api::
                orderBy('timestamp', 'asc')->
                where('user_id', '=', $data['emp_id'])->
                where('timestamp', '<=', $end_time)->
                where('timestamp', '>=', $start_time)->
                where('activity', '<>', 'Monitor')->
                where('activity', '<>', 'Ideal')->
                with('trip_id')->
                with('vehicle_info')->
                get();


            $data       = [];
            $data_array = $array->toArray();

            foreach ($data_array as $key => $value) {

                if (array_key_exists($key - 1, $data_array)) {

                    $first = $data_array[$key - 1];

                    $coordA = Geotools::coordinate([$first['lat'], $first['lng']]);

                    $coordB = Geotools::coordinate([$value['lat'], $value['lng']]);

                    $distance = Geotools::distance()->setFrom($coordA)->setTo($coordB);

                    $time1    = new \DateTime($first['timestamp']);
                    $time2    = new \DateTime($value['timestamp']);
                    $interval = $time1->diff($time2);

                    $value['distance']   = round($distance->in('km')->haversine(), 2);
                    $value['accuracy']   = (int) $value['accuracy'];
                    $value['time_taken'] = $interval->h . ' hours ' . $interval->i . ' mins ';
                    $value['time_api']   = $interval->h . ':' . $interval->i;

                } else {
                    $value['distance']   = '';
                    $value['time_taken'] = '';
                }

                $data_array[$key] = $value;

            }

            $data['trips_list'] = $array->unique('trip_id');
            // $data['trip_ids']  = $array->unique('trip_id')->pluck("trip_id");
            $data['visits_list'] = $data_array;

            return Base::touser($data, true);
        } else {
            Base::touser([], false);
        }
    }

    public function emp_filter(Request $request)
    {
        $data = $request->input('data');

        if ((null !== $data['start_date']) && (null !== $data['end_date'])) {
            $start_time = Base::tomysqldatetime($data['start_date']);
            $end_time   = Base::tomysqldatetime($data['end_date']);

            $period = [$start_time, $end_time];

            if ($data['emp_id'] == 'all') {

                if ($this->manager) {

                    $belongsemp = Base::getEmpBelongsUser($this->emp_id);

                    $tasks = task::whereIn('emp', $belongsemp)

              ->where(function ($query) use ($period) {
                    $query->where(function ($query) use ($period) {

                            $query
                                ->where("start_dt", '>=', $period[0])
                                ->where("end_dt", '<=', $period[0]);

                        })

                        ->orwhere(function ($query) use ($period) {

                            $query
                                ->where("start_dt", '<=', $period[0])
                                ->where("end_dt", '>=',$period[0]);

                        })

                        ->orwhere(function ($query) use ($period) {

                            $query
                                ->where("start_dt", '>=',$period[0])
                                ->where("start_dt", '<=', $period[1]);
                        })
                        ->orwhere(function ($query) use ($period) {

                            $query
                                ->where("start_dt", '<=', $period[0])
                                ->WhereNull("end_dt");
                        });

                          })
                        ->with('tasks.cust_info')
                        ->with('vehicle_info')
                        ->with('emp_info')

                        ->get()->toArray();

                } else {

                    $tasks = task::
                     where(function ($query) use ($period) {
                                 $query->where(function ($query) use ($period) {

                            $query
                                ->where("start_dt", '>=', $period[0])
                                ->where("end_dt", '<=', $period[0]);

                        })

                        ->orwhere(function ($query) use ($period) {

                            $query
                                ->where("start_dt", '<=', $period[0])
                                ->where("end_dt", '>=',$period[0]);

                        })

                        ->orwhere(function ($query) use ($period) {

                            $query
                                ->where("start_dt", '>=',$period[0])
                                ->where("start_dt", '<=', $period[1]);
                        })
                        ->orwhere(function ($query) use ($period) {

                            $query
                                ->where("start_dt", '<=', $period[0])
                                ->WhereNull("end_dt");
                        });
                          })

                        ->with('tasks.cust_info')
                        ->with('vehicle_info')
                        ->with('emp_info')

                    // print_r($tasks);
                    // die();
                        ->get()->toArray();
                }

            } else {

                $tasks = task::where('emp', '=', $data['emp_id'])

                 ->where(function ($query) use ($period) {


                                     $query->where(function ($query) use ($period) {

                            $query
                                ->where("start_dt", '>=', $period[0])
                                ->where("end_dt", '<=', $period[0]);

                        })

                        ->orwhere(function ($query) use ($period) {

                            $query
                                ->where("start_dt", '<=', $period[0])
                                ->where("end_dt", '>=',$period[0]);

                        })

                        ->orwhere(function ($query) use ($period) {

                            $query
                                ->where("start_dt", '>=',$period[0])
                                ->where("start_dt", '<=', $period[1]);
                        })
                        ->orwhere(function ($query) use ($period) {

                            $query
                                ->where("start_dt", '<=', $period[0])
                                ->WhereNull("end_dt");
                        });
                        })

                    ->with('tasks.cust_info')
                    ->with('vehicle_info')
                    ->with('emp_info')
                //     ->toSql();
                // print_r($tasks);
                // die();
                    ->get()->toArray();

            }

            $info  = [];
            $array = [];

            foreach ($tasks as $key => $value) {

                foreach ($value['tasks'] as $key1 => $val) {

                    $val['start_dt']         = $value['start_dt'];
                    $val['end_dt']           = $value['end_dt'];
                    $val['trip_id']          = $value['id'];
                    $val['cust_name']        = $val['cust_info']['name'];
                    $val['cust_type']        = $val['cust_info']['type'];
                    $val['cust_address']     = $val['cust_info']['address'];
                    $val['cust_phone']       = $val['cust_info']['contact_no'];
                    $val['loc_lat']          = $val['cust_info']['loc_lat'];
                    $val['loc_lng']          = $val['cust_info']['loc_lng'];
                    $val['allocated_emp']    = $value['emp_info']['first_name'] . ' ' . $value['emp_info']['last_name'];
                    $val['allocated_emp_id'] = $value['emp_info']['user_id'];
                    $val['loc_lng']          = $val['cust_info']['loc_lng'];
                    $val['loc_lng']          = $val['cust_info']['loc_lng'];

                    $array[] = $val;

                }

            }

            $info['visit_list'] = $array;

            return Base::touser($info, true);
        } else {
            Base::touser([], false);
        }
    }

    public function current_emp_filter(Request $request)
    {
        $start = $request->input('start');
        $end   = $request->input('end');
        $date  = $request->input('date');
        if ($this->manager) {
            $emp_id = (null !== $request->input('emp')) ? $request->input('emp') : $this->emp_id;
        } else {
            $emp_id = $this->emp_id;
        }

        // $start = Base::tomysqldatetime($start);
        // $end   = Base::tomysqldatetime($end);

        if ((null !== $start) && (null !== $end)) {

            $start = Base::tomysqldatetime($start);
            $end   = Base::tomysqldatetime($end);

        } else {
            $start = Base::tomysqldate($date) . ' 00:00:0';
            $end   = Base::tomysqldate($date) . ' 23:59:00';
        }

        $array = api::
            orderBy('timestamp', 'asc')->
            where('user_id', '=', $emp_id)->
            where('timestamp', '<=', $end)->
            where('timestamp', '>=', $start)
            ->get()->toArray();

        $distance = 0;

        for ($key = 0; $key < count($array) - 1; $key++) {
            $coordA   = Geotools::coordinate([$array[$key]['lat'], $array[$key]['lng']]);
            $coordB   = Geotools::coordinate([$array[$key + 1]['lat'], $array[$key + 1]['lng']]);
            $dist     = Geotools::distance()->setFrom($coordA)->setTo($coordB);
            $distance = round($dist->in('km')->haversine(), 2) + $distance;

        }

        $start      = false;
        $end        = false;
        $time_taken = 'No Data';
        if (count($array) > 1) {
            if ($array[0]['timestamp']) {
                $start = $array[0]['timestamp'];
            }

            if ($array[count($array) - 1]['timestamp']) {
                $end = $array[count($array) - 1]['timestamp'];
            }

            if (($start) && ($end)) {
                $time_taken = Base::time_elapsed_string($end, true, $start);

                if (empty($time_taken)) {
                    $time_taken = '1 min';
                }
            }
        }

        $data               = [];
        $data['geoData']    = $array;
        $data['distance']   = round($distance, 2) . ' Kms / Hours';
        $data['time_taken'] = $time_taken;

        return Base::touser($data, true);

    }

    public function emp(Request $request, $id)
    {
        $array = [];

        $array = api::orderBy('timestamp', 'desc')->with('user')->where('user_id', '=', $id)->get()->toArray();

        // $array = api::union('user_id')->orderBy('timestamp', 'desc')->get();

        foreach ($array as $i => $value) {
            $array[$i]['timestamp'] = Base::time_elapsed_string($array[$i]['timestamp']);

            $array[$i]['user']['profile_image'] = (Array) json_decode(stripslashes($array[$i]['user']['profile_image']));
        }

        return Base::touser($array, true);
    }
}
