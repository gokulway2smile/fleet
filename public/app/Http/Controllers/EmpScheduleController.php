<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Base;
use App\Models\EmpCustSchedule as emp_cust;
use App\Models\EmpSchedule as task;
use App\Models\ScheduleTaskStatus;
use App\Models\TravelHistory as api;
use Illuminate\Http\Request;
use Toin0u\Geotools\Facade\Geotools;
use Validator;

class EmpScheduleController extends Controller
{

    public function getTaskSummary(Request $request, $task_id)
    {

        try
        {
            $task = emp_cust::where('id', $task_id)->with('all_status')->first()->toArray();

        } catch (Exception $e) {
            return Base::touser('Task not found');
        }

        if (count($task) < 1) {
            return Base::touser('Task not found');
        } else {

            if (count($task['all_status']) < 1) {
                return Base::touser('Task Status not found');
            }

            if ((Base::mobile_header() == 1) && ($task['emp_id'] != $this->emp_id)) {
                return Base::touser('Task Status not belongs to you');
            }

            $taskStatus = array_reverse($task['all_status']);

            $Progress = array_first($taskStatus, function ($value, $key) use ($task) {

                if ($value['emp_id'] == $task['emp_id']) {
                    return $value['status'] == 'In-Progress';
                }

            });

            $Delivered = array_first($taskStatus, function ($value, $key) use ($task) {

                if ($value['emp_id'] == $task['emp_id']) {

                    return $value['status'] == 'Completed';
                }

            });

            if ($Progress) {

                if ($Delivered) {

                    $data = $Delivered;
                     if ($data['timestamps']) {
                        $end = $data['timestamps'];
                    } else {
                        $end = $data['updated_at'];
                    }
                  }

                    if ($Progress['timestamps']) {
                        $start = $Progress['timestamps'];
                    } else {
                        $start = $Progress['updated_at'];
                    }

                    if (!$start) {
                        return Base::touser('Task Date Error');
                    }

                    $end = isset($end) ? $end : Base::current_client_datetime();

                    // $end = "2017-07-04 05:35:00";
                    $start = $start;

                    $gpsData = api::
                        orderBy('timestamp', 'asc')->
                        where('user_id', $task['emp_id'])->
                        where('timestamp', '<=', Base::tomysqldatetime($end))->
                        where('timestamp', '>=', Base::tomysqldatetime($start))->
                        get()->toArray();

                    $distInMeter = 0;

                    for ($x = 0; $x < count($gpsData) - 1; $x++) {

                        $data1                   = $gpsData[$x];
                        $data2                   = $gpsData[$x + 1];
                        $gpsData[$x]['path']     = [$data1['lat'], $data1['lng']];
                        $gpsData[$x + 1]['path'] = [$data2['lat'], $data2['lng']];
                        $coordA                  = Geotools::coordinate($gpsData[$x]['path']);
                        $coordB                  = Geotools::coordinate($gpsData[$x + 1]['path']);
                        $distance                = Geotools::distance()->setFrom($coordA)->setTo($coordB);
                        $distInMeter             = $distance->flat() + $distInMeter;

                    }

                    $time_taken = Base::time_elapsed_string($end, true, $start);

                    if (empty($time_taken)) {
                        $time_taken = '1 min';
                    }

                    $distInMeter = $distInMeter / 1000;
                    $Summary     = [
                        'time_taken' => $time_taken,
                        'start'      => $start,
                        'end'        => $end,
                        'gpsData'    => $gpsData,
                        'distance'   => round($distInMeter, 2) . ' kms',
                    ];

                    return Base::touser($Summary, true);


            }

            return Base::touser('Task Status Error');
        }

    }

    public function index(Request $request)
    {
        if ($this->admin || $this->backend) {
            $array = task::with('cust_jobs')->with('emp_info')->get()->toArray();

            foreach ($array as $key => $value) {

                $array[$key]['emp_info'] = $array[$key]['emp_info']['first_name'] . ' ' . $array[$key]['emp_info']['last_name'];
            }
        } elseif ($this->manager) {

            $belongsemp = Base::getEmpBelongsUser($this->emp_id);

            if ($request->input('mobile_mode')) {

                if ($request->input('date')) {
                    $value = Base::tomysqldate($request->input('date'));
                } else {
                    $value = date('Y-m-d');
                }

                if ($request->input('start') !== null || $request->input('end') !== null) {

                    $start_dt = Base::tomysqldatetime($request->input('start'));
                    $end_dt   = Base::tomysqldatetime($request->input('end'));
                    $emp      = $request->input('emp');

                    $period = [$start_dt, $end_dt];

                    $new = task::where('emp', $emp)

                      ->where(function ($query) use ($period) {

                        $query->where(function ($query) use ($period) {

                            $query
                                ->where("start_dt", '>=', $period[0])
                                ->where("end_dt", '<=', $period[0]);

                        })

                        ->orwhere(function ($query) use ($period) {

                            $query
                                ->where("start_dt", '<=', $period[0])
                                ->where("end_dt", '>=', $period[0]);

                        })

                        ->orwhere(function ($query) use ($period) {

                            $query
                                ->where("start_dt", '>=', $period[0])
                                ->where("start_dt", '<=', $period[1]);
                        })
                        ->orwhere(function ($query) use ($period) {

                            $query
                                ->where("start_dt", '<=', $period[0])
                                ->WhereNull("end_dt");
                        });
                          })
                        ->with('tasks.cust_info')
                        ->with('vehicle_info')
                        ->with('emp_info')
                    // ->toSql();

                    // return $new;
                        ->get()->toArray();

                } else {

                    $new = task::whereIn('emp', $belongsemp)
                     ->where(function ($query) use ($value) {

                        $query->where(function ($query) use ($value) {

                            $query
                                ->where("start_dt", '>=', Base::tomysqldatetime($value . ' 00:00:00'))
                                ->where("end_dt", '<=', Base::tomysqldatetime($value . ' 00:00:00'));

                        })

                        ->orwhere(function ($query) use ($value) {

                            $query
                                ->where("start_dt", '<=', Base::tomysqldatetime($value . ' 00:00:00'))
                                ->where("end_dt", '>=', Base::tomysqldatetime($value . ' 00:00:00'));

                        })

                        ->orwhere(function ($query) use ($value) {

                            $query
                                ->where("start_dt", '>=', Base::tomysqldatetime($value . ' 00:00:00'))
                                ->where("start_dt", '<=', Base::tomysqldatetime($value . ' 23:59:59'));
                        })
                        ->orwhere(function ($query) use ($value) {

                            $query
                                ->where("start_dt", '<=', Base::tomysqldatetime($value . ' 00:00:00'))
                                ->WhereNull("end_dt");
                        });
                          })
                        ->with('tasks.cust_info')
                        ->with('vehicle_info')
                        ->with('emp_info')

                        ->get()->toArray();

                }
                $array = [];

                foreach ($new as $key => $value) {

                    foreach ($value['tasks'] as $key1 => $val) {

                        $val['start_dt']         = $value['start_dt'];
                        $val['end_dt']           = $value['end_dt'];
                        $val['trip_id']          = $value['id'];
                        $val['cust_name']        = $val['cust_info']['name'];
                        $val['cust_type']        = $val['cust_info']['type'];
                        $val['cust_address']     = $val['cust_info']['address'];
                        $val['cust_phone']       = $val['cust_info']['contact_no'];
                        $val['loc_lat']          = $val['cust_info']['loc_lat'];
                        $val['loc_lng']          = $val['cust_info']['loc_lng'];
                        $val['allocated_emp']    = $value['emp_info']['first_name'] . ' ' . $value['emp_info']['last_name'];
                        $val['allocated_emp_id'] = $value['emp_info']['user_id'];
                        $val['loc_lng']          = $val['cust_info']['loc_lng'];
                        $val['loc_lng']          = $val['cust_info']['loc_lng'];

                        $array[] = $val;

                    }

                }

            } else {

                $array = task::with('cust_jobs', 'emp_info')->whereIn('emp', $belongsemp)->get()->toArray();

                foreach ($array as $key => $value) {

                    $array[$key]['emp_info'] = $array[$key]['emp_info']['first_name'] . ' ' . $array[$key]['emp_info']['last_name'];
                }
            }

        } else {

            if ($request->input('date')) {
                $value = Base::tomysqldate($request->input('date'));
            } else {
                $value = date('Y-m-d');
            }

            if ($request->input('page')) {

                if ($request->input('start') !== null || $request->input('end') !== null) {

                    $start_dt = Base::tomysqldatetime($request->input('start'));
                    $end_dt   = Base::tomysqldatetime($request->input('end'));
                    $period   = [$start_dt, $end_dt];

                    if ($request->input('filterStatus') == 'deliveries') {

                        $array = task::where('emp', '=', $this->emp_id)
                        ->where(function ($query) use ($period) {

                             $query->where(function ($query) use ($period) {

                                $query
                                    ->where("start_dt", '>=', $period[0])
                                    ->where("end_dt", '<=', $period[0]);

                            })

                            ->orwhere(function ($query) use ($period) {

                                $query
                                    ->where("start_dt", '<=', $period[0])
                                    ->where("end_dt", '>=', $period[0]);

                            })

                            ->orwhere(function ($query) use ($period) {

                                $query
                                    ->where("start_dt", '>=', $period[0])
                                    ->where("start_dt", '<=', $period[1]);
                            })
                            ->orwhere(function ($query) use ($period) {

                                $query
                                    ->where("start_dt", '<=', $period[0])
                                    ->WhereNull("end_dt");
                            });
                            })

                            ->with('vehicle_info')
                            ->with('tasks.cust_info')

                            ->paginate(10);

                    } else {

                        $array = task::where('emp', '=', $this->emp_id)
                          ->where(function ($query) use ($period) {
                            $query->where(function ($query) use ($period) {

                                $query
                                    ->where("start_dt", '>=', $period[0])
                                    ->where("end_dt", '<=', $period[0]);

                            })

                            ->orwhere(function ($query) use ($period) {

                                $query
                                    ->where("start_dt", '<=', $period[0])
                                    ->where("end_dt", '>=', $period[0]);

                            })

                            ->orwhere(function ($query) use ($period) {

                                $query
                                    ->where("start_dt", '>=', $period[0])
                                    ->where("start_dt", '<=', $period[1]);
                            })
                            ->orwhere(function ($query) use ($period) {

                                $query
                                    ->where("start_dt", '<=', $period[0])
                                    ->WhereNull("end_dt");
                            });
                            })

                            ->with('tasks.cust_info')
                            ->with('vehicle_info')
                            ->paginate(10);

                    }

                } else {

                    $array = task::where('emp', '=', $this->emp_id)
           ->where(function ($query) use ($value) {

                        $query->where(function ($query) use ($value) {

                            $query
                                ->where("start_dt", '>=', Base::tomysqldatetime($value . ' 00:00:00'))
                                ->where("end_dt", '<=', Base::tomysqldatetime($value . ' 00:00:00'));

                        })

                        ->orwhere(function ($query) use ($value) {

                            $query
                                ->where("start_dt", '<=', Base::tomysqldatetime($value . ' 00:00:00'))
                                ->where("end_dt", '>=', Base::tomysqldatetime($value . ' 00:00:00'));

                        })

                        ->orwhere(function ($query) use ($value) {

                            $query
                                ->where("start_dt", '>=', Base::tomysqldatetime($value . ' 00:00:00'))
                                ->where("start_dt", '<=', Base::tomysqldatetime($value . ' 23:59:59'));
                        })
                        ->orwhere(function ($query) use ($value) {

                            $query
                                ->where("start_dt", '<=', Base::tomysqldatetime($value . ' 00:00:00'))
                                ->WhereNull("end_dt");
                        });
                          })

                        ->with('tasks.cust_info')
                        ->with('vehicle_info')
                        ->paginate(10);
                }
            } else {

                if ($request->input('start') !== null || $request->input('end') !== null) {

                    $start_dt = Base::tomysqldatetime($request->input('start'));
                    $end_dt   = Base::tomysqldatetime($request->input('end'));
                    $period   = [$start_dt, $end_dt];

                    $array = task::where('emp', $this->emp_id)
                      ->where(function ($query) use ($period) {
                        $query->where(function ($query) use ($period) {

                            $query
                                ->where("start_dt", '>=', $period[0])
                                ->where("end_dt", '<=', $period[0]);

                        })

                        ->orwhere(function ($query) use ($period) {

                            $query
                                ->where("start_dt", '<=', $period[0])
                                ->where("end_dt", '>=', $period[0]);

                        })

                        ->orwhere(function ($query) use ($period) {

                            $query
                                ->where("start_dt", '>=', $period[0])
                                ->where("start_dt", '<=', $period[1]);
                        })
                        ->orwhere(function ($query) use ($period) {

                            $query
                                ->where("start_dt", '<=', $period[0])
                                ->WhereNull("end_dt");
                        });
                        })

                        ->with('tasks.cust_info')
                        ->with('vehicle_info')
                        ->get()->toArray();
                    return Base::touser($array, true);
                } else {

                    $array = task::where('emp', $this->emp_id)

                        ->where(function ($query) use ($value) {
                        $query->where(function ($query) use ($value) {

                            $query
                                ->where("start_dt", '>=', Base::tomysqldatetime($value . ' 00:00:00'))
                                ->where("end_dt", '<=', Base::tomysqldatetime($value . ' 00:00:00'));

                        })

                        ->orwhere(function ($query) use ($value) {

                            $query
                                ->where("start_dt", '<=', Base::tomysqldatetime($value . ' 00:00:00'))
                                ->where("end_dt", '>=', Base::tomysqldatetime($value . ' 00:00:00'));

                        })

                        ->orwhere(function ($query) use ($value) {

                            $query
                                ->where("start_dt", '>=', Base::tomysqldatetime($value . ' 00:00:00'))
                                ->where("start_dt", '<=', Base::tomysqldatetime($value . ' 23:59:59'));
                        })
                        ->orwhere(function ($query) use ($value) {

                            $query
                                ->where("start_dt", '<=', Base::tomysqldatetime($value . ' 00:00:00'))
                                ->WhereNull("end_dt");
                        });
                        })

                        ->with('tasks.cust_info')

                        ->with('vehicle_info')
                        ->get()->toArray();
                    return Base::touser($array, true);

                }
            }
            return $array;

        }

        return Base::touser($array, true);
    }

    public function updatetaskStatus(Request $request, $id)
    {
        $rules = [
            'status' => 'required',
        ];

        $data = $request->input('data');

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {

            return Base::touser($validator->errors()->all()[0]);
        }
        $status       = $request->input('data')['status'];
        $emp          = new emp_cust();
        $data         = $emp->find($id);
        $data->status = isset($status) ? $status : $data->status;
        $data->save();
        $user = \App\Models\User::find($this->emp_id);

        $task_status          = new ScheduleTaskStatus();
        $task_status->emp_id  = isset($data['emp']) ? $data['emp'] : $this->emp_id;
        $task_status->task_id = $data->id;
        $task_status->address = '';
        $task_status->lat     = $data->lat;
        $task_status->long    = $data->lng;
        $task_status->status  = $data->status;
        // $task_status->timestamps = $data->timestamp;
        $task_status->save();

        event(new \App\Events\TaskUpdateEvent($data->id, $this->emp_id));

        $notification = $user->notify(new \App\Notifications\TaskCompleted($data, $user));
        event(new \App\Events\NotificationEvent($user));
        return Base::touser($data, true);
    }

    public function updatetask(Request $request, $id)
    {

        $rules = [
            'status'    => 'required',
            'lat'       => 'required',
            'timestamp' => 'required',

        ];

        $data = $request->input('data');

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {

            return Base::touser($validator->errors()->all()[0]);
        }

        $emp       = new emp_cust();
        $data      = $emp->find($id);
        $req       = $request->input('data')['status'];
        $reqlat    = $request->input('data')['lat'];
        $reqlng    = $request->input('data')['lng'];
        $timestamp = $request->input('data')['timestamp'];
        $remarks   = $request->input('data')['remarks'];
        $customer  = \App\Models\Customer::find($data->cust_id);

        $coordA = Geotools::coordinate([$reqlat, $reqlng]);
        $coordB = Geotools::coordinate([$customer->loc_lat, $customer->loc_lng]);

        $distance = Geotools::distance()->setFrom($coordA)->setTo($coordB);

        if ($distance->flat() >= $customer->desc) {

            return Base::touser('Location must be within ' . $customer->desc . ' meters');
        }

        $data->status    = isset($req) ? strtolower($req) : 'Assigned';
        $data->lat       = isset($reqlat) ? $reqlat : '';
        $data->lng       = isset($reqlng) ? $reqlng : '';
        $data->timestamp = isset($timestamp) ? Base::tomysqldatetime($timestamp) : '';
        $data->remarks   = isset($remarks) ? $remarks : '';

        $data->delivery_to      = isset($request->input('data')['remarks']) ? $request->input('data')['remarks'] : '';
        $data->delivery_phone   = isset($request->input('data')['delivery_phone']) ? $request->input('data')['delivery_phone'] : '';
        $data->is_cust_delivery = isset($request->input('data')['is_cust_delivery']) ? $request->input('data')['is_cust_delivery'] : '';
        $data->signature        = isset($request->input('data')['signature']) ? $request->input('data')['signature'] : '';

        $data->save();

        $task_status             = new ScheduleTaskStatus();
        $task_status->emp_id     = isset($data['emp']) ? $data['emp'] : $this->emp_id;
        $task_status->task_id    = $data->id;
        $task_status->address    = '';
        $task_status->lat        = $data->lat;
        $task_status->long       = $data->lng;
        $task_status->status     = $data->status;
        $task_status->timestamps = $data->timestamp;
        $task_status->save();

        if ($data->emp_cust_id) {
            $tasks = \App\Models\EmpSchedule::where('id', '=', $data->emp_cust_id)->first();
            if (empty($tasks->end_dt)) {

                if ($tasks->getIsTaskComplete()) {
                    $tasks->end_dt = date("Y-m-d H:i");
                    $tasks->save();
                } else {
                }
            }
        }
        $user         = \App\Models\User::find($this->emp_id);
        $notification = $user->notify(new \App\Notifications\TaskCompleted($data, $user));
        event(new \App\Events\NotificationEvent($user));

        event(new \App\Events\TaskUpdateEvent($data->id, $this->emp_id));

        return Base::touser($data, true);
    }

    public function store(Request $request)
    {
        $rules = [
            'emp'        => 'exists:user,user_id',
            'add_by'     => 'exists:user,user_id',
            'start_dt'   => 'required',
            // 'end_dt'     => 'required',
            'vehicle_no' => 'required',
            // 'cust_id'  => 'required|exists:customer,cust_id',
        ];

        $data = $request->input('data');

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return Base::touser($validator->errors()->all()[0]);
        }

        // $period = [Base::tomysqldatetime($data['start_dt']), Base::tomysqldatetime($data['end_dt'])];

        // if (task::where('emp', $data['emp'])
        //     ->where(function ($query) use ($period) {
        //         return $query->whereBetween('end_dt', $period)
        //         ->OrwhereBetween('start_dt', $period);

        //     })

        //     ->count() > 0) {

        //     return Base::touser('Already Driver had Trip Same Time Period');
        // }

        // if (task::where('vehicle_no', $data['vehicle_no'])

        //     ->where(function ($query) use ($period) {
        //         return $query->whereBetween('end_dt', $period)
        //         ->OrwhereBetween('start_dt', $period);

        //     })

        //     ->count() > 0) {

        //     return Base::touser('Already Vehicle had Trip Same Time Period');
        // }

        $task = new task();
        $data = $request->input('data');

        $task->end_dt         = isset($data['end_dt']) ? Base::tomysqldatetime($data['end_dt']) : null;
        $task->notes          = isset($data['notes']) ? $data['notes'] : null;
        $task->start_dt       = isset($data['start_dt']) ? Base::tomysqldatetime($data['start_dt']) : null;
        $task->vehicle_no     = isset($data['vehicle_no']) ? $data['vehicle_no'] : null;
        $task->speed_limit    = isset($data['speed_limit']) ? $data['speed_limit'] : null;
        $task->stoppage_limit = isset($data['stoppage_limit']) ? $data['stoppage_limit'] : null;

        if ($this->admin || $this->backend) {
            $task->emp = $data['emp'];

            if (empty($data['add_by'])) {

                return Base::touser('Admin Must Provide Allocated Employee Value');
            }

            $task->add_by = $data['add_by'];
        } elseif ($this->manager) {
            $task->emp    = $data['emp'];
            $task->add_by = $this->emp_id;
        } else {
            $task->emp    = $this->emp_id;
            $task->add_by = $this->emp_id;
        }

        $task->save();

        $data['cust_jobs'] = array_filter($data['cust_jobs']);

        foreach ($data['cust_jobs'] as $key => $value) {

            $sub_task              = new emp_cust();
            $sub_task->emp_id      = $data['emp'];
            $sub_task->emp_cust_id = $task->id;
            // $sub_task->start_dt         = Base::tomysqldatetime($data['start_dt']);
            // $sub_task->end_dt           = Base::tomysqldatetime($data['end_dt']);
            $sub_task->cust_id          = isset($data['cust_jobs'][$key]['cust_id']) ? $data['cust_jobs'][$key]['cust_id'] : '';
            $sub_task->notes            = isset($data['cust_jobs'][$key]['notes']) ? $data['cust_jobs'][$key]['notes'] : '';
            $sub_task->status           = isset($data['cust_jobs'][$key]['status']) ? $data['cust_jobs'][$key]['status'] : 'Assigned';
            $sub_task->timestamp        = isset($data['cust_jobs'][$key]['timestamp']) ? Base::tomysqldatetime($data['cust_jobs'][$key]['timestamp']) : null;
            $sub_task->lat              = isset($data['cust_jobs'][$key]['lat']) ? $data['cust_jobs'][$key]['lat'] : '';
            $sub_task->lng              = isset($data['cust_jobs'][$key]['lng']) ? $data['cust_jobs'][$key]['lng'] : '';
            $sub_task->delivery_to      = isset($data['cust_jobs'][$key]['delivery_to']) ? $data['cust_jobs'][$key]['delivery_to'] : '';
            $sub_task->delivery_phone   = isset($data['cust_jobs'][$key]['delivery_phone']) ? $data['cust_jobs'][$key]['delivery_phone'] : '';
            $sub_task->is_cust_delivery = isset($data['cust_jobs'][$key]['is_cust_delivery']) ? $data['cust_jobs'][$key]['is_cust_delivery'] : 1;
            $sub_task->method           = isset($data['cust_jobs'][$key]['method']) ? $data['cust_jobs'][$key]['method'] : null;
            $sub_task->remarks          = isset($data['cust_jobs'][$key]['remarks']) ? $data['cust_jobs'][$key]['remarks'] : '';
            $sub_task->signature        = isset($data['cust_jobs'][$key]['signature']) ? $data['cust_jobs'][$key]['signature'] : '';
            $sub_task->save();

            $task_status          = new ScheduleTaskStatus();
            $task_status->emp_id  = isset($data['emp']) ? $data['emp'] : $this->emp_id;
            $task_status->task_id = $sub_task->id;
            $task_status->address = '';
            $task_status->lat     = '';
            $task_status->long    = '';
            $task_status->status  = $sub_task->status;
            $task_status->save();

        }

        emp_cust::insert($data['cust_jobs']);

        $user = \App\Models\User::find($task->emp);

        $user->notify(new \App\Notifications\TaskAllocated($task, $user));

        return Base::touser('Task Created', true);
    }

    public function show(Request $request, $id)
    {
        $val = true;

        if ($this->admin || $this->backend) {
            $data = task::with('cust_jobs')->get()->find($id)->toArray();
        } elseif ($this->manager) {

            if ($request->input('subtask') == 'true') {
                $info = emp_cust::where('id', $id)
                    ->with('cust_info')
                    ->with('task.vehicle_info')
                    ->with('task.emp_info')
                    ->get();

                if ($info) {

                    $info = $info->toArray();

                    if (count($info) !== 1) {
                        return Base::touser('Task Removed');
                    } else {
                        $info = $info[0];
                    }
                } else {
                    return Base::touser('Task Removed');
                }

                $info['trip_id']          = $info['emp_cust_id'];
                $info['cust_name']        = $info['cust_info']['name'];
                $info['cust_type']        = $info['cust_info']['type'];
                $info['cust_address']     = $info['cust_info']['address'];
                $info['cust_phone']       = $info['cust_info']['contact_no'];
                $info['loc_lat']          = $info['cust_info']['loc_lat'];
                $info['loc_lng']          = $info['cust_info']['loc_lng'];
                $info['start_dt']         = $info['task']['start_dt'];
                $info['end_dt']           = $info['task']['end_dt'];
                $info['allocated_emp']    = $info['task']['emp_info']['first_name'] . ' ' . $info['task']['emp_info']['last_name'];
                $info['allocated_emp_id'] = $info['task']['emp_info']['user_id'];
                $data                     = $info;
            } else {

                $belongsemp = Base::getEmpBelongsUser($this->emp_id);

                $data = task::with('cust_jobs')->whereIn('emp', $belongsemp)->get()->find($id)->toArray();
            }

        } else {
            $data = emp_cust::where('emp_id', $this->emp_id)->get()->toArray();

            $val = true;
        }
        return Base::touser($data, true);

    }

    public function getWithStatus($id)
    {
        return Base::touser(task::with('cust_jobs')->get()->find($id), true);
    }

    public function update(Request $request, $id)
    {

        $rules = [
            'emp'        => 'exists:user,user_id',
            'add_by'     => 'exists:user,user_id',
            'start_dt'   => 'required',
            // 'end_dt'     => 'required',
            'vehicle_no' => 'required',

        ];

        $data = $request->input('data');

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return Base::touser($validator->errors()->all()[0]);
        }

        \DB::beginTransaction();

        try {
            $task = new task();
            $data = $request->input('data');
            $task = $task->where('id', '=', $id)->first();

            $task->end_dt   = isset($data['end_dt']) ? Base::tomysqldatetime($data['end_dt']) : null;
            $task->notes    = isset($data['notes']) ? $data['notes'] : null;
            $task->start_dt = isset($data['start_dt']) ? Base::tomysqldatetime($data['start_dt']) : null;

            $task->vehicle_no     = isset($data['vehicle_no']) ? $data['vehicle_no'] : null;
            $task->speed_limit    = isset($data['speed_limit']) ? $data['speed_limit'] : null;
            $task->stoppage_limit = isset($data['stoppage_limit']) ? $data['stoppage_limit'] : null;

            if ($this->admin || $this->backend) {
                $task->emp = $data['emp'];

                if (empty($data['add_by'])) {

                    return Base::touser('Admin must provide allocated employee Value');
                }

                $task->add_by = $data['add_by'];
            } elseif ($this->manager) {
                $task->emp    = $data['emp'];
                $task->add_by = $this->emp_id;
            } else {
                $task->emp = $this->emp_id;
            }

            $task->save();
            $data['cust_jobs'] = array_filter($data['cust_jobs']);

            $data['cust_jobs'] = array_filter($data['cust_jobs']);

            $sub_tasks = [];

            foreach ($data['cust_jobs'] as $key => $value) {

                if (isset($data['cust_jobs'][$key]['id'])) {
                    $sub_task = emp_cust::where('id', $data['cust_jobs'][$key]['id'])->first();

                } else {
                    $sub_task = new emp_cust();
                }

                $sub_task->emp_id      = $data['emp'];
                $sub_task->emp_cust_id = $task->id;
                // $sub_task->start_dt         = Base::tomysqldatetime($data['start_dt']);
                // $sub_task->end_dt           = Base::tomysqldatetime($data['end_dt']);
                $sub_task->cust_id          = isset($data['cust_jobs'][$key]['cust_id']) ? $data['cust_jobs'][$key]['cust_id'] : '';
                $sub_task->notes            = isset($data['cust_jobs'][$key]['notes']) ? $data['cust_jobs'][$key]['notes'] : '';
                $sub_task->status           = isset($data['cust_jobs'][$key]['status']) ? $data['cust_jobs'][$key]['status'] : 'Assigned';
                $sub_task->timestamp        = isset($data['cust_jobs'][$key]['timestamp']) ? Base::tomysqldatetime($data['cust_jobs'][$key]['timestamp']) : null;
                $sub_task->lat              = isset($data['cust_jobs'][$key]['lat']) ? $data['cust_jobs'][$key]['lat'] : '';
                $sub_task->lng              = isset($data['cust_jobs'][$key]['lng']) ? $data['cust_jobs'][$key]['lng'] : '';
                $sub_task->delivery_to      = isset($data['cust_jobs'][$key]['delivery_to']) ? $data['cust_jobs'][$key]['delivery_to'] : '';
                $sub_task->delivery_phone   = isset($data['cust_jobs'][$key]['delivery_phone']) ? $data['cust_jobs'][$key]['delivery_phone'] : '';
                $sub_task->is_cust_delivery = isset($data['cust_jobs'][$key]['is_cust_delivery']) ? $data['cust_jobs'][$key]['is_cust_delivery'] : 1;
                $sub_task->method           = isset($data['cust_jobs'][$key]['method']) ? $data['cust_jobs'][$key]['method'] : null;
                $sub_task->remarks          = isset($data['cust_jobs'][$key]['remarks']) ? $data['cust_jobs'][$key]['remarks'] : '';
                $sub_task->signature        = isset($data['cust_jobs'][$key]['signature']) ? $data['cust_jobs'][$key]['signature'] : '';
                $sub_task->save();

                $task_status          = new ScheduleTaskStatus();
                $task_status->emp_id  = isset($data['emp']) ? $data['emp'] : $this->emp_id;
                $task_status->task_id = $sub_task->id;
                $task_status->address = '';
                $task_status->lat     = '';
                $task_status->long    = '';
                $task_status->status  = $sub_task->status;
                // $task_status->timestamps = $sub_task->timestamp;
                $task_status->save();

                $sub_tasks[] = $sub_task->id;

            }

            ScheduleTaskStatus::whereNotIn('task_id', $sub_tasks)
                ->delete();

            emp_cust::where('emp_cust_id', '=', $task->id)->whereNotIn('id', $sub_tasks)
                ->delete();

            // emp_cust::insert($data['cust_jobs']);

            // foreach ($data['cust_jobs'] as $key => $value) {
            //     $data['cust_jobs'][$key]['emp_id']      = $data['emp'];
            //     $data['cust_jobs'][$key]['emp_cust_id'] = $task->id;
            //     $data['cust_jobs'][$key]['end_dt']      = Base::tomysqldatetime($data['end_dt']);
            //     $data['cust_jobs'][$key]['start_dt']    = Base::tomysqldatetime($data['start_dt']);
            //     $data['cust_jobs'][$key]['notes']       = isset($data['cust_jobs'][$key]['notes']) ? $data['cust_jobs'][$key]['notes'] : null;
            //     $data['cust_jobs'][$key]['status']      = isset($data['cust_jobs'][$key]['status']) ? $data['cust_jobs'][$key]['status'] : 'Assigned';
            //     $data['cust_jobs'][$key]['updated_at']  = isset($data['cust_jobs'][$key]['updated_at']) ? Base::tomysqldatetime($data['cust_jobs'][$key]['updated_at']) : null;
            //     $data['cust_jobs'][$key]['remarks']     = isset($data['cust_jobs'][$key]['remarks']) ? $data['cust_jobs'][$key]['remarks'] : null;
            //     $data['cust_jobs'][$key]['method']      = isset($data['cust_jobs'][$key]['method']) ? $data['cust_jobs'][$key]['method'] : null;

            //     $data['cust_jobs'][$key]['delivery_to']      = isset($data['cust_jobs'][$key]['delivery_to']) ? $data['cust_jobs'][$key]['delivery_to'] : '';
            //     $data['cust_jobs'][$key]['delivery_phone']   = isset($data['cust_jobs'][$key]['delivery_phone']) ? $data['cust_jobs'][$key]['delivery_phone'] : '';
            //     $data['cust_jobs'][$key]['is_cust_delivery'] = isset($data['cust_jobs'][$key]['is_cust_delivery']) ? $data['cust_jobs'][$key]['is_cust_delivery'] : 1;
            //     $data['cust_jobs'][$key]['signature']        = isset($data['cust_jobs'][$key]['signature']) ? $data['cust_jobs'][$key]['signature'] : '[]';

            // }

            // emp_cust::where('emp_cust_id', '=', $task->id)
            //     ->delete();

            // emp_cust::insert($data['cust_jobs']);
        } catch (Exception $e) {
            \DB::rollBack();

            throw $e;
        }

        \DB::commit();

        return Base::touser('Task Updated', true);
    }

    public function destroy($id)
    {

        $data = emp_cust::where('emp_cust_id', '=', $id)->get()->pluck(['id']);

        ScheduleTaskStatus::whereIn('task_id', $data)
            ->delete();

        emp_cust::where('emp_cust_id', '=', $id)
            ->delete();

        $api = task::find($id);
        $api->delete();
        return Base::touser('Task Deleted', true);
    }
}
