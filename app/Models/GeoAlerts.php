<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class GeoAlerts
 */
class GeoAlerts extends Model
{



    // public function cust()
    // {
    //     return $this->belongsTo('App\Models\Customer', 'loc_id', 'id');
    // }



    protected $table = 'geo_alerts';

    public $timestamps = true;

    protected $primaryKey = 'id';
    
    protected $fillable = [
        'info',
        'add_by',
        'to_driver',
        'title',
        'status',
        'address',
        'loc_id',
        'loc_type', 
        'notifi_type',
        'lat',
        'lng',
        'radius'
    ];

    protected $guarded = [];
}
