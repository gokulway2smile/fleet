<?php

namespace App\Models;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

/**
 * Class EmpSchedule
 */
class EmpSchedule1 extends Model implements AuditableContract
{
    use Auditable;

     /**
     * Display timestamps in user's timezone
     */
    protected function asDateTime($value)
    {

        $value = \App\Http\Controllers\Base::ConvertTimezone($value);

        return $value;

    }




    public function emp_info()
    {
        return $this->belongsTo('App\Models\User', 'emp', 'user_id');
    }

    public function cust_jobs()
    {
        return $this->hasMany('App\Models\EmpCustSchedule', 'emp_cust_id', 'id');
    }
    public function tasks()
    {
        return $this->hasMany('App\Models\EmpCustSchedule', 'emp_cust_id', 'id');
    }


    public function getIsTaskComplete()
    {

        $AssignedTask = array_first($this->tasks->toArray(), function ($value, $key) {
            return $value['status'] == 'Assigned';
        });

        if($AssignedTask)
        {
             return false;
        }

       return true;
    }



    public function vehicle_info()
    {

        return $this->belongsTo('App\Models\Competitor', 'vehicle_no', 'id');
    }


    protected $table = 'emp_schedule';

    public $timestamps = true;

    protected $fillable = [
        'emp',
        'add_by',
        "start_dt",
        "vehicle_no",
        "end_dt",
        "notes",
        "speed_limit",
        "stoppage_limit"
    ];

    protected $guarded = [];
}
