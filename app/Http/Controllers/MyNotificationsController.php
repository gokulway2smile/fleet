<?php

namespace App\Http\Controllers;

use App\Models\Notifications;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
class MyNotificationsController extends Controller
{

    public function GetNotifications()
    {
        if ($this->manager) {
            $data = Notifications::where('notifiable_type', 'App\Models\User')
                ->whereIn('notifiable_id', Base::getEmpBelongsUser($this->emp_id))
                ->whereNull('read_at')
                ->paginate(10);

            return Base::touser($data, true);

        }



        $user = \App\Models\User::find($this->emp_id);
        // $data = $user->unreadNotifications()->get()->toArray();


      $data =  Notifications::where('notifiable_id', $this->emp_id)
                        ->whereNull('read_at')
                        ->get()->toArray();


//return $data;
        $notifications = [];
        foreach ($data as $key => $value) {

            $title = '';
            switch ($value['type']) {
                 case 'App\Notifications\TaskUpdated':

                      $title = "Trip is Updated.";

                    break;

                case 'App\Notifications\TaskAllocated':

                    $title = "New Trip is allocated.";

                    break;

                case 'App\Notifications\TaskCompleted':

                    $title = "Trip is Completed.";
                    break;
                default:
                    break;
            }

            if (!empty($title)) {

                $info       = [];
                $info['id'] = $value['id'];
                $info['timestamps'] = $value['created_at'];
                $info['title'] = $title;

                if ($value['data']) {

                   $value['data'] =  json_decode($value['data'],true);

                    if (array_key_exists('data',$value['data'])) {



                        if (array_key_exists('start_dt',$value['data']['data'])) {
                            $start_dt =  date('d/M/Y h:i A',strtotime($value['data']['data']['start_dt']));

                        }

                        if (array_key_exists('end_dt',$value['data']['data'])) {

                            if(empty($value['data']['data']['end_dt']))
                            {
                                $end_dt = 'not specified';

                            }
                            else{
                                $end_dt = date('d/M/Y h:i A',strtotime($value['data']['data']['end_dt']));
                            }



                        }

                        if( !empty($start_dt) && !empty($end_dt))
                        {

                            $info['message'] = $start_dt .' to ' .$end_dt.' '.$title;
                            $notifications[] = $info;


                        }

                    }

                }

            }

        }




            $perPage             = 10;
            $pageStart           = \Request::get('page', 1);
            $offSet              = ($pageStart * $perPage) - $perPage;
            $itemsForCurrentPage = array_slice($notifications, $offSet, $perPage);

            $paginator = new LengthAwarePaginator($itemsForCurrentPage, count($notifications), $perPage);


            return $paginator;

    }

    public function ReadNotifications(Request $request, $id)
    {

        $notification = Notifications::where('id', $id)->first();

        if ($notification) {

            $notification->read_at = Carbon::now();
            $notification->save();
            return Base::touser('ok', true);
        } else {
            return Base::touser('ok');
        }

    }

}
