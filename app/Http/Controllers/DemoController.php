<?php
namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Base;
class DemoController extends Controller
{


    public  function getAllUrlSeen(Request $request)
    {
       return Base::touser(User::where('user_id', $this->emp_id)->get(['demo_links'])[0],true);
    }


     public  function AddUrlToSeenList(Request $request)
    {
        $url = $request->input('data')['url'];

        $data = User::where('user_id', $this->emp_id)->first();

        $allurl =json_decode($data->demo_links,true);

        if($data->demo_links)
        {

             $allurl[] = $url;


        }
        else
        {
                  $allurl = array();
           $allurl[] = $url;
        }


        $data->demo_links = json_encode($allurl);

        $data->save();


  return Base::touser(User::where('user_id', $this->emp_id)->get(['demo_links'])[0],true);
    }

     public function getContentbyHash(Request $request)
    {





  switch ($request->input('url')) {
    case 'admin.company_dashboard':

       $title = 'Fleets';
      $body = '<p>
Welcome to Fleet Dashboard. It shows your fleets current location, speed, distance traveled for the trip, current location.
Would like to look the GPS map view of your fleet ? Just click the view icon to view more.
</p>



<h4>Left Menu provides you the following options  : </h4>

<h4>Drivers:  </h4>
<p>
         Create, Edit and Delete your drivers. Should be able to activate / deactivate them.
</p>
<h4>Locations:  </h4>
<p>
         Manage all your locations including Customer, Warehouse, Branch. Mention their address and name. Will be super easy to manage.
</p>
<h4>Fleets:  </h4>
<p>
        Manage your vehicles by entering Vehicle details.
</p>
<h4>Trip Schedule:   </h4>
<p>

    Here you connect a Fleet with a Driver. This schedule tasks for Drivers for the day / week / month.
  </p>
<h4>Trip Summary:  </h4>
<p>
        Just pull up what happened in a specific Trip ? Fetch the report and get more details.
</p>
<h4>Report:   </h4>
<p>
        Detailed view of what\'s going on in your Fleet business. Collective view of all.
</p>
<h4>Stoppage Report:  </h4>
<p>

    Identify the Idle fleet time with zero movement!
   </p>
<h4>Speed Report:  </h4>
<p>

     Over speeding ? Just catch them through online.
  </p>
<h4>Live Tracking: </h4>
<p>

   </p>     Live Monitoring of your vehicles and delivery status.

<h4>GPS - History: </h4>
<p>
        Map view of their travel history. Path route can be seen here.
</p>
<h4>GEO - Fence:  </h4>
<p>

     Fence a specific area and check if your fleet gets into this area!
  </p>
<h4>Download App: </h4>
<p>
        Download our Android and iPhone App. Its cool to monitor.
</p>
<h4>My Account : </h4>
<p>
        Manages your profile and account.
</p>
<h4>Notifications: </h4>
<p>
        Right top corner will show you all notifications happening in your business.
</p>

';
      break;


 case 'admin.user':

       $title = 'Drivers Management';
      $body = '


<p style="    font-size: 16px;
    line-height: 40px;">
 <b> Drivers : </b> <br/>

List view of all drivers. You shall edit or create a new one. Add Driver allows you to add a Driver along with the following details.

<br/>
<b> GPS Tracking : </b> <br/>

If you set It as ON, Driver can not turn off GPS Tracking from his app. <br/> It\'s always ON
If It\'s set as OFF, Driver has the option to turn ON or OFF the tracking from their device.
<br/>
It\'s good to remind that Drivers can only access from mobile app ( iPhone and Android ).
<br/>
<b>Employee Status : </b> <br/> Employee can be activated or deactivated using Status dropdown.
</p>';
 break;

case 'admin.competitor':

       $title = 'Fleets Management';
      $body = '
<p style="    font-size: 16px;
    line-height: 40px;">
All you vehicles data should be managed here. Just enter number and type. Fields are self explanatory!

</p>';
 break;

case 'admin.competitor':

       $title = 'Trip Schedule';
      $body = '
<p style="    font-size: 16px;
    line-height: 40px;">
All your vehicles data should be managed here. Just enter number and type. Fields are self explanatory!

</p>';
 break;


 case 'admin.schedule':

       $title = 'Trip Schedule';
      $body = '
<p style="    font-size: 16px;
    line-height: 40px;">
It connects your fleet along with a driver.
It means the driver will pick up the fleet and deliver goods as per Trip schedule.

</p>';
 break;

 case 'admin.map':

       $title = 'Live Tracking';
      $body = '
<p style="    font-size: 16px;
    line-height: 40px;">
Show the live status of your Fleets and Drivers.
</p>';
 break;



 case 'admin.category':

       $title = 'GEO - Fence';
      $body = '
<p style="    font-size: 16px;
    line-height: 40px;">
Fence a specific area and check if your fleet gets into this area!
</p>';
 break;



 case 'admin.trip-summary':

       $title = 'Report';
      $body = '
<p style="    font-size: 16px;
    line-height: 40px;">
Just pull up what happened in a specific Trip ? Fetch the report and get more details.

</p>';
 break;




 case 'admin.report':

       $title = 'Report';
      $body = '
<p style="    font-size: 16px;
    line-height: 40px;">
Detailed view of what\'s going on in your Fleet business. Collective view of all.

</p>';
 break;


 case 'admin.stop-report':

       $title = 'Stoppage Report';
      $body = '
<p style="    font-size: 16px;
    line-height: 40px;">
Identify the Idle fleet time with zero movement!

</p>';
 break;


 case 'admin.profile':

       $title = 'My Account';
      $body = '
<p style="    font-size: 16px;
    line-height: 40px;">
Manages your profile and account.

</p>';
 break;






 case 'admin.mobile-app':

       $title = 'Download App';
      $body = '
<p style="    font-size: 16px;
    line-height: 40px;">
 Download our Android and iPhone App. Its cool to monitor !.



</p>';
 break;





 case 'admin.speed-report':

       $title = 'Speed Report';
      $body = '
<p style="    font-size: 16px;
    line-height: 40px;">
Over speeding ? Just catch them through online.

</p>';
 break;



case 'admin.customer':

       $title = 'Location Management';
      $body = '


<p style="    font-size: 16px;
    line-height: 40px;">
Location is your customer\'s pickup or drop off address.
Location type is classified as
<ul>
<li>Customer</li>
<li>Office</li>
<li>Warehouse</li>
<li>Branch</li>
</ul>
</p>
<p style="    font-size: 16px;
    line-height: 40px;">
<b>Geofence radius : </b></br>
It allows you to mention the geofencing distance.
If a fleet enters into the specific radius,</br>
You will be notified and driver will get notification through mobile app. </br>
</p>';
 break;



    default:
       $title = 'Fleets';
      $body = '<h1> We are working on documentations</h1>';
      break;
  }





      return  '<md-dialog aria-label=Employee Management">
  <form ng-cloak>
    <md-toolbar>
      <div class="md-toolbar-tools">
        <h2>'.$title.'
        </h2>
        <span flex></span>
        <md-button class="md-icon-button" ng-click="cancel()">

        </md-button>
      </div>
    </md-toolbar>

    <md-dialog-content>
      <div class="md-dialog-content">




      '.$body.'

      </div>
    </md-dialog-content>

    <md-dialog-actions layout="row">
      <span flex>
      </span>
      <md-button ng-click="close()">
       Got it
      </md-button>
    </md-dialog-actions>
  </form>
</md-dialog>';
    }

}
