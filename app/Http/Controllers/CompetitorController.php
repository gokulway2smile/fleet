<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Competitor;

use App\Http\Controllers\Base;

use Validator;

class CompetitorController extends Controller
{
    public function index()
    {

          $loggedUser = \App\Models\User::find($this->emp_id);
          $belongsemp = [];
        if($loggedUser)
        {


        $belongsemp[] = $loggedUser->user_id;


        if($loggedUser->belongs_manager)
        {

            $temp = Base::getEmpBelongsUser($loggedUser->belongs_manager);
            $temp1 = Base::getEmpBelongsUser($loggedUser->user_id);

            $temp = array_merge($temp,$temp1);
            if(is_array($temp))
            {
                 array_push($belongsemp,$temp);
            }
        }

        }

        if ($this->role === 'user') {

            if(count($belongsemp) > 0)
            {
                 return Base::touser(Competitor::whereIn('belongs_to',$belongsemp)->get(), true);

            }

            return Base::touser(Competitor::all(), true);


        } else {



            if(count($belongsemp) > 0)
            {
                  $data = Competitor::whereIn('belongs_to',$belongsemp)->get()->toArray();

            }
            else
            {
                 $data = Competitor::get()->toArray();
            }

            return Base::touser($data, true);
        }



        return Base::touser(Competitor::all(), true);


    }


    public function store(Request $request)
    {
        $rules = [
'name' => 'required',
'address' => 'required',
// 'loc_lat' => 'required',
// 'loc_lng' => 'required',
        ];

        $data = $request->input('data');


        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return Base::touser($validator->errors()->all()[0]);
        }



        if ($this->admin || $this->backend) {


            if (empty($data['belongs_to'])) {

                return Base::touser('Admin Must Provide Belongs to Employee Value');
            }

        }




        $competitor = new Competitor();
        $competitor->name = $data['name'];
        $competitor->address = $data['address'];
        $competitor->loc_lat = isset($data['loc_lat']) ? $data['loc_lat'] : null;
        $competitor->loc_lng = isset($data['loc_lng']) ? $data['loc_lng'] : null;
        $competitor->remark = isset($data['remark']) ? $data['remark'] : null ;
          $competitor->belongs_to = isset($data['belongs_to']) ? $data['belongs_to'] : $this->emp_id;

        $competitor->desc = isset($data['desc']) ? $data['desc'] : null ;
        $competitor->uploads = isset($data['uploads']) ? json_encode($data['uploads']) : null ;
        $competitor->save();
        return Base::touser('Competitor Created', true);
    }


    public function show($id)
    {
        return Base::touser(Competitor::find($id), true);
    }



    public function update(Request $request, $id)
    {
        $rules = [
//'name' => 'required|unique:competitor,id,'.$id,
'name' => 'required',
'address' => 'required',
'loc_lat' => 'required',
'loc_lng' => 'required',
        ];

        $data = $request->input('data');


        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return Base::touser($validator->errors()->all()[0]);
        }
        \DB::beginTransaction();

        try {
            $competitor = new Competitor();
            $data = $request->input('data');
            $competitor = $competitor->where('id', '=', $id)->first();
            $competitor->name = $data['name'];
            $competitor->address = $data['address'];



        if ($this->admin || $this->backend) {


            if (empty($data['belongs_to'])) {

                return Base::touser('Admin Must Provide Belongs to Employee Value');
            }

        }



            if($this->manager)
            {

                    $belongsemp = Base::getEmpBelongsUser($this->emp_id);
                    if(!in_array($competitor->belongs_to, $belongsemp))
                    {

                    return Base::touser('Fleet not belongs to your team', false);

                    }

            }




            $competitor->loc_lat = isset($data['loc_lat']) ? $data['loc_lat'] : null;


            $competitor->belongs_to = isset($data['belongs_to']) ? $data['belongs_to'] : $this->emp_id;

            $competitor->loc_lng = isset($data['loc_lng']) ? $data['loc_lng'] : null;
            $competitor->remark = isset($data['remark']) ? $data['remark'] : null ;
            $competitor->desc = isset($data['desc']) ? $data['desc'] : null ;
            $competitor->uploads = json_encode($data['uploads']);
            $competitor->save();
        } catch (Exception $e) {
            \DB::rollBack();

            throw $e;
        }

        \DB::commit();




        return Base::touser('Competitor Updated', true);
    }


    public function destroy($id)
    {
        $api = Competitor::find($id);
        $api->delete();


        return Base::touser('Competitor Deleted', true);
    }
}
