<?php
namespace App\Http\Controllers;

use Illuminate\Auth\Access\Response;
use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Controllers\Base;
use Validator;

class UserProfileController extends Controller
{
    public function index()
    {

      $data =  User::where('user_id', '=', $this->emp_id)->get()->toArray()[0];

      if($data['user_id']!=$data['belongs_manager'])
      {
            $manager =  User::where('user_id', '=', $data['belongs_manager'])->get()->toArray();
            if(is_array($manager))
            {
              if(!empty($manager[0]['timezonename']))
                {
                    $data['timezone']=$manager[0]['timezone'];
                    $data['timezonename']=$manager[0]['timezonename'];
                }
            }
            else
            {
                if(!empty($manager['timezonename']))
                {
                    $data['timezone']=$manager['timezone'];
                    $data['timezonename']=$manager['timezonename'];
                }
            }
            
      }
        if(Base::mobile_header() == 1)
        {
        $data['profile_image'] =  json_decode($data['profile_image']);


        // $data['profile_image']  = (array) str_replace("]","",str_replace("[",'',$data['profile_image']));

        }
        else
        {
             $data['profile_image'] =  json_decode($data['profile_image']);
        }

        return Base::touser($data, true);
    }

//     public function update(Request $request)
//     {
//         $data = $request->input('data');

//         $rules = [
// 'first_name' => 'required',
// 'last_name' => 'required',
// 'street' => 'required',
// // 'city' => 'required',
// // 'state' => 'required',
// // 'zipcode' => 'required',
// 'phone' => 'required',
// // 'country' => 'required',
// // 'profile_image' => 'required',
// 'email' => 'required|email|unique:user,email,'.$this->emp_id.',user_id'
//         ];

//         $validator = Validator::make($data, $rules);

//         if ($validator->fails()) {
//             return Base::touser($validator->errors()->all()[0]);
//         }


//         $user = new User();
//         $user = $user->where('user_id', '=', $this->emp_id)->first();
//         $user->first_name = $data['first_name'];
//         $user->last_name = $data['last_name'];
//         $user->phone = $data['phone'];
//         $user->city = $data['city'];
//         $user->street = $data['street'];
//         $user->state = $data['state'];
//         $user->zipcode = $data['zipcode'];
//         $user->email = $data['email'];
//         $user->state = $data['state'];
//         $user->zipcode = $data['zipcode'];
//         $user->country = $data['country'];
//         $user->profile_image = json_encode($data['profile_image'], true);
//         $user->save();
//         return Base::touser('Profile Updated', true);
//     }



 public function update(Request $request)
    {

         $data1 =  User::where('user_id', '=', $this->emp_id)->get()->toArray()[0];
         $old_email = $data1['email'];

        $data = $request->input('data');

         

        $rules = [
'first_name' => 'required',
'last_name' => 'required',
'street' => 'required',
// 'city' => 'required',
// 'state' => 'required',
// 'zipcode' => 'required',
'phone' => 'required',
// 'country' => 'required',
// 'profile_image' => 'required',
'email' => 'required|email|unique:user,email,'.$this->emp_id.',user_id'
        ];


        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return Base::touser($validator->errors()->all()[0]);
        }


        $user = new User();
        
        if(!empty($data['timezone']))
        {
        $timezone=$data['timezone'];
        $timezonename=$data['timezone'];
        }
        if(!empty($data['timezonename']))
        {
        $timezone=$data['timezonename'];
        $timezonename=$data['timezonename'];
        }



        
        $user = $user->where('user_id', '=', $this->emp_id)->first();
        $user->first_name = $data['first_name'];
        $user->last_name = $data['last_name'];
        $user->phone = $data['phone'];
        $user->street = $data['street'];
        $user->email = $data['email'];
        if(!empty($timezonename))
        $user->timezonename=$timezonename;
        if(!empty($timezone))
        $user->timezone=$timezone;
        if(!empty($data['mailnote']))
        $user->mailnote=$data['mailnote'];
        if(!empty($data['smsnote']))
        $user->smsnote=$data['smsnote'];
        //print_r($user);exit;
        // $user->city = $data['city'];
        // $user->street = $data['street'];
        // $user->state = $data['state'];
        // $user->zipcode = $data['zipcode'];
        // $user->email = $data['email'];
        // $user->state = $data['state'];
        // $user->zipcode = $data['zipcode'];
        // $user->country = $data['country'];

        if(isset($data['profile_image']))
        {
        $user->profile_image = json_encode($data['profile_image'], true);
        }


        $user->save();


        if($old_email!=$data['email'])
        {
            \App\Http\Controllers\NotificationsController::UpdateEmail($user);
        }

        
        return Base::touser('Profile Updated', true);
    }




  public function updateActivity(Request $request)
        {
            $data = $request->input('data');

            $rules = [
                     'activity' => 'required'
            ];

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                return Base::touser($validator->errors()->all()[0]);
            }
            $user = new User();
            $user = $user->where('user_id', '=', $this->emp_id)->first();
            $user->activity = $data['activity'];
            $user->save();



        $api =  \App\Models\TravelHistory::where('user_id','=', $user->user_id)
           ->orderBy('timestamp', 'desc')
           ->first();

if ($api === null) {

}
else
{
           event(new \App\Events\LocationUpdate($api, $this->emp_id));
}




            return Base::touser('Profile Updated', true);
        }





    public function reset_password(Request $request)
    {
        $data = $request->input('data');


        $rules = [
'old_password' => 'required',
'new_password' => 'required',
'confirm_password' => 'required|same:new_password',
        ];


        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return Base::touser($validator->errors()->all()[0]);
        }



        try {
            $reset = User::find($this->emp_id);

            if ((decrypt($reset->user_pwd) === $data['old_password'])) {
                $reset->user_pwd = encrypt($data['new_password']);
                $reset->save();
                return Base::touser('Password Changed', true);
            }
            return Base::touser('Old Password Incorrect');
        } catch (\Exception $e) {
            return Base::throwerror();
        }
    }
}
