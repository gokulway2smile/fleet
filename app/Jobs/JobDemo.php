<?php
namespace App\Jobs;

use App\Http\Controllers\Base;
use App\Models\TravelHistory as gpsData;
use App\Models\User as emp;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Redis;

class JobDemo implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $domain;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($domain)
    {
        $this->domain = $domain;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        Base::change_db($this->domain);
        // GPS UsersOnline
        $key_name = $this->domain . '_GpsCronUserList';
        // set validate
        Redis::set($key_name . '_Validated', date('Y-m-d H:s:i'));

        $exitingUser = json_decode(Redis::get($key_name), true);

        if (count($exitingUser) >= 1) {
            $checkedUserList = $exitingUser;
        } else {
            $checkedUserList = [];
        }

        $UsersOnline = emp::where('activity', '<>', 'Offline')
            ->whereNotIn('user_id', $checkedUserList)
            ->pluck('user_id')->toArray();

        Redis::set($key_name, json_encode(array_merge($checkedUserList, $UsersOnline)));

        $checkedUserList = json_decode(Redis::get($key_name), true);

        $GpsData = gpsData::orderBy('timestamp', 'desc')
            ->groupBy('user_id')
            ->with('user')
            ->whereIn('user_id', $checkedUserList)
            ->whereraw('TIMESTAMPDIFF(MINUTE, timestamp, "' . date("Y-m-d H:i:s") . '"  ) >= 10')
            ->get();

        $tempData = $GpsData->pluck('user_id');

        foreach ($tempData as $key => $value) {

            if (in_array($value, $checkedUserList)) {

                unset($checkedUserList[array_search($value, $checkedUserList)]);

            }
            $user           = new \App\Models\User();
            $user           = $user->where('user_id', '=', $value)->first();
            $user->activity = 'Offline';
            $user->save();

            $api = \App\Models\TravelHistory::where('user_id', '=', $user->user_id)
                ->orderBy('timestamp', 'desc')
                ->first();

            $user->notify(new App\Notifications\EmpForceOffline($user));

            event(new \App\Events\NotificationEvent($user));

            if ($api === null) {

            } else {

                event(new \App\Events\LocationUpdate($api, $user->user_id));

                if ($api->activity == 'Monitor') {
                    $api->activity = 'Stop';
                    $api->save();
                }
                if ($api->activity == 'Start') {
                    $api->delete();
                }
            }

        }

        $GpsData = gpsData::orderBy('timestamp', 'desc')
            ->groupBy('user_id')
            ->whereIn('user_id', $checkedUserList)
            ->whereraw('TIMESTAMPDIFF(MINUTE, timestamp, "' . date("Y-m-d H:i:s") . '"  ) >= 5')
            ->whereraw('TIMESTAMPDIFF(MINUTE, timestamp, "' . date("Y-m-d H:i:s") . '"  ) <= 6')
            ->get();

        $tempData = $GpsData->pluck('user_id');

        foreach ($tempData as $key => $value) {

            $user = new \App\Models\User();
            $user = $user->where('user_id', '=', $value)->first();

            $user->notify(new App\Notifications\AdminEmpAlertGoingOffline($user));

            event(new \App\Events\NotificationEvent($user));

        }

        Redis::set($key_name, json_encode($checkedUserList));

    }
}
