<?php

namespace App\Notifications;

use App\Models\User;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class FleetRegisterMail extends Mailable
{
    use SerializesModels;

    /**
     * The order instance.
     *
     * @var User
     */
    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $user->user_pwd = decrypt($user->user_pwd);
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->to($this->user->email)
                    ->bcc('bd@manageteamz.com')
                    ->markdown('FleetRegister');
    }
}