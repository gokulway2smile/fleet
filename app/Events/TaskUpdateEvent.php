<?php
namespace App\Events;

use App\Http\Controllers\Base;
use App\Models\EmpCustSchedule;
use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class TaskUpdateEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $data;
    private $user_id;
    private $userChannels = [];
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($task_id, $user_id)
    {
        $info = EmpCustSchedule::where('id',$task_id)
            ->with('cust_info')
            ->with('task.vehicle_info')
            ->with('task.emp_info')
            ->get()->toArray()[0];

        // $data['start_dt'] = $info['start_dt'];
        // $data['end_dt']   = $info['end_dt'];
        $info['trip_id']   = $info['emp_cust_id'];
        $info['cust_name'] = $info['cust_info']['name'];
        $info['start_dt']         = $info['task']['start_dt'];
        $info['end_dt']           = $info['task']['end_dt'];
        $info['cust_type']        = $info['cust_info']['type'];
        $info['cust_address']     = $info['cust_info']['address'];
        $info['cust_phone']       = $info['cust_info']['contact_no'];
        $info['loc_lat']          = $info['cust_info']['loc_lat'];
        $info['loc_lng']          = $info['cust_info']['loc_lng'];
        $info['allocated_emp']    = $info['task']['emp_info']['first_name'] . ' ' . $info['task']['emp_info']['last_name'];
        $info['allocated_emp_id'] = $info['task']['emp_info']['user_id'];
        // $info['loc_lng']          = $info['task']['cust_info']['loc_lng'];
        // $info['loc_lng']          = $info['task']['cust_info']['loc_lng'];
        $this->data         = $info;
        $this->user_id      = User::find($user_id);
        $this->data['user'] = $this->user_id;
        if ($this->user_id->belongs_manager) {
            $this->userChannels[] = new PrivateChannel(Base::get_sub_domain() . '-user-' . $this->user_id->belongs_manager);
            // $this->userChannels[] =  new PrivateChannel(Base::get_sub_domain().'-tracking-customers-'.$this->user_id->user_id);
        }

        // $this->userChannels[] =  new PrivateChannel(Base::get_sub_domain().'-current_location');
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {

        return $this->userChannels;

    }
}
