<?php
namespace App\Events;

use App\Http\Controllers\Base;
use App\Models\TravelHistory;
use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Toin0u\Geotools\Facade\Geotools;
use Illuminate\Queue\SerializesModels;

class LocationUpdate implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $data;
    private $user_id;
    private $userChannels = [];
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(TravelHistory $data, $user_id)
    {
        $this->data         = $data->toArray();
        $this->user_id      = User::find($user_id);
        $this->data['user'] = $this->user_id;

        // $first = TravelHistory::where('trip_id', $this->data['trip_id'])
        //     ->orderBy('timestamp', 'asc')->first();




        // $coordA = Geotools::coordinate([$first->lat, $first->lng]);

        // $coordB = Geotools::coordinate([$this->data['lat'], $this->data['lng']]);

        // $distance = Geotools::distance()->setFrom($coordA)->setTo($coordB);

       // $this->data['Distance'] = round($distance->in('km')->haversine(), 2);
        $this->data['Distance'] = '';

        if ($this->user_id->belongs_manager) {
            $this->userChannels[] = new PrivateChannel(Base::get_sub_domain() . '-user-' . $this->user_id->belongs_manager);
        }

        $this->userChannels[] = new PrivateChannel(Base::get_sub_domain() . '-admin');
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {

        return $this->userChannels;

    }
}
